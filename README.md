Prerequisites
=============
 * Visual Studio 2012+ / 2013 is better choice. One reason is new version of ASP.NET Web Tools and MVC 5 razor syntax
 * Resharper
 * TDS 5.0 latest version
 * SQL Server 2008 R2+ / SQL Server Express 2012+
 * Git
 * NodeJs
 * Sass

 
Environment Setup
==================
 1. Get source files from Bitbucket ssh://git@bitbucket.org:brainjocks/dmsr.git repository. Check out Develop branch.
 2. Install Sitecore version - Sitecore 8.1 rev. 151207 into sandbox subfolder.
 3. Website name - dekalbmedical
 4. license - use BrainJocks Sitecore DEV license
 5. * Install WFFM - Web Forms for Marketers 8.1 rev. 160304, 
    * Sitecore Powershell Extensions- https://marketplace.sitecore.net/en/Modules/Sitecore_PowerShell_console.aspx and 
    * WFFM-to-SQL provider - https://marketplace.sitecore.net/Modules/W/WEB_FORMS_FOR_MARKETERS_8X_SQL_PROVIDER.aspx
 6. Open solution in Visual Studio. Switch to Sandbox build configuration
 7. Open Package Manager Console to install Score into sitecore
 8. Deploy Solution
 9. open http://dekalbmedical/sitecore in browser and publish Site with Republish option.
 10. You should see home page is you open http://dekalbmedical/
 11. Add Score License and make sure http://dekalbmedical/score/about/version and http://dekalbmedical/scorebootstrapui/about/version gives you valid results.

Current main branch
====================
 **develop**

Updates to Web.Config for  bug #84051
====================
Ref: https://dev.sitecore.net/Downloads/Sitecore%20Experience%20Platform/Sitecore%2081/Sitecore%20Experience%20Platform%2081%20Update2/Release%20Notes

1. Open /sitecore/shell/client/Sitecore/ExperienceEditor/ExperienceEditor.js
2. Update line 510 to decodeURIComponent(JSON.stringify(commandContext))
3. Open /sitecore/shell/client/Sitecore/ExperienceEditor/RibbonPageCode.js 
4. Update line 24 to ribbonUrl: decodeURIComponent(this.PageEditBar.get("url")),