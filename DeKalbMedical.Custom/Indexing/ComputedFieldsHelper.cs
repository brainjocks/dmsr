﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeKalbMedical.Data.DTO.DataPart.DoctorOffices;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using Sitecore.Resources.Media;

namespace DeKalbMedical.Custom.Indexing
{
    public class ComputedFieldsHelper
    {
        public static string GetLocationAddress(Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            // Check if the template inherits the tagging template
            if (!item.Template.Name.Equals("Location"))
            {
                return null;
            } 

            // Check that the field can be found
            Assert.IsNotNull(item.Fields["Address 1"], "Field is null");

            var sb = new StringBuilder();
            var address1 = item.Fields["Address 1"].Value;
            var address2 = item.Fields["Address 2"].Value;
            var city = item.Fields["City"].Value;
            string state = string.Empty;
            LookupField stateField = item.Fields["State"];
            if (stateField != null)
            {
                var stateItem = stateField.TargetItem;
                if (stateItem != null)
                {
                    state = stateItem.Fields["Abbreviation"].Value;
                }
            }
            var zip = item.Fields["Zip"].Value;

            if (!string.IsNullOrWhiteSpace(address1))
            {
                sb.AppendLine(address1);
            }
            if (!string.IsNullOrWhiteSpace(address2))
            {
                sb.AppendLine(address2);
            }

            if (!string.IsNullOrWhiteSpace(city))
            {
                sb.Append(string.Concat(city, ", "));
            }

            if (!string.IsNullOrWhiteSpace(state))
            {
                sb.Append(string.Concat(state, " "));
            }
            if (!string.IsNullOrWhiteSpace(zip))
            {
                sb.Append(zip);
            }

            return sb.ToString();
        }

        public static List<Item> GetLocationsForDoctor(Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            if (!item.Template.Name.Equals("DeKalbMedical Physician Page"))
            {
                return null;
            }

            var officesItem = item.Axes.GetDescendant("Offices");
            Assert.IsNotNull(officesItem, "Field is null");
            var resultList = new List<Item>();
            if (officesItem.Children.Count > 0)
            {
                foreach (Item officeItem in officesItem.Children)
                {
                    LookupField locationField = officeItem.Fields["Location"];
                    var locationItem = locationField.TargetItem;
                    if (locationItem != null)
                    {
                        resultList.Add(locationItem);
                    }
                }
            }

            return resultList;
        }

        public static List<DoctorOffice> GetOfficesDataForDoctorLocation(Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            if (!item.Template.Name.Equals("DeKalbMedical Physician Page"))
            {
                return null;
            }

            var officesItem = item.Axes.GetDescendant("Offices");
            Assert.IsNotNull(officesItem, "Field is null");
            var result = new List<DoctorOffice>();

            if (officesItem.Children.Count > 0)
            {
                foreach (Item officeItem in officesItem.Children)
                {
                    result.Add(
                        new DoctorOffice
                        {
                            LocationId = IdHelper.NormalizeGuid(officeItem.Fields["Location"].Value),
                            Name = officeItem.Fields["Office Name"].Value,
                            Phone = officeItem.Fields["Office Phone"].Value,
                            Fax = officeItem.Fields["Office Fax"].Value,
                            IsPrimary = officeItem.Fields["Primary Office"].Value
                        }
                    );
                }
            }

            return result;
        }
        public static string GetGeneralLink(LinkField lf)
        {
            switch (lf.LinkType.ToLower())
            {
                case "internal":
                    UrlOptions urlOptions = new UrlOptions
                    {
                        AlwaysIncludeServerUrl = false,
                        LanguageEmbedding = LanguageEmbedding.Never
                    };
                    if (lf.TargetItem != null)
                    {
                        var computeFieldValue = LinkManager.GetItemUrl(lf.TargetItem, urlOptions);
                        computeFieldValue = computeFieldValue.Replace("/sitecore/shell", string.Empty);//workaround for indexing by Index Manager
                        return computeFieldValue;
                    }
                    return string.Empty;

                case "media":
                    MediaUrlOptions muo = new MediaUrlOptions
                    {
                        AlwaysIncludeServerUrl = false,
                        AbsolutePath = false
                    };
                    return lf.TargetItem != null ? MediaManager.GetMediaUrl(lf.TargetItem, muo) : string.Empty;
                case "external":
                    return lf.Url;
                case "anchor":
                    return !string.IsNullOrEmpty(lf.Anchor) ? "#" + lf.Anchor : string.Empty;
                case "mailto":
                    return lf.Url;
                case "javascript":
                    return lf.Url;
                default:
                    return lf.Url;
            }
        }

        public static List<string> GetShortIdsForDoctor(Item item, string fieldName, bool isParent = false)
        {
            Assert.ArgumentNotNull(item, "item");

            if (!item.Template.Name.Equals("DeKalbMedical Physician Page"))
            {
                return null;
            }

            Assert.IsNotNull(item.Fields[fieldName], "Field is null");
            var resultList = new List<string>();

            MultilistField fieldCollection = item.Fields[fieldName];
            if (fieldCollection != null)
            {
                var itemArray = fieldCollection.GetItems();

                resultList.AddRange(itemArray.Select(i => isParent 
                                        ? i.ParentID.ToShortID().ToString().ToLower() 
                                        : i.ID.ToShortID().ToString().ToLower()));
            }

            return resultList.Any() ? resultList.Distinct().ToList() : null;
        }
    }
}
