﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    public class DoctorInsurancePlansField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            return ComputedFieldsHelper.GetShortIdsForDoctor(indexable as SitecoreIndexableItem, "Insurance Accepted");
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
