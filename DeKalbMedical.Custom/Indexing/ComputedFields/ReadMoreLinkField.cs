﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    /// <summary>
    /// 
    /// </summary>
    public class ReadMoreLinkField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            return GetReadMoreLink(indexable as SitecoreIndexableItem);
        }

        private static string GetReadMoreLink(Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            if (!item.Template.Name.Equals("Location"))
            {
                return null;
            }

            Assert.IsNotNull(item.Fields["Read More Link"], "Field is null");
            LinkField lf = item.Fields["Read More Link"];

            switch (lf.LinkType.ToLower())
            {
                case "internal":
                    UrlOptions urlOptions = new UrlOptions
                    {
                        AlwaysIncludeServerUrl = false,
                        LanguageEmbedding = LanguageEmbedding.Never
                    };
                    if (lf.TargetItem != null)
                    {
                        var computeFieldValue = LinkManager.GetItemUrl(lf.TargetItem, urlOptions);
                        computeFieldValue = computeFieldValue.Replace("/sitecore/content/DeKalbMedical/home", string.Empty).Replace("/sitecore/shell/DeKalbMedical/home", string.Empty);
                            //workaround for indexing by Index Manager
                        return computeFieldValue;
                    }
                    return string.Empty;
                case "external":
                    return lf.Url;
                case "anchor":
                    return !string.IsNullOrEmpty(lf.Anchor) ? "#" + lf.Anchor : string.Empty;
                case "javascript":
                    return lf.Url;
                default:
                    return lf.Url;
            }
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
