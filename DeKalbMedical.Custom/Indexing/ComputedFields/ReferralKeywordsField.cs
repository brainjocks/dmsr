﻿using System.Linq;
using System.Text;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    /// <summary>
    /// 
    /// </summary>
    public class ReferralKeywordsField : IComputedIndexField
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="indexable"></param>
        /// <returns></returns>
        public object ComputeFieldValue(IIndexable indexable)
        {
            return GetKeywords(indexable as SitecoreIndexableItem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private string GetKeywords(Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            // Check if the template inherits the tagging template
            if (!item.Template.Name.Equals("Referral Service"))
                return null;

            // Check that the field can be found
            Assert.IsNotNull(item.Fields[FieldName], "Field is null");

            var selectedIDs = new MultilistField(item.Fields[FieldName]);
            var sb = new StringBuilder();

            if (selectedIDs.TargetIDs.Any())
            {
                var targetItems = selectedIDs.GetItems();

                foreach (var serviceItem in targetItems)
                {
                    sb.Append(string.Concat(serviceItem["Value"], "|"));
                }

                return sb.ToString();
            }

            return null;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
