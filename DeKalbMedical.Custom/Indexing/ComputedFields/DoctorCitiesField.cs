﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    public class DoctorCitiesField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            return GetCities(indexable as SitecoreIndexableItem);
        }

        private static List<string> GetCities(Item item)
        {
            var locationItems  = ComputedFieldsHelper.GetLocationsForDoctor(item);
            var resultList = new List<string>();
            if (locationItems != null)
            {
                foreach (var locationItem in locationItems)
                {
                    var city = locationItem.Fields["City"];
                    if (city != null)
                    {
                        resultList.Add(city.Value.ToLower());
                    }
                }
            }

            return resultList.Any() ? resultList: null;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
