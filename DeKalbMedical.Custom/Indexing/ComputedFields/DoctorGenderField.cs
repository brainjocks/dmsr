﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    public class DoctorGenderField  : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            return GetGender(indexable as SitecoreIndexableItem);
        }

        private static string GetGender(Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            if (!item.Template.Name.Equals("DeKalbMedical Physician Page"))
            {
                return null;
            }
                
            Assert.IsNotNull(item.Fields["Gender"], "Field is null");
            var gender = item.Fields["Gender"].Value;

            return gender.Equals("Female") 
                ? "Female" 
                : "Male";
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}