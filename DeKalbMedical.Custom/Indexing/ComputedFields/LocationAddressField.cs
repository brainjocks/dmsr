﻿using System.Text;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    public class LocationAddressField : IComputedIndexField
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="indexable"></param>
        /// <returns></returns>
            public object ComputeFieldValue(IIndexable indexable)
            {
                return GetAddress(indexable as SitecoreIndexableItem);
            }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private static string GetAddress(Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            // Check if the template inherits the tagging template
            if (!item.Template.Name.Equals("Location"))
                return null;

            // Check that the field can be found
            Assert.IsNotNull(item.Fields["Address 1"], "Field is null");

            var sb = new StringBuilder();
            var address1 = item.Fields["Address 1"].Value;
            var address2 = item.Fields["Address 2"].Value;
            var city = item.Fields["City"].Value;
            string state = string.Empty;
            LookupField stateField = item.Fields["State"];
            if (stateField != null)
            {
                var stateItem = stateField.TargetItem;
                if (stateItem != null)
                {
                    state = stateItem.Fields["Abbreviation"].Value;
                }
            }
            var zip = item.Fields["Zip"].Value;

            if (!string.IsNullOrWhiteSpace(address1))
            {
                sb.AppendLine(address1);
            }
            if (!string.IsNullOrWhiteSpace(address2))
            {
                sb.AppendLine(address2);
            }

            if (!string.IsNullOrWhiteSpace(city))
            {
                sb.Append(string.Concat(city, ", "));
            }

            if (!string.IsNullOrWhiteSpace(state))
            {
                sb.Append(string.Concat(state, " "));
            }
            if (!string.IsNullOrWhiteSpace(zip))
            {
                sb.Append(zip);
            }

            return sb.ToString();
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
