﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    public class DoctorZipsField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            return GetZips(indexable as SitecoreIndexableItem);
        }

        private static List<string> GetZips(Item item)
        {
            var locationItems = ComputedFieldsHelper.GetLocationsForDoctor(item);
            var resultList = new List<string>();
            if (locationItems != null)
            {
                foreach (var locationItem in locationItems)
                {
                    var zipField = locationItem.Fields["Zip"];
                    if (zipField != null)
                    {
                        resultList.Add(zipField.Value.ToLower());
                    }
                }
            }

            return resultList.Any() ? resultList : null;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
