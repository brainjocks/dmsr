﻿using System;
using System.Linq;
using System.Text;
using Score.Web.XmlControls;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    /// <summary>
    /// 
    /// </summary>
    public class LocationServicesField : IComputedIndexField
    {
        /// <summary>
        /// 
        /// </summary>
        private static Database ContentOrContextDatabase
        {
            get
            {
                return Sitecore.Context.ContentDatabase ?? Sitecore.Context.Database;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="indexable"></param>
        /// <returns></returns>
        public object ComputeFieldValue(IIndexable indexable)
        {
            return GetServices(indexable as SitecoreIndexableItem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private string GetServices(Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            // Check if the template inherits the tagging template
            if (!item.Template.Name.Equals("Location"))
                return null;

            // Check that the field can be found
            Assert.IsNotNull(item.Fields[FieldName], "Field is null");

            var serviceIDs = new MultilistField(item.Fields[FieldName]);
            var sb = new StringBuilder();
            try
            {
                if (serviceIDs.TargetIDs.Any())
                {
                    var targetIDs = serviceIDs.TargetIDs;

                    foreach (var serviceId in targetIDs)
                    {
                        var serviceItem = ContentOrContextDatabase.GetItem(serviceId);

                        if (serviceItem != null && serviceItem["Name"] != null)
                        {
                            sb.Append(string.Concat(serviceItem["Name"], "|")); 
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error("Failed to GetServices for " + item.ID.ToString() + " with error " + ex.Message, this);
            }

            return sb.ToString();
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
