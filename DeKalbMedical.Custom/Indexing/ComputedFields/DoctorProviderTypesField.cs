﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    public class DoctorProviderTypesField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            return ComputedFieldsHelper.GetShortIdsForDoctor(indexable as SitecoreIndexableItem, "Provider Types");
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
