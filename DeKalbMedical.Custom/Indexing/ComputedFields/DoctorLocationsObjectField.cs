﻿using System.Collections.Generic;
using System.Linq;
using DeKalbMedical.Data.DTO.DataPart.DoctorOffices;
using Newtonsoft.Json;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    public class DoctorLocationsObjectField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            return GetLocationObject(indexable as SitecoreIndexableItem);
        }

        private static List<string> GetLocationObject(Item item)
        {
            var locationItems = ComputedFieldsHelper.GetLocationsForDoctor(item);
            var doctorOffices = ComputedFieldsHelper.GetOfficesDataForDoctorLocation(item);
            var resultList = new List<string>();
            
            if (locationItems != null && locationItems.Count > 0)
            {
                foreach (var locationItem in locationItems)
                {
                    var locationShortId = locationItem.ID.ToShortID().ToString().ToLower();

                    var address = ComputedFieldsHelper.GetLocationAddress(locationItem);
 
                    var isPrimary = "0";
                    var locationName = string.Empty;
                    var phone = string.Empty;
                    var fax = string.Empty;

                    foreach (var office in doctorOffices)
                    {
                        if (office.LocationId.Equals(locationShortId))
                        {
                            isPrimary = office.IsPrimary;
                            locationName = office.Name;
                            phone = office.Phone;
                            fax = office.Fax;
                        }
                    }

                    locationName = string.IsNullOrWhiteSpace(locationName)
                        ? locationItem.Fields["Page Title"].Value
                        :locationName;

                    phone = string.IsNullOrWhiteSpace(locationItem.Fields["Main Phone"].Value)
                        ? phone
                        : locationItem.Fields["Main Phone"].Value;
                    
                    fax = string.IsNullOrWhiteSpace(locationItem.Fields["Main Fax"].Value)
                        ? fax
                        : locationItem.Fields["Main Fax"].Value;

                    var geoLocation = string.IsNullOrWhiteSpace(locationItem.Fields["GeoLocation"].Value)
                        ? string.Empty
                        : locationItem.Fields["GeoLocation"].Value;

                    var website = ComputedFieldsHelper.GetGeneralLink(locationItem.Fields["Website"]);

                    var resultLine = new DoctorOfficesResponse
                    {
                        Id = locationShortId,
                        Name = locationName,
                        Address = address,
                        IsPrimary = isPrimary,
                        Phone = phone,
                        Fax = fax,
                        Geolocation = geoLocation,
                        Website = website
                    };

                    resultList.Add(JsonConvert.SerializeObject(resultLine));
                }
            }
            else
            {
                if (doctorOffices != null)
                {
                    foreach (var office in doctorOffices)
                    {
                        var resultLine = new DoctorOfficesResponse
                        {
                            Id = string.Empty,
                            Name = office.Name,
                            Address = string.Empty,
                            IsPrimary = office.IsPrimary,
                            Phone = office.Phone,
                            Fax = office.Fax,
                            Geolocation = string.Empty,
                            Website = string.Empty
                        };

                        resultList.Add(JsonConvert.SerializeObject(resultLine));
                    }
                }
            }

            return resultList.Any() ? resultList : null;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
