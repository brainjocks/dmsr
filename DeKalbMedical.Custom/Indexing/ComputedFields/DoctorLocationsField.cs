﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    public class DoctorLocationsField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            return GetLocationIds(indexable as SitecoreIndexableItem);
        }

        private static List<string> GetLocationIds(Item item)
        {
            var locationItems = ComputedFieldsHelper.GetLocationsForDoctor(item);
            var resultList = new List<string>();
            if (locationItems != null)
            {
                resultList.AddRange(locationItems.Select(locationItem => locationItem.ID.ToShortID().ToString().ToLower()));
            }

            return resultList.Any() ? resultList : null;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
