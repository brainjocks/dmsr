﻿using System;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    public class DoctorSpecialtiesField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            return GetSpecialties(indexable as SitecoreIndexableItem);
        }

        private static string GetSpecialties(Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            if (!item.Template.Name.Equals("DeKalbMedical Physician Page"))
            {
                return null;
            }

            Assert.IsNotNull(item.Fields["Specialties"], "Field is null");

            MultilistField specialtiesFields = item.Fields["Specialties"];
            if (specialtiesFields != null)
            {
                var specialtiesList = specialtiesFields.GetItems();
                
                    return string.Join(", ", Array.ConvertAll(specialtiesList, x => x.Name));
            }

            return string.Empty;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
