﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    public class LocationStateField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            return GetAddress(indexable as SitecoreIndexableItem);
        }

        private static string GetAddress(Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            if (!item.Template.Name.Equals("Location"))
            {
                return null;
            }
                
            Assert.IsNotNull(item.Fields["State"], "Field is null");

            LookupField stateField = item.Fields["State"];
            if (stateField != null)
            {
                var stateItem = stateField.TargetItem;
                if (stateItem != null)
                {
                    return stateItem.Fields["Abbreviation"].Value;
                }
            }
            
            return string.Empty;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
