﻿using Score.Data.Items.MetaData;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    public class KeywordsField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            var item = (SitecoreIndexableItem)indexable;

            if (item == null)
                return string.Empty;

            PageMetaDataItem metaDataItem = item.Item;

            if (metaDataItem == null)
                return null;

            return metaDataItem.KeywordList;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
