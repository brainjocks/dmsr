﻿using System;
using System.Globalization;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    /// <summary>
    /// 
    /// </summary>
    public class GeolocationLongitudeField  : IComputedIndexField
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="indexable"></param>
        /// <returns></returns>
        public object ComputeFieldValue(IIndexable indexable)
        {
            return GetGeoData(indexable as SitecoreIndexableItem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private static double GetGeoData(Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            if (!item.Template.Name.Equals("Location"))
            {
                return 0.0d;
            }
                
            Assert.IsNotNull(item.Fields["GeoLocation"], "Field is null");

            var coords = item["GeoLocation"];
            if (!string.IsNullOrEmpty(coords))
            {
                if (!string.IsNullOrWhiteSpace(coords))
                {
                    var coordsSplit = coords.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                    if (coordsSplit.Length > 1)
                    {
                        return Convert.ToDouble(coordsSplit[1], CultureInfo.InvariantCulture);
                    }
                }
            }
            return 0.0d;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
