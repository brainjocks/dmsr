﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Sitecore.Resources.Media;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    /// <summary>
    /// 
    /// </summary>
    public class ImageIndexField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Assert.ArgumentNotNull(indexable, "indexable");
            var indexableItem = indexable as SitecoreIndexableItem;

            if (indexableItem == null)
            {
                Log.Warn(string.Format("{0} : unsupported IIndexable type : {1}", this, indexable.GetType()), this);
                return null;
            }

            if (indexableItem.Item.Fields[FieldName] == null)
            {
                return null;
            }

            ImageField img = indexableItem.Item.Fields[FieldName];

            //using custom MediaUrlOptions here to avoid Sitecore issue:
            //if running Index Rebuild by Index Manager, the host is sitecore/shell and it's added to each link;
            //remove AbsolutePath to skip that
            MediaUrlOptions muo = new MediaUrlOptions
            {
                AlwaysIncludeServerUrl = false,
                AbsolutePath = false
            };
            return img == null || img.MediaItem == null ? null : MediaManager.GetMediaUrl(img.MediaItem, muo);
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
