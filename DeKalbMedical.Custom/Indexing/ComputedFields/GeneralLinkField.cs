﻿using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.Sites;
using Sitecore.Web;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    /// <summary>
    /// 
    /// </summary>
    public class GeneralLinkField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Assert.ArgumentNotNull(indexable, "indexable");
            var indexableItem = indexable as SitecoreIndexableItem;

            if (indexableItem == null)
            {
                Log.Warn(string.Format("{0} : unsupported IIndexable type : {1}", this, indexable.GetType()), this);
                return null;
            }

            if (indexableItem.Item.Fields[FieldName] == null)
            {
                return null;
            }

            LinkField lf = indexableItem.Item.Fields[FieldName];

            switch (lf.LinkType.ToLower())
            {
                case "internal":
                    UrlOptions urlOptions = new UrlOptions
                    {
                        AlwaysIncludeServerUrl = false,
                        LanguageEmbedding = LanguageEmbedding.Never
                    };
                    if (lf.TargetItem != null)
                    {
                        var computeFieldValue = LinkManager.GetItemUrl(lf.TargetItem, urlOptions);
                        computeFieldValue = computeFieldValue.Replace("/sitecore/content/DeKalbMedical/home", string.Empty).Replace("/sitecore/shell/DeKalbMedical/home", string.Empty);//workaround for indexing by Index Manager
                        return computeFieldValue;
                    }
                    return  string.Empty;

                case "media":
                    MediaUrlOptions muo = new MediaUrlOptions
                    {
                        AlwaysIncludeServerUrl = false,
                        AbsolutePath = false
                    };
                    return lf.TargetItem != null ? MediaManager.GetMediaUrl(lf.TargetItem, muo) : string.Empty;
                case "external":
                    return lf.Url;
                case "anchor":
                    return !string.IsNullOrEmpty(lf.Anchor) ? "#" + lf.Anchor : string.Empty;
                case "mailto":
                    return lf.Url;
                case "javascript":
                    return lf.Url;
                default:
                    return lf.Url;
            }
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}
