﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;

namespace DeKalbMedical.Custom.Indexing.ComputedFields
{
    public class DoctorInsurancesField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            return ComputedFieldsHelper.GetShortIdsForDoctor(indexable as SitecoreIndexableItem, "Insurance Accepted", true);
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}