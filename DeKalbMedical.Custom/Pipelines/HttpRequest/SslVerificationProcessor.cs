﻿using System.Web;
using Score.Custom.Pipelines.SslDetection;
using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Pipelines.HttpRequest;

namespace DeKalbMedical.Custom.Pipelines.HttpRequest
{
    public class SslVerificationProcessor
    {
        public void Process(HttpRequestArgs args)
        {
            if (Context.Item == null)
            {
                return;
            }

            if (args.Context == null)
            {
                return;
            }

            if (!Context.PageMode.IsNormal)
            {
                return;
            }

            if (args.Context.Request.IsSecureConnection)
            {
                return;
            }

            var sslDetectionManager = new SslDetectionManager();
            var wrappedRequest = new HttpRequestWrapper(args.Context.Request);
            var newUrl = sslDetectionManager.GetRedirectUrlForWrongProtocol(Context.Item, wrappedRequest);

            if (string.IsNullOrEmpty(newUrl))
            {
                return;
            }
                
            Log.Info($"SslVerificationProcessor: redirects from {args.Context.Request.Url.AbsoluteUri} to {newUrl}", this);

            args.Context.Response.Redirect(newUrl);
        }
    }
}