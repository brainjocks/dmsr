﻿using System;
using System.Text.RegularExpressions;
using Sitecore.WFFM.Abstractions.Actions;
using WFFM8.To.SQL.Model;

namespace DeKalbMedical.Custom.WFFM
{
    public class FieldFactory
    {
        public Field Create(WFFM8.To.SQL.Infrastructure.Data.Field dataField, Form form)
        {
            if (dataField == null)
                return (Field)null;
            return new Field()
            {
                FieldId = dataField.FieldId,
                Form = (IForm)form,
                Value = dataField.Value,
                Data = dataField.Data,
                FieldName = dataField.FieldName,
                StorageName = form.FormStorageName,
                FormTimeStamp = form.Timestamp
            };
        }

        public WFFM8.To.SQL.Infrastructure.Data.Field Create(AdaptedControlResult adaptedControlResult)
        {
            if (adaptedControlResult == null)
                return (WFFM8.To.SQL.Infrastructure.Data.Field)null;
            return new WFFM8.To.SQL.Infrastructure.Data.Field()
            {
                Id = Guid.NewGuid(),
                FieldId = new Guid(((ControlResult)adaptedControlResult).FieldID),
                FieldName = ((ControlResult)adaptedControlResult).FieldName,
                Data = ((ControlResult)adaptedControlResult).Parameters?? string.Empty,
                Value = ((ControlResult)adaptedControlResult).Parameters == null || !((ControlResult)adaptedControlResult).Parameters.StartsWith("secure:") ? adaptedControlResult.Value : Regex.Replace(((ControlResult)adaptedControlResult).Parameters, "<schidden>.*</schidden>", "<schidden></schidden>")
            };
        }
    }
}
