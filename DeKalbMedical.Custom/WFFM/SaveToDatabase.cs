﻿using System;
using System.Data;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.WFFM.Abstractions.Actions;

namespace DeKalbMedical.Custom.WFFM
{
    public class SaveToDatabase : ISaveAction
    {
        private readonly FormRepository _formRepository = new FormRepository();

        public virtual void Execute(ID formId, AdaptedResultList fields, ActionCallContext actionCallContext = null, params object[] data)
        {
            Assert.ArgumentNotNull((object)formId, "formid");
            Assert.ArgumentNotNull((object)data, "data");
            try
            {
                ID result = ID.Null;
                if (data != null && data.Length > 0)
                    ID.TryParse(data[0], out result);
                this._formRepository.Insert(formId, fields, result,
                    data == null || data.Length <= 1 ? (string)null : data[0].ToString());
            }
            catch (EntityCommandExecutionException ex)
            {
                Exception exception = (Exception)ex;
                if (ex.InnerException != null)
                    exception = ex.InnerException;
                Log.Error("Save To Database failed.", exception, (object)exception);
                throw exception;
            }
            catch (Exception ex)
            {
                Log.Error("Save To Database failed.", ex, (object)ex);
                throw;
            }
        }

        //[Obsolete("Use Execute instead.")]
        //public void Submit(ID formid, Sitecore.WFFM.Abstractions.Actions.AdaptedResultList fields)
        //{
        //    this.Execute(formid, fields, (object[])null);
        //}

        public ActionState QueryState(ActionQueryContext queryContext)
        {
            return ActionState.Enabled;
        }

        public ID ActionID { get; set; }
        public string UniqueKey { get; set; }
        public ActionType ActionType { get; private set; }
    }
}
