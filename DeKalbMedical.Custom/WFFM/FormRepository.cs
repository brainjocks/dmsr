﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.WFFM.Abstractions.Actions;
using WFFM8.To.SQL.Infrastructure.Data;
using WFFM8.To.SQL.Model;
using Sitecore.Configuration;
using System.Configuration;

namespace DeKalbMedical.Custom.WFFM
{
    public class FormRepository
    {
        private readonly FormFactory _formFactory = new FormFactory();

        private string ConnectionString
        {
            get
            {
                return Settings.GetConnectionString(Settings.GetSetting("WFM.ConnectionString"));
            }
        }

        public void Insert(ID formId, AdaptedResultList fields, ID sessionID, string data)
        {
            Assert.ArgumentNotNull((object)formId, "formId");
            Assert.ArgumentNotNull((object)fields, "fields");
            WFFM8.To.SQL.Infrastructure.Data.Form entity = this._formFactory.Create(formId, fields, sessionID, data);
            using (WebFormForMarketersDataContext marketersDataContext = new WebFormForMarketersDataContext(this.ConnectionString))
            {
                marketersDataContext.Forms.InsertOnSubmit(entity);
                marketersDataContext.SubmitChanges();
            }
        }

        public IEnumerable<Form> Get(ID formId)
        {
            List<Form> list = new List<Form>();
            using (WebFormForMarketersDataContext marketersDataContext = new WebFormForMarketersDataContext(this.ConnectionString))
            {
                IQueryable<WFFM8.To.SQL.Infrastructure.Data.Form> source = Queryable.Where<WFFM8.To.SQL.Infrastructure.Data.Form>((IQueryable<WFFM8.To.SQL.Infrastructure.Data.Form>)marketersDataContext.Forms, (Expression<Func<WFFM8.To.SQL.Infrastructure.Data.Form, bool>>)(f => f.FormItemId == formId.Guid));
                Expression<Func<WFFM8.To.SQL.Infrastructure.Data.Form, DateTime>> keySelector = (Expression<Func<WFFM8.To.SQL.Infrastructure.Data.Form, DateTime>>)(f => f.Timestamp);
                foreach (WFFM8.To.SQL.Infrastructure.Data.Form dataForm in (IEnumerable<WFFM8.To.SQL.Infrastructure.Data.Form>)Queryable.OrderBy<WFFM8.To.SQL.Infrastructure.Data.Form, DateTime>(source, keySelector))
                {
                    WFFM8.To.SQL.Model.Form form = this._formFactory.Create(dataForm);
                    if (form != null)
                        list.Add((Form)form);
                }
            }
            return (IEnumerable<Form>) list;
        }

        public IEnumerable<Form> Get(ID formId, DateTime from, DateTime to)
        {
            List<Form> list = new List<Form>();
            using (WebFormForMarketersDataContext marketersDataContext = new WebFormForMarketersDataContext(this.ConnectionString))
            {
                IQueryable<WFFM8.To.SQL.Infrastructure.Data.Form> source = Queryable.Where<WFFM8.To.SQL.Infrastructure.Data.Form>(Queryable.Where<WFFM8.To.SQL.Infrastructure.Data.Form>((IQueryable<WFFM8.To.SQL.Infrastructure.Data.Form>)marketersDataContext.Forms, (Expression<Func<WFFM8.To.SQL.Infrastructure.Data.Form, bool>>)(f => f.FormItemId == formId.Guid)), (Expression<Func<WFFM8.To.SQL.Infrastructure.Data.Form, bool>>)(f => f.Timestamp >= from && f.Timestamp <= to));
                Expression<Func<WFFM8.To.SQL.Infrastructure.Data.Form, DateTime>> keySelector = (Expression<Func<WFFM8.To.SQL.Infrastructure.Data.Form, DateTime>>)(f => f.Timestamp);
                foreach (WFFM8.To.SQL.Infrastructure.Data.Form dataForm in (IEnumerable<WFFM8.To.SQL.Infrastructure.Data.Form>)Queryable.OrderBy<WFFM8.To.SQL.Infrastructure.Data.Form, DateTime>(source, keySelector))
                {
                    WFFM8.To.SQL.Model.Form form = this._formFactory.Create(dataForm);
                    if (form != null)
                        list.Add((Form)form);
                }
            }
            return (IEnumerable<Form>)list;
        }

        public DateTime OldestFormSubmission(ID formId)
        {
            DateTime dateTime = DateTime.Now;
            using (WebFormForMarketersDataContext marketersDataContext = new WebFormForMarketersDataContext(this.ConnectionString))
            {
                IQueryable<WFFM8.To.SQL.Infrastructure.Data.Form> source = Queryable.Where<WFFM8.To.SQL.Infrastructure.Data.Form>((IQueryable<WFFM8.To.SQL.Infrastructure.Data.Form>)marketersDataContext.Forms, (Expression<Func<WFFM8.To.SQL.Infrastructure.Data.Form, bool>>)(f => f.FormItemId == formId.Guid));
                if (Queryable.Count<WFFM8.To.SQL.Infrastructure.Data.Form>(source) > 0)
                    dateTime = Queryable.Min<WFFM8.To.SQL.Infrastructure.Data.Form, DateTime>(source, (Expression<Func<WFFM8.To.SQL.Infrastructure.Data.Form, DateTime>>)(f => f.Timestamp));
            }
            return dateTime;
        }

        public void UpdateStorage(string formId)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                var queryString = "UPDATE Form SET StorageName = 'Archived' WHERE  Id = '" + formId + "'";
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlCommand cmd = new SqlCommand(queryString, con);

                // execute the query to update the database
                cmd.ExecuteNonQuery();

                con.Close();
            }
        }
    }
}
