﻿using System;
using System.Collections.Generic;
using Sitecore.Pipelines.GetAboutInformation;
using WFFM8.To.SQL.Model;

namespace DeKalbMedical.Custom.WFFM
{
    public class Form : WFFM8.To.SQL.Model.Form
    {
        public new DateTime Timestamp { get; set; }
        public string FormStorageName { get; set; }

        public Guid FormId { get; set; }

        public Form(DateTime timestamp) : base(timestamp)
        {
            this.Timestamp = timestamp;
            this.Field = (IEnumerable<IField>)new List<IField>();
        }
    }
}
