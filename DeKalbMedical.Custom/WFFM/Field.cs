﻿using System;

namespace DeKalbMedical.Custom.WFFM
{
    public class Field :WFFM8.To.SQL.Model.Field
    {
        public string StorageName { get; set; }
        public DateTime FormTimeStamp { get; set; }
    }
}
