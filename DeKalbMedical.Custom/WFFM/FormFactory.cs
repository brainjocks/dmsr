﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.WFFM.Abstractions.Actions;
using WFFM8.To.SQL.Model;

namespace DeKalbMedical.Custom.WFFM
{
    public class FormFactory
    {
        private readonly FieldFactory _fieldFactory = new FieldFactory();

        internal WFFM8.To.SQL.Infrastructure.Data.Form Create(ID formId, AdaptedResultList fields, ID sessionID, string data)
        {
            Assert.ArgumentNotNull((object)formId, "formId");
            Assert.ArgumentNotNull((object)fields, "fields");
            WFFM8.To.SQL.Infrastructure.Data.Form form = new WFFM8.To.SQL.Infrastructure.Data.Form()
            {
                Id = Guid.NewGuid(),
                FormItemId = formId.Guid,
                SessionId = sessionID.ToGuid(),
                Data = data,
                StorageName = string.Empty,
                Timestamp = DateTime.Now
            };
            IEnumerator enumerator = fields.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    WFFM8.To.SQL.Infrastructure.Data.Field entity = this._fieldFactory.Create((AdaptedControlResult)enumerator.Current);
                    form.Fields.Add(entity);
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable != null)
                    disposable.Dispose();
            }
            return form;
        }

        internal Form Create(WFFM8.To.SQL.Infrastructure.Data.Form dataForm)
        {
            if (dataForm.StorageName == String.Empty)
                dataForm.StorageName = "Not Archived";
            Form form = new Form(dataForm.Timestamp)
            {
                Timestamp = dataForm.Timestamp,
                FormItemId = dataForm.FormItemId,
                InteractionId = dataForm.SessionId,
                Data = dataForm.Data, 
                FormStorageName = dataForm.StorageName, 
                FormId =  dataForm.Id
            };
            List<Field> list = new List<Field>();
            foreach (WFFM8.To.SQL.Infrastructure.Data.Field dataField in dataForm.Fields)
            {
                Field field = this._fieldFactory.Create(dataField, form);
                int test = string.Compare(field.FieldName, "*Captcha", StringComparison.CurrentCultureIgnoreCase);
                if (field != null && (string.Compare(field.FieldName, "*Captcha", StringComparison.CurrentCultureIgnoreCase) != 0) && (string.Compare(field.FieldName, "Captcha", StringComparison.CurrentCultureIgnoreCase) != 0))
                    list.Add(field);
            }
            form.Field = (IEnumerable<IField>)list;
            return form;
        }
    }
}
