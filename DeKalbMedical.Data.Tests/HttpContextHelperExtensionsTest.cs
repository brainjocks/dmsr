﻿using System;
using DeKalbMedical.Web.Extensions;
using FluentAssertions;
using NUnit.Framework;

namespace DeKalbMedical.Data.Tests
{
    public class HttpContextHelperExtensionsTest
    {
        [Test]
        public void ApplyLinkFormatShouldWorkCorrectlyIfUriIsNotEmpty()
        {
            var link = "/DeKalbMedical/home/our-services/workswell";

            var result = link.ApplyLinkFormat(new Uri("http://test.com"));

            result.Should().Be("http://test.com/home/our-services/workswell");
        }
    }
}
