﻿using DeKalbMedical.Data.DTO.DataPart.DoctorOffices;
using DeKalbMedical.Data.DTO.DataPart.Google;
using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;

namespace DeKalbMedical.Data.Tests
{
    public class SearchServiceTest
    {
        private DoctorOfficesResponse _doctorOfficesResponse;
        private string _json;

        [SetUp]
        public void Init()
        {
            this._doctorOfficesResponse = new DoctorOfficesResponse
            {
                Id = "44c1c66e92304ad7a9b67aa045083e00",
                Name = "East Atlanta Cardiology LLC 2675 North Decatur Road",
                Address = "2675 North Decatur Road Suite 307 Decatur, GA 30033",
                IsPrimary = "1",
                Phone = "(404) 508-7300",
                Fax = string.Empty,
                Geolocation = string.Empty,
                Website = string.Empty
            };

            this._json = "{\"Id\":\"44c1c66e92304ad7a9b67aa045083e00\",\"Name\":\"East Atlanta Cardiology LLC 2675 North Decatur Road\",\"Address\":\"2675 North Decatur Road Suite 307 Decatur, GA 30033\",\"IsPrimary\":\"1\",\"Phone\":\"(404) 508-7300\",\"Fax\":\"\",\"Geolocation\":\"\",\"Website\":\"\"}";
        }

        [Test]
        public void DoctorOfficesResponseShouldBeSerializedCorrectly()
        {
            var result = JsonConvert.SerializeObject(this._doctorOfficesResponse);
            result.Should().BeEquivalentTo(this._json);
        }

        [Test]
        public void DoctorOfficesResponseShouldBeDeserializedCorrectly()
        {
            var result = JsonConvert.DeserializeObject<DoctorOfficesResponse>(this._json);
            result.Should().Equals(this._doctorOfficesResponse);
        }
    }
}
