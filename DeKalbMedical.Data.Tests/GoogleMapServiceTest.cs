﻿using System.Collections.Generic;
using DeKalbMedical.Data.DTO.DataPart.Google;
using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;

namespace DeKalbMedical.Data.Tests
{
    public class GoogleMapServiceTest
    {
        private GoogleResponse _googleResponse;
        private string _json;

        [SetUp]
        public void Init()
        {
            this._googleResponse = new GoogleResponse
            {
                Status = "OK",
                Results = new List<GoogleResponseResults>
                {
                    new GoogleResponseResults
                    {
                        AddressComponents = new List<GoogleResponseAddressComponents>
                        {
                            new GoogleResponseAddressComponents
                            {
                                LongName = "30033",
                                ShortName = "30033",
                                Types = new List<string> {"postal_code"}
                            },
                            new GoogleResponseAddressComponents
                            {
                                LongName = "Decatur",
                                ShortName = "Decatur",
                                Types = new List<string> {"locality", "political"}
                            }
                        },
                        FormattedAddress = "Decatur, GA 30033, USA",
                        Geometry = new GoogleResponseGeometry
                        {
                            Bounds = new GoogleResponseBounds
                            {
                                NorthEast = new Location(33.844839, -84.2507189),
                                SouthWest = new Location(33.7812579, -84.32241999999999)
                            },
                            Location = new Location(33.8092255, -84.28054779999999),
                            LocationType = "APPROXIMATE",
                            ViewPort = new GoogleResponseBounds
                            {
                                NorthEast = new Location(33.844839, -84.2507189),
                                SouthWest = new Location(33.7812579, -84.32241999999999)
                            }
                        },
                        PlaceId = "ChIJl2vKGZEH9YgRfSoUdhsp0T0",
                        Types = new List<string> {"postal_code"}
                    }
                }
            };

            this._json = "{\"results\":[{\"address_components\":[{\"long_name\":\"30033\",\"short_name\":\"30033\",\"types\":[\"postal_code\"]},{\"long_name\":\"Decatur\",\"short_name\":\"Decatur\",\"types\":[\"locality\",\"political\"]}],\"formatted_address\":\"Decatur, GA 30033, USA\",\"geometry\":{\"bounds\":{\"northeast\":{\"lat\":33.844839,\"lng\":-84.2507189},\"southwest\":{\"lat\":33.7812579,\"lng\":-84.32242}},\"location\":{\"lat\":33.8092255,\"lng\":-84.2805478},\"location_type\":\"APPROXIMATE\",\"viewport\":{\"northeast\":{\"lat\":33.844839,\"lng\":-84.2507189},\"southwest\":{\"lat\":33.7812579,\"lng\":-84.32242}}},\"place_id\":\"ChIJl2vKGZEH9YgRfSoUdhsp0T0\",\"types\":[\"postal_code\"]}],\"status\":\"OK\"}";
        }

        [Test]
        public void GoogleResponseShouldBeSerializedCorrectly()
        {
            var result = JsonConvert.SerializeObject(this._googleResponse);
            result.Should().BeEquivalentTo(this._json);
        }

        [Test]
        public void GoogleResponseShouldBeDeserializedCorrectly()
        {
            var result = JsonConvert.DeserializeObject<GoogleResponse>(this._json);
            result.Should().Equals(this._googleResponse);
        }
    }
}
