﻿namespace DeKalbMedical.Data.DTO.DataPart
{
    public class PagedInfo
    {
        public int TotalRows { get; set; }
        public int Current { get; set; }
        public int Size { get; set; }
    }
}
