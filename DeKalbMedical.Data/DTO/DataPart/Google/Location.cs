namespace DeKalbMedical.Data.DTO.DataPart.Google
{
    /// <summary>
    /// 
    /// </summary>
    public class Location
    {
        public Location()
        {
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_lat"></param>
        /// <param name="_lng"></param>
        public Location(double _lat, double _lng)
        {
            lat = _lat;
            lng = _lng;
        }

        public double lat { get; set; }

        public double lng { get; set; }
    }
}