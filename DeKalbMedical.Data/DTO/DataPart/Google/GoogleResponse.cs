﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DeKalbMedical.Data.DTO.DataPart.Google
{
    public class GoogleResponse
    {
        [JsonProperty(PropertyName = "results")]
        public List<GoogleResponseResults> Results { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
    }

    [JsonObject(Title = "results")]
    public class GoogleResponseResults
    {
        [JsonProperty(PropertyName = "address_components")]
        public List<GoogleResponseAddressComponents> AddressComponents { get; set; }

        [JsonProperty(PropertyName = "formatted_address")]
        public string FormattedAddress { get; set; }

        [JsonProperty(PropertyName = "geometry")]
        public GoogleResponseGeometry Geometry { get; set; }

        [JsonProperty(PropertyName = "place_id")]
        public string PlaceId { get; set; }

        [JsonProperty(PropertyName = "types")]
        public List<string> Types { get; set; }
    }

    [JsonObject(Title = "address_components")]
    public class GoogleResponseAddressComponents
    {
        [JsonProperty(PropertyName = "long_name")]
        public string LongName { get; set; }

        [JsonProperty(PropertyName = "short_name")]
        public string ShortName { get; set; }

        [JsonProperty(PropertyName = "types")]
        public List<string> Types { get; set; }
    }

    [JsonObject(Title = "geometry")]
    public class GoogleResponseGeometry
    {
        [JsonProperty(PropertyName = "bounds")]
        public GoogleResponseBounds Bounds { get; set; }

        [JsonProperty(PropertyName = "location")]
        public Location Location { get; set; }

        [JsonProperty(PropertyName = "location_type")]
        public string LocationType { get; set; }

        [JsonProperty(PropertyName = "viewport")]
        public GoogleResponseBounds ViewPort { get; set; }
    }

    public class GoogleResponseBounds
    {
        [JsonProperty(PropertyName = "northeast")]
        public Location NorthEast { get; set; }

        [JsonProperty(PropertyName = "southwest")]
        public Location SouthWest { get; set; }
    }
}
