using System.Collections.Generic;

namespace DeKalbMedical.Data.DTO.DataPart.Google
{
    /// <summary>
    /// 
    /// </summary>
    public class RootObject
    {
        public RootObject()
        {
            
        }
        public List<Result> results { get; set; }
    }
}