﻿namespace DeKalbMedical.Data.DTO.DataPart.Google
{
    /// <summary>
    /// 
    /// </summary>
    public class ExtremePoint
    {
        public Location MaxPoint;
        public Location MinPoint;

        public ExtremePoint(Location minPoint, Location maxPoint)
        {
            this.MaxPoint = maxPoint;
            this.MinPoint = minPoint;
        }
    }
}
