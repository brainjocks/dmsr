﻿using Sitecore.ContentSearch;

namespace DeKalbMedical.Data.DTO.DataPart
{
    /// <summary>
    /// 
    /// </summary>
    public class ReferralServiceResultItem : SearchResultItem
    {
        [IndexField("_templatename")]
        public string TemplateName { get; set; }

        [IndexField("Service or Specialty Name")]
        public string ServiceOrSpecialtyName { get; set; }

        [IndexField("Contact Information")]
        public string ContactInformation { get; set; }

        [IndexField("Comprehensive Services")]
        public string ComprehensiveServices { get; set; }

        [IndexField("Referral Keywords")]
        public string ReferralKeywords { get; set; }
    }
}
