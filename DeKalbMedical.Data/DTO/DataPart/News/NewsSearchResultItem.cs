﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Converters;
using Sitecore.Data;

namespace DeKalbMedical.Data.DTO.DataPart.News
{
    public class NewsSearchResultItem// : Sitecore.ContentSearch.SearchTypes.SearchResultItem
    {
        [IndexField("press_release_title")]
        public string PressReleaseTitle { get; set; }

        [IndexField("meta_description")]
        public string PressMetaDescription { get; set; }

        [IndexField("press_release_date")]
        public DateTime PressReleaseDate { get; set; }

//        [IndexField("press_release_image")]
//        public Image PressReleaseImage { get; set; }

        [IndexField("press release content")]
        public string PressReleaseContent { get; set; }

        [IndexField("press_release_category")]
        public Guid[] PressReleaseCategories { get; set; }
        
        [IndexField("primary_service_line")]
        public Guid[] ServiceLines { get; set; }

        [IndexField("page_title")]
        public string LinkTitle { get; set; }

        [IndexField("urllink")]
        public string LinkHref { get; set; }

        [IndexField("_group")]
        [TypeConverter(typeof(IndexFieldIDValueConverter))]
        public virtual ID ItemId { get; set; }

        [IndexField("_name")]
        public string Name { get; set; }

        [IndexField("_templatename")]
        public string TemplateName { get; set; }
    }
}
