﻿namespace DeKalbMedical.Data.DTO.DataPart.DoctorOffices
{
    public class DoctorOffice
    {
        public string LocationId { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }
        
        public string IsPrimary { get; set; }
    }
}
