﻿using Newtonsoft.Json;

namespace DeKalbMedical.Data.DTO.DataPart.DoctorOffices
{
    public class DoctorOfficesResponse
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "Address")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "IsPrimary")]
        public string IsPrimary { get; set; }

        [JsonProperty(PropertyName = "Phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "Fax")]
        public string Fax { get; set; }

        [JsonProperty(PropertyName = "Geolocation")]
        public string Geolocation { get; set; }

        [JsonProperty(PropertyName = "Website")]
        public string Website { get; set; }
    }
}
