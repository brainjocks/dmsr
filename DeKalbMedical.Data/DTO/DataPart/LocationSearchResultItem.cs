﻿using System;
using Sitecore.ContentSearch;
using Sitecore.Data.Items;

namespace DeKalbMedical.Data.DTO.DataPart
{
    /// <summary>
    /// 
    /// </summary>
    public class LocationSearchResultItem : SearchResultItem
    {
        public LocationSearchResultItem(Item locationitem)
        {
            TemplateName = locationitem.TemplateName;
            LocationName = locationitem.Fields["Location Name"].Value;
        }

        public LocationSearchResultItem()
        {
            
        }

        [IndexField("_templatename")]
        public string TemplateName { get; set; }

        [IndexField("geolocation_latitude")]
        public double Latitude { get; set; }

        [IndexField("geolocation_longitude")]
        public double Longitude { get; set; }

        [IndexField("location_name")]
        public string LocationName { get; set; }

        [IndexField("zip")]
        public string Zip { get; set; }

        [IndexField("city")]
        public string City { get; set; }

        [IndexField("Services")]
        public string Services { get; set; }

        [IndexField("address")]
        public string Address { get; set; }

        [IndexField("Photo")]
        public string Photo { get; set; }

        [IndexField("Website")]
        public string Website { get; set; }

        [IndexField("Read_More_Link")]
        public string ReadMoreLink { get; set; }
    }
}
