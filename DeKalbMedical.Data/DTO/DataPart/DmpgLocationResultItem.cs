﻿using System.ComponentModel;
using DeKalbMedical.Data.Converters;
using Sitecore.ContentSearch;

namespace DeKalbMedical.Data.DTO.DataPart
{
    public  class DmpgLocationResultItem
    {
        [IndexField("_group")]
        public string Id { get; set; }

        [IndexField("page_title")]
        public string Name { get; set; }

        [TypeConverter(typeof(GeoLocationItemConverter))]
        [IndexField("geolocation")]
        public GeoLocationItem Geolocation { get; set; }

        [IndexField("geolocation_latitude")]
        public double Latitude { get; set; }

        [IndexField("geolocation_longitude")]
        public double Longitude { get; set; }

        [IndexField("city")]
        public string City { get; set; }

        [IndexField("zip")]
        public string Zip { get; set; }

        [IndexField("address_1")]
        public string AddressFirst { get; set; }

        [IndexField("address_2")]
        public string AddressSecond { get; set; }

        [IndexField("main_phone")]
        public string Phone { get; set; }

        [IndexField("main_fax")]
        public string Fax { get; set; }

        [IndexField("website")]
        public string Url { get; set; }

        [IndexField("Location_State")]
        public string State { get; set; }
    }
}
