﻿using DeKalbMedical.Data.DTO.DataPart.Google;

namespace DeKalbMedical.Data.DTO.DataPart
{
    public class GeoLocationItem : Location
    {
        public GeoLocationItem(double _lat, double _lng, string _placeId) : base(_lat, _lng)
        {
            placeId = _placeId;
        }

        public string placeId { get; set; }
    }
}
