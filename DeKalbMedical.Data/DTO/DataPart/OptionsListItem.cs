﻿namespace DeKalbMedical.Data.DTO.DataPart
{
    /// <summary>
    /// 
    /// </summary>
    public class OptionsListItem
    {
        public string Class { get; set; }

        public int Order { get; set; }

        public bool Selected { get; set; }

        public string Text { get; set; }

        public string Value { get; set; }
    }
}
