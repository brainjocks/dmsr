﻿using System.Collections.Generic;
using System.Linq;
using DeKalbMedical.Data.DTO.DataPart.DoctorOffices;
using DeKalbMedical.Data.DTO.DataPart.Google;
using Sitecore.ContentSearch;

namespace DeKalbMedical.Data.DTO.DataPart
{
    public class DoctorSearchResultItem
    {
        [IndexField("_templatename")]
        public string TemplateName { get; set; }

        [IndexField("page_title")]
        public string LinkTitle { get; set; }

        [IndexField("first_name")]
        public string FirstName { get; set; }

        [IndexField("last_name")]
        public string LastName { get; set; }

        [IndexField("doctor_gender")]
        public string Gender { get; set; }

        [IndexField("specialties")]
        public string[] Specialties { get; set; }

        [IndexField("doctor_specialties")]
        public string SpecialtiesText { get; set; }
        
        [IndexField("facilities")]
        public string[] Facilities { get; set; }

        [IndexField("doctor_cities")]
        public string[] Cities { get; set; }

        [IndexField("doctor_insurances")]
        public string[] Insurances { get; set; }

        [IndexField("doctor_insurance_plans")]
        public string[] InsurancePlans { get; set; }

        [IndexField("doctor_zips")]
        public string[] Zips { get; set; }

        [IndexField("doctor_location_ids")]
        public string[] LocationIds { get; set; }

        [IndexField("doctor_provider_types")]
        public string[] DoctorProviderTypes { get; set; }

        public bool IsEmployedDekalbPhysician 
        {
            get
            {
                if (this.DoctorProviderTypes == null)
                {
                    return false;
                }

                return this.DoctorProviderTypes.Any(type => type.Equals(Consts.Guids.EmployedDekalbPhysicianTypeId.ToShortID().ToString().ToLower()));
            } 
        }
        
        [IndexField("doctor_location_obj")]
        public string[] SerializedOffices { get; set; }

        [IndexField("languages_spoken")]
        public string[] Languages { get; set; }

        [IndexField("Photo")]
        public string Photo { get; set; }

        [IndexField("urllink")]
        public string LinkHref { get; set; }

        public List<DoctorOfficesResponse> Offices { get; set; }
    }
}
