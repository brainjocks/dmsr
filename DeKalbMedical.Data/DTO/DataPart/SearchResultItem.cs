﻿using System.Collections.Generic;
using Sitecore.ContentSearch;

namespace DeKalbMedical.Data.DTO.DataPart
{
    public class SearchResultItem
    {        
        [IndexField("page_title")]
        public string LinkTitle { get; set; }

        [IndexField("urllink")]
        public string LinkHref { get; set; }

        [IndexField("meta_description")]
        public string Metadescription { get; set; }

        [IndexField("_group")]
        public string ParentId { get; set; }

        [IndexField("_content")]
        public string Content { get; set; }

        [IndexField("rich_text_content")]
        public string RichTextContent { get; set; }

        [IndexField("keywords")]
        public string Keywords { get; set; }

        [IndexField("_name")]
        public string Name { get; set; }

        [IndexField("_path")]
        public string[] Path { get; set; }

        [IndexField("show_in_search_results")]
        public bool ShowInSearchResults { get; set; }
    }
}
