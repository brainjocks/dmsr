﻿using System.Collections.Generic;
using DeKalbMedical.Data.DTO.DataPart;

namespace DeKalbMedical.Data.DTO
{
    /// <summary>
    /// 
    /// </summary>
    public class LocationSearchResultsDto 
    {
        public List<LocationSearchResultItem> Items { get; set; }
        public PagedInfo Pager { get; set; }
    }
}
