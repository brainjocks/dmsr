﻿using System.Collections.Generic;
using DeKalbMedical.Data.DTO.DataPart;

namespace DeKalbMedical.Data.DTO
{
    public class SearchResultsDto 
    {
        public List<SearchResultItem> Items { get; set; }

        public PagedInfo Pager { get; set; }
    }
}
