﻿using System.Collections.Generic;
using DeKalbMedical.Data.DTO.DataPart;

namespace DeKalbMedical.Data.DTO
{
    public class DoctorSearchResultsDto
    {
        public List<DoctorSearchResultItem> Items { get; set; }

        public PagedInfo Pager { get; set; }
    }
}
