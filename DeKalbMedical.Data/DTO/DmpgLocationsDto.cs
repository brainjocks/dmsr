﻿using System.Collections.Generic;
using DeKalbMedical.Data.DTO.DataPart;

namespace DeKalbMedical.Data.DTO
{
    public class DmpgLocationsDto
    {
        public List<DmpgLocationResultItem> Items { get; set; }

        public PagedInfo Pager { get; set; }
    }
}
