﻿using System.Collections.Generic;
using DeKalbMedical.Data.DTO.DataPart;

namespace DeKalbMedical.Data.DTO
{
    /// <summary>
    /// 
    /// </summary>
    public class ReferralServicesDto
    {
        public List<ReferralServiceResultItem> Items { get; set; }

        public string Service { get; set; }

        public string Keyword { get; set; }

        public PagedInfo Pager { get; set; }
    }
}
