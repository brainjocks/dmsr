﻿using System;
using System.Collections.Generic;
using DeKalbMedical.Data.DTO.DataPart;
using DeKalbMedical.Data.DTO.DataPart.News;

namespace DeKalbMedical.Data.DTO
{
    public class NewsSearchResultsDto
    {
        public List<NewsSearchResultItem> Items { get; set; }
        public PagedInfo Pager { get; set; }
    }

    //public class NewsSearchResultDto
    //{
    //    public NewsSearchResultDto()
    //    {
    //    }

    //    public NewsSearchResultDto(NewsSearchResultItem newsSearchResultItem)
    //    {
    //        PressReleaseTitle = newsSearchResultItem.PressReleaseTitle;
    //        PressReleaseDate = newsSearchResultItem.PressReleaseDate;
    //        PressReleaseCategories = newsSearchResultItem.PressReleaseCategories;
    //        ServiceLines = newsSearchResultItem.ServiceLines;
    //        LinkTitle = newsSearchResultItem.LinkTitle;
    //        LinkHref = newsSearchResultItem.LinkHref;
    //        ItemId = newsSearchResultItem.ItemId;
    //    }
        
    //    public NewsSearchResultDto(string pressReleaseTitle, DateTime pressReleaseDate, Guid[] pressReleaseCategories, Guid[] serviceLines, string linkTitle, string linkHref, string id)
    //    {
    //        PressReleaseTitle = pressReleaseTitle;
    //        PressReleaseDate = pressReleaseDate;
    //        PressReleaseCategories = pressReleaseCategories;
    //        ServiceLines = serviceLines;
    //        LinkTitle = linkTitle;
    //        LinkHref = linkHref;
    //        ItemId = id;
    //    }

    //    public string ItemId { get; set; }

    //    public string PressReleaseTitle { get; set; }

    //    public DateTime PressReleaseDate { get; set; }

    //    public Guid[] PressReleaseCategories { get; set; }

    //    public Guid[] ServiceLines { get; set; }

    //    public string LinkTitle { get; set; }

    //    public string LinkHref { get; set; }
    //}
}
