﻿using DeKalbMedical.Data.DTO.DataPart.Google;

namespace DeKalbMedical.Data.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public interface IGoogleMapsService
    {
        Location GetCoordinatesByZipCode(string zipCode);
    }
}
