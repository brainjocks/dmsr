﻿
using System;
using System.Collections.Generic;
using DeKalbMedical.Data.DTO;
using DeKalbMedical.Data.DTO.DataPart;

namespace DeKalbMedical.Data.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISearchService
    {
        SearchResultsDto GetPaged(string keyword, string parentId, int pageIndex, int pageSize);

        LocationSearchResultsDto GetPagedLocations(string locationName, string service, string city, string zipcode, int range, int pageIndex, int pageSize);

        ReferralServicesDto GetPagedReferralServices(string service, string keyword, int pageIndex, int pageSize);

        DmpgLocationsDto GetDmpgLocations(string categories, string zip, string range, string categoryPath, int pageIndex, int pageSize);
   
        DoctorSearchResultsDto GetDoctors(string firstName, string lastName, string gender, string city, string specialty, string language, string facility, string zip, string range, string insurance, string insurancePlans, int pageIndex, int pageSize);

        List<OptionsListItem> GetPlansForInsurance(string insuranceId);
        NewsSearchResultsDto GetNews(string keywords, IList<Guid> categories, IList<Guid> serviceLines, DateTime? dateFrom, DateTime? dateTo, int pageIndex, int pageSize);
    }
}
