﻿using DeKalbMedical.Data.Abstractions;
using DeKalbMedical.Data.DTO.DataPart.Google;
using Newtonsoft.Json;
using RestSharp;
using Sitecore.Configuration;
using Sitecore.Diagnostics;

namespace DeKalbMedical.Data.Services
{
    /// <summary>
    /// Retrieves data from Google Maps API
    /// </summary>
    public class GoogleMapService : IGoogleMapsService
    {
        /// <summary>
        /// Gets Location DTO by Zip code using Google Maps API Geocode
        /// </summary>
        /// <param name="zipCode"></param>
        /// <returns></returns>
        public Location GetCoordinatesByZipCode(string zipCode)
        {
            Location result = null;

            var googleMapApiKey = Settings.GetSetting(Consts.Names.GoogleMapsApiKey);
            if (string.IsNullOrEmpty(googleMapApiKey))
            {
                Log.Error("GoogleMapService.GetCoordinatesByZipCode() - Cannot load the GoogleMapsApiKey from Configuration by path: configuration/sitecore/settings/setting name = 'Score.Google.ApiKey.Maps'", this);
            }
            else
            {
                var client = new RestClient("https://maps.googleapis.com");

                var request = new RestRequest("maps/api/geocode/json?address={zipcode}&key={key}", Method.POST);
                request.AddUrlSegment("zipcode", zipCode);
                request.AddUrlSegment("key", googleMapApiKey);

                var response = client.Execute<RootObject>(request);

                var responseObject = JsonConvert.DeserializeObject<GoogleResponse>(response.Content);

                if (responseObject.Results.Count > 0)
                {
                    result = responseObject.Results[0].Geometry.Location;
                }
                else
                {
                    Log.Error($"GoogleMapService.GetCoordinatesByZipCode() - response from Google API: '{responseObject.Status}'", this);
                }
            }

            return result;
        }
    }
}