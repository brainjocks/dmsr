﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DeKalbMedical.Data.Abstractions;
using DeKalbMedical.Data.DTO;
using DeKalbMedical.Data.DTO.DataPart;
using DeKalbMedical.Data.DTO.DataPart.DoctorOffices;
using DeKalbMedical.Data.DTO.DataPart.Google;
using DeKalbMedical.Data.DTO.DataPart.News;
using Newtonsoft.Json;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Debug = System.Diagnostics.Debug;

namespace DeKalbMedical.Data.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class SearchService : ISearchService
    {
        private readonly IGoogleMapsService _googleMapsService;

        private readonly ISearchIndex _searchIndex;

        public SearchService(IGoogleMapsService googleMapsService)
        {
            this._searchIndex = ContentSearchManager.GetIndex(Consts.Names.DekalbWebIndex);
            _googleMapsService = googleMapsService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="parentId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public SearchResultsDto GetPaged(string keyword, string parentId, int pageIndex, int pageSize)
        {
            List<SearchResultItem> documents = null;
            int totalRows = 0;
            try
            {
                using (var context = _searchIndex.CreateSearchContext())
                {
                    var predicate = BuildSearchPredicate(keyword);
                    var baseQuery = context.GetQueryable<SearchResultItem>().Where(predicate);

                    if (!string.IsNullOrEmpty(parentId))
                    {
                        parentId = IdHelper.NormalizeGuid(parentId);
                        baseQuery = baseQuery.Where(p => p.Path.Any(x => x.Equals(parentId)));
                    }

                    totalRows = baseQuery.Count();
                    var querable = baseQuery.Skip(pageSize * pageIndex).Take(pageSize);
                    var searchResults = querable.GetResults();
                    documents = searchResults.Hits.Select(h => h.Document).ToList();
                }
            }
            catch (Exception exception)
            {
                Log.Error(string.Format("Failed to find anything by {0} query. Details: {1}", keyword, exception.Message), exception, this);
            }

            var result = new SearchResultsDto
            {
                Items = documents,
                Pager = new PagedInfo
                {
                    Current = pageIndex,
                    Size = pageSize,
                    TotalRows = totalRows
                }
            };

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locationName"></param>
        /// <param name="service"></param>
        /// <param name="city"></param>
        /// <param name="zipcode"></param>
        /// <param name="range"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public LocationSearchResultsDto GetPagedLocations(string locationName, string service, string city, string zipcode, int range, int pageIndex, int pageSize)
        {
            var searchIndex = ContentSearchManager.GetIndex(Consts.Names.DekalbWebIndex);
            List<LocationSearchResultItem> documents = new List<LocationSearchResultItem>();
            List<LocationSearchResultItem> filterDocumentsByService = new List<LocationSearchResultItem>();
            var db = Database.GetDatabase("web");
            int totalRows = 0;
            try
            {
                if (service != "-1")
                {
                    var startLocationItem = db.GetItem("{BF734068-11E4-4DA1-9853-C2A77AAFFA72}");
                    var locationItems = startLocationItem.Axes.GetDescendants().Where(x => x.TemplateName == "Location");
                    foreach (var locationitem in locationItems)
                    {
                        if (((MultilistField) locationitem.Fields["Services"]).Contains(service))
                        {
                            filterDocumentsByService.Add(new LocationSearchResultItem(locationitem));
                        }
                    }
                    foreach (var locationsearchresultitem in filterDocumentsByService)
                    {
                        using (var context = searchIndex.CreateSearchContext())
                        {
                            var predicate = BuildSearchPredicate(locationsearchresultitem.LocationName, city, zipcode, range);
                            var baseQuery = context.GetQueryable<LocationSearchResultItem>().Where(predicate);

                            var querable = baseQuery.Skip(pageSize*pageIndex).Take(pageSize);
                            var searchResults = querable.GetResults();
                            foreach (var hit in searchResults.Hits)
                            {
                                if (locationsearchresultitem.LocationName == hit.Document.LocationName)
                                {
                                    documents.Add(hit.Document);
                                }
                            }
                        }
                    }

                    totalRows = documents.Count;
                }
                else
                {
                    using (var context = searchIndex.CreateSearchContext())
                    {
                        var predicate = BuildSearchPredicate(locationName, service,
                            city, zipcode, range);
                        var baseQuery = context.GetQueryable<LocationSearchResultItem>().Where(predicate);

                        //Locations Module: use predefined ID for the folder; this can be changed on the fly however
                        var locationsModuleFolderId =
                            IdHelper.NormalizeGuid("{BF734068-11E4-4DA1-9853-C2A77AAFFA72}");
                        baseQuery = baseQuery.Where(p => p.Path.Any(x => x.Equals(locationsModuleFolderId)));

                        totalRows = baseQuery.Count();
                        var querable = baseQuery.Skip(pageSize * pageIndex).Take(pageSize);
                        var searchResults = querable.GetResults();
                        documents = searchResults.Hits.Select(h => h.Document).ToList();
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error(string.Format("Failed to find locations. Details: locationName {0}; zip:{1}; exception: {2}", locationName, zipcode, exception.Message), exception, this);
            }

            var result = new LocationSearchResultsDto
            {
                Items = documents,
                Pager = new PagedInfo
                {
                    Current = pageIndex,
                    Size = pageSize,
                    TotalRows = totalRows
                }
            };

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public ReferralServicesDto GetPagedReferralServices(string service, string keyword, int pageIndex, int pageSize)
        {
            var searchIndex = ContentSearchManager.GetIndex(Consts.Names.DekalbWebIndex);
            List<ReferralServiceResultItem> documents = null;
            int totalRows = 0;
            try
            {
                using (var context = searchIndex.CreateSearchContext())
                {
                    var predicate = PredicateBuilder.True<ReferralServiceResultItem>();

                    predicate = predicate.And(p => p.TemplateName == "Referral Service");

                    if (!string.IsNullOrWhiteSpace(service))
                    {
                        predicate = predicate.And(p => p.ServiceOrSpecialtyName == service);
                    }

                    if (!string.IsNullOrWhiteSpace(keyword))
                    {
                        predicate = predicate.And(p =>
                            p.ReferralKeywords.Contains(keyword) ||
                            p.ComprehensiveServices.Contains(keyword) ||
                            p.ContactInformation.Contains(keyword));
                    }

                    var baseQuery = context.GetQueryable<ReferralServiceResultItem>().Where(predicate);

                    var referralServicesFolderId = IdHelper.NormalizeGuid("{59BE9982-F795-41E8-983B-2445E56F0EEC}");
                    baseQuery = baseQuery.Where(p => p.Path.Any(x => x.Equals(referralServicesFolderId)));

                    totalRows = baseQuery.Count();
                    var querable = baseQuery.Skip(pageSize * pageIndex).Take(pageSize);
                    var searchResults = querable.GetResults();
                    documents = searchResults.Hits.Select(h => h.Document).ToList();
                }
            }
            catch (Exception exception)
            {
                Log.Error(string.Format("Failed to find referral services. Details: service {0}; keyword:{1}; exception: {2}", service, keyword, exception.Message), exception, this);
            }

            var result = new ReferralServicesDto
            {
                Items = documents,
                Pager = new PagedInfo
                {
                    Current = pageIndex,
                    Size = pageSize,
                    TotalRows = totalRows
                }
            };

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        private Expression<Func<SearchResultItem, bool>> BuildSearchPredicate(string keyword)
        {
            // build search query
            var predicate = PredicateBuilder.True<SearchResultItem>();

            // Split query in subqueries separated by comma
            foreach (var subquery in keyword.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
            {
                var subPredicate = BuildSubQuery(subquery.Trim());
                predicate = predicate.Or(subPredicate);
            }

            return predicate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locationName"></param>
        /// <param name="service"></param>
        /// <param name="city"></param>
        /// <param name="zipcode"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        private Expression<Func<LocationSearchResultItem, bool>> BuildSearchPredicate(string locationName, string service, string city, string zipcode, int range)
        {
            city = string.IsNullOrWhiteSpace(city) ? "-1" : city;
            service = string.IsNullOrWhiteSpace(city) ? "-1" : service;

            var predicate = PredicateBuilder.True<LocationSearchResultItem>();

            predicate = predicate.And(p => p.TemplateName == "Location");

            if (!string.IsNullOrWhiteSpace(locationName))
            {
                predicate = predicate.And(p => p.LocationName.Contains(locationName));
            }

            if (!string.IsNullOrWhiteSpace(service) && !service.Equals("-1"))
            {
                predicate = predicate.And(p => p.Services.Contains(string.Concat(service, "|")));
            }

            if (!string.IsNullOrWhiteSpace(city) && !city.Equals("-1"))
            {
                predicate = predicate.And(p => p.City.Equals(city));
            }

            if (!string.IsNullOrWhiteSpace(zipcode) && range < 0)
            {
                predicate = predicate.And(p => p.Zip == zipcode);
            }
            else if (!string.IsNullOrWhiteSpace(zipcode) && range > 0)
            {
                var zipLocationPoint = _googleMapsService.GetCoordinatesByZipCode(zipcode);

                if (zipLocationPoint != null)
                {
                    double longitudesDiffForPoint = GetExtremeLongitudesDiffForPoint(zipLocationPoint, range);
                    double latitudesDiffForPoint = GetExtremeLatitudesDiffForPoint(zipLocationPoint, range);

                    var extremePoint = new ExtremePoint(
                        ValidatePoint(new Location(zipLocationPoint.lat - latitudesDiffForPoint, zipLocationPoint.lng - longitudesDiffForPoint)),
                        ValidatePoint(new Location(zipLocationPoint.lat + latitudesDiffForPoint, zipLocationPoint.lng + longitudesDiffForPoint))
                        );

                    predicate = predicate.And(p => p.Latitude != 0 && p.Latitude >= extremePoint.MinPoint.lat && p.Latitude <= extremePoint.MaxPoint.lat
                        && p.Longitude != 0 && p.Longitude >= extremePoint.MinPoint.lng && p.Longitude <= extremePoint.MaxPoint.lng);
                }
            }

            return predicate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locationName"></param>
        /// <param name="service"></param>
        /// <param name="city"></param>
        /// <param name="zipcode"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        private Expression<Func<LocationSearchResultItem, bool>> BuildSearchPredicate(string locationName, string city, string zipcode, int range)
        {
            city = string.IsNullOrWhiteSpace(city) ? "-1" : city;

            var predicate = PredicateBuilder.True<LocationSearchResultItem>();

            predicate = predicate.And(p => p.TemplateName == "Location");

            if (!string.IsNullOrWhiteSpace(locationName))
            {
                predicate = predicate.And(p => p.LocationName == locationName);
            }

            if (!city.Equals("-1"))
            {
                predicate = predicate.And(p => p.City.Equals(city));
            }

            if (!string.IsNullOrWhiteSpace(zipcode) && range < 0)
            {
                predicate = predicate.And(p => p.Zip == zipcode);
            }
            else if (!string.IsNullOrWhiteSpace(zipcode) && range > 0)
            {
                var zipLocationPoint = _googleMapsService.GetCoordinatesByZipCode(zipcode);

                if (zipLocationPoint != null)
                {
                    double longitudesDiffForPoint = GetExtremeLongitudesDiffForPoint(zipLocationPoint, range);
                    double latitudesDiffForPoint = GetExtremeLatitudesDiffForPoint(zipLocationPoint, range);

                    var extremePoint = new ExtremePoint(
                        ValidatePoint(new Location(zipLocationPoint.lat - latitudesDiffForPoint, zipLocationPoint.lng - longitudesDiffForPoint)),
                        ValidatePoint(new Location(zipLocationPoint.lat + latitudesDiffForPoint, zipLocationPoint.lng + longitudesDiffForPoint))
                        );

                    predicate = predicate.And(p => p.Latitude != 0 && p.Latitude >= extremePoint.MinPoint.lat && p.Latitude <= extremePoint.MaxPoint.lat
                        && p.Longitude != 0 && p.Longitude >= extremePoint.MinPoint.lng && p.Longitude <= extremePoint.MaxPoint.lng);
                }
            }

            return predicate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private Expression<Func<SearchResultItem, bool>> BuildSubQuery(string query)
        {
            var predicate = PredicateBuilder.True<SearchResultItem>();

            var term = query.Trim();
            predicate = predicate.And(
                p =>
                    (p.LinkTitle.Contains(term) || p.Metadescription.Contains(term) || p.Content.Contains(term) ||
                    p.RichTextContent.Contains(term) || p.Keywords.Contains(term) || p.Name.Contains(term)) && p.ShowInSearchResults);

            return predicate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        private static double GetExtremeLongitudesDiffForPoint(Location p1, double distance)
        {
            double num = Math.Cos(ToRadian(p1.lat)) * 6367.0;
            return ToDegree(distance / num);
        }

        private static double GetExtremeLatitudesDiffForPoint(Location p1, double distance)
        {
            return ToDegree(distance / 6367.0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private static double ToRadian(double val)
        {
            return val * (Math.PI / 180.0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private static double ToDegree(double val)
        {
            return val * 57.2957795130823;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private static Location ValidatePoint(Location point)
        {
            if (point.lat > 90.0)
                point.lat = 90.0 - (point.lat - 90.0);
            if (point.lat < -90.0)
                point.lat = -90.0 - (point.lat + 90.0);
            if (point.lng > 180.0)
                point.lng = point.lng - 180.0 - 180.0;
            if (point.lng < -180.0)
                point.lng = 180.0 + (point.lng + 180.0);
            return point;
        }

        public DmpgLocationsDto GetDmpgLocations(string categories, string zip, string range, string categoryPath, int pageIndex, int pageSize)
        {
            List<DmpgLocationResultItem> documents = null;
            int totalRows = 0;
            var locationIds = new List<string>();
            try
            {
                var dmpgCategories = Sitecore.Context.Database.GetItem(categoryPath);

                foreach (Item categoryItem in dmpgCategories.Children)
                {
                    if (categories != "all")
                    {
                        if (categoryItem.Fields["Value"].Value == categories)
                        {
                            GetDmpgLocationIds(categoryItem, locationIds);
                        }
                    }
                    else
                    {
                        GetDmpgLocationIds(categoryItem, locationIds);
                    }
                }

                using (var context = _searchIndex.CreateSearchContext())
                {
                    var predicate = DmpgLocationsQuery(zip, range, locationIds);
                    var baseQuery = context.GetQueryable<DmpgLocationResultItem>().Where(predicate);
                    totalRows = baseQuery.Count();
                    var querable = baseQuery.Skip(pageSize * pageIndex).Take(pageSize);
                    var searchResults = querable.GetResults();
                    documents = searchResults.Hits.Select(h => h.Document).ToList();
                }
            }
            catch (Exception exception)
            {
                Log.Error(string.Format("Failed to find DMPG Locations by {0} query. Details: {1}", categories, exception.Message), exception, this);
            }

            var result = new DmpgLocationsDto
            {
                Items = documents,
                Pager = new PagedInfo
                {
                    Current = pageIndex,
                    Size = pageSize,
                    TotalRows = totalRows
                }
            };

            return result;
        }

        public DoctorSearchResultsDto GetDoctors(string firstName, string lastName, string gender, string city, string specialty,
                                            string language, string facility, string zip, string range, string insurance, string insurancePlan, int pageIndex, int pageSize)
        {
            List<DoctorSearchResultItem> documents = null;
            int totalRows = 0;
            try
            {
                using (var context = _searchIndex.CreateSearchContext())
                {
                    var predicate = DoctorsQuery(firstName, lastName, gender, city, specialty, language, facility, zip, range, insurance, insurancePlan);
                    var baseQuery = context.GetQueryable<DoctorSearchResultItem>().Where(predicate);
                    totalRows = baseQuery.Count();
                    var querable = baseQuery.Skip(pageSize * pageIndex).Take(pageSize).OrderBy(h => h.LastName);
                    var searchResults = querable.GetResults();
                    documents = searchResults
                                    .Hits
                                    .Select(h => h.Document)
                                    .ToList();

                    foreach (var item in documents)
                    {
                        if (item.SerializedOffices != null)
                        {
                            item.Offices = new List<DoctorOfficesResponse>();
                            foreach (var json in item.SerializedOffices)
                            {
                                item.Offices.Add(JsonConvert.DeserializeObject<DoctorOfficesResponse>(json));
                            }
                        }

                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error(string.Format("Failed to find Doctors. Details: {0}", exception.Message), exception, this);
            }

            var result = new DoctorSearchResultsDto
            {
                Items = documents,
                Pager = new PagedInfo
                {
                    Current = pageIndex,
                    Size = pageSize,
                    TotalRows = totalRows
                }
            };

            return result;
        }

        public List<OptionsListItem> GetPlansForInsurance(string insuranceId)
        {
            var result = new List<OptionsListItem>();
            try
            {
                var insurance = Sitecore.Context.Database.GetItem(insuranceId);

                foreach (Item planItem in insurance.Children)
                {
                    result.Add(new OptionsListItem
                    {
                        Text = planItem.DisplayName,
                        Value = planItem.ID.ToString()
                    });
                }
            }
            catch (Exception exception)
            {
                Log.Error(string.Format("Failed to get plans for insuranceId {0}. Details: {1}", insuranceId, exception.Message), exception, this);
            }

            return result;
        }

        private Expression<Func<DoctorSearchResultItem, bool>> DoctorsQuery(string firstName, string lastName, string gender, string city,
                                                                             string specialty, string language, string facility,
                                                                             string zip, string range, string insurance, string insurancePlan)
        {
            int rangeInt = 0;
            if (!string.IsNullOrWhiteSpace(range))
            {
                int.TryParse(range, out rangeInt);
            }

            gender = string.IsNullOrWhiteSpace(gender) ? "-1" : gender;
            language = string.IsNullOrWhiteSpace(language) ? "-1" : language;
            facility = string.IsNullOrWhiteSpace(facility) ? "-1" : facility;
            specialty = string.IsNullOrWhiteSpace(specialty) ? "-1" : specialty;
            city = string.IsNullOrWhiteSpace(city) ? "-1" : city;
            insurance = string.IsNullOrWhiteSpace(insurance) ? "-1" : insurance;
            insurancePlan = string.IsNullOrWhiteSpace(insurancePlan) ? "-1" : insurancePlan;

            var predicate = PredicateBuilder.True<DoctorSearchResultItem>();

            predicate = predicate.And(p => p.TemplateName == "DeKalbMedical Physician Page");

            if (!string.IsNullOrWhiteSpace(firstName))
            {
                predicate = predicate.And(d => d.FirstName.Contains(firstName.ToLower()));
            }

            if (!string.IsNullOrWhiteSpace(lastName))
            {
                predicate = predicate.And(d => d.LastName.Contains(lastName.ToLower()));
            }

            if (!gender.Equals("-1"))
            {
                predicate = predicate.And(d => d.Gender.Equals(gender));
            }

            if (!language.Equals("-1"))
            {
                language = IdHelper.NormalizeGuid(language);
                predicate = predicate.And(d => d.Languages.Any(x => x.Equals(language)));
            }

            if (!facility.Equals("-1"))
            {
                facility = IdHelper.NormalizeGuid(facility);
                predicate = predicate.And(d => d.Facilities.Any(x => x.Equals(facility)));
            }

            if (!specialty.Equals("-1"))
            {
                specialty = IdHelper.NormalizeGuid(specialty);
                predicate = predicate.And(d => d.Specialties.Any(x => x.Equals(specialty)));
            }

            if (!city.Equals("-1"))
            {
                predicate = predicate.And(d => d.Cities.Any(x => x.Equals(city)));
            }

            if (!insurance.Equals("-1") && insurancePlan.Equals("-1"))
            {
                insurance = IdHelper.NormalizeGuid(insurance);
                predicate = predicate.And(d => d.Insurances.Any(x => x.Equals(insurance)));
            }

            if (!insurancePlan.Equals("-1"))
            {
                insurancePlan = IdHelper.NormalizeGuid(insurancePlan);
                predicate = predicate.And(d => d.InsurancePlans.Any(x => x.Equals(insurancePlan)));
            }

            if (!string.IsNullOrWhiteSpace(zip) && rangeInt <= 0)
            {
                predicate = predicate.And(d => d.Zips.Any(x => x.Equals(zip)));
            }

            else if (!string.IsNullOrWhiteSpace(zip) && rangeInt > 0)
            {
                var parentIds = GetLocationsByGeolocation(zip, rangeInt).Select(x => x.ParentId);

                var makesExpression = PredicateBuilder.False<DoctorSearchResultItem>();
                foreach (var parentId in parentIds)
                {
                    var tempTargetId = parentId;
                    makesExpression = makesExpression.Or(d => d.LocationIds.Any(x => x.Equals(tempTargetId)));
                }

                predicate = predicate.And(makesExpression);
            }

            return predicate;
        }

        private List<LocationSearchResultItem> GetLocationsByGeolocation(string zipcode, int range)
        {
            var searchIndex = ContentSearchManager.GetIndex(Consts.Names.DekalbWebIndex);
            List<LocationSearchResultItem> documents = null;
            int totalRows = 0;
            try
            {
                using (var context = searchIndex.CreateSearchContext())
                {
                    var predicate = BuildSearchPredicate(null, null, null, zipcode, range);
                    var baseQuery = context.GetQueryable<LocationSearchResultItem>().Where(predicate);

                    var searchResults = baseQuery.GetResults();
                    documents = searchResults.Hits.Select(h => h.Document).ToList();
                }
            }
            catch (Exception exception)
            {
                Log.Error(string.Format("Failed to find locations. Details: zip:{0}; exception: {1}", zipcode, exception.Message), exception, this);
            }

            return documents;
        }

        private Expression<Func<DmpgLocationResultItem, bool>> DmpgLocationsQuery(string zip, string range, ICollection<string> locationIds)
        {
            int rangeInt = 0;
            if (!string.IsNullOrWhiteSpace(range))
            {
                int.TryParse(range, out rangeInt);
            }

            var predicate = PredicateBuilder.True<DmpgLocationResultItem>();
            predicate = predicate.And(l => locationIds.Contains(l.Id));

            if (!string.IsNullOrWhiteSpace(zip) && rangeInt <= 0)
            {
                predicate = predicate.And(l => l.Zip.Equals(zip));
            }

            else if (!string.IsNullOrWhiteSpace(zip) && rangeInt > 0)
            {
                var zipLocationPoint = _googleMapsService.GetCoordinatesByZipCode(zip);

                if (zipLocationPoint != null)
                {
                    double longitudesDiffForPoint = GetExtremeLongitudesDiffForPoint(zipLocationPoint, rangeInt);
                    double latitudesDiffForPoint = GetExtremeLatitudesDiffForPoint(zipLocationPoint, rangeInt);

                    var extremePoint = new ExtremePoint(
                        ValidatePoint(new Location(zipLocationPoint.lat - latitudesDiffForPoint, zipLocationPoint.lng - longitudesDiffForPoint)),
                        ValidatePoint(new Location(zipLocationPoint.lat + latitudesDiffForPoint, zipLocationPoint.lng + longitudesDiffForPoint))
                        );

                    predicate = predicate.And(p => p.Latitude != 0 && p.Latitude >= extremePoint.MinPoint.lat && p.Latitude <= extremePoint.MaxPoint.lat
                        && p.Longitude != 0 && p.Longitude >= extremePoint.MinPoint.lng && p.Longitude <= extremePoint.MaxPoint.lng);
                }
            }

            return predicate;
        }

        private static void GetDmpgLocationIds(Item categoryItem, List<string> locationIds)
        {
            MultilistField multilistField = categoryItem.Fields["Options List"];
            if (multilistField != null)
            {
                var items = multilistField.GetItems();
                var tempLocationIds = new List<string>();

                foreach (var item in items)
                {
                    tempLocationIds.AddRange(item.Fields["Locations"].Value.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries));
                }

                locationIds.AddRange(tempLocationIds.Select(locationId => IdHelper.NormalizeGuid(locationId)));
            }
        }

        private static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double num = 3956.0;
            return num * 2.0 * Math.Asin(Math.Min(1.0, Math.Sqrt(Math.Pow(Math.Sin(DiffRadian(lat1, lat2) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin(DiffRadian(lng1, lng2) / 2.0), 2.0))));
        }

        public static double DiffRadian(double val1, double val2)
        {
            return ToRadian(val2) - ToRadian(val1);
        }


        public NewsSearchResultsDto GetNews(string keywords, IList<Guid> categories, IList<Guid> serviceLines, DateTime? dateFrom, DateTime? dateTo, int pageIndex, int pageSize)
        {
            var searchIndex = ContentSearchManager.GetIndex(Consts.Names.DekalbWebIndex);
            var documents = new List<NewsSearchResultItem>();
            int totalRows = 0;
            try
            {
                using (var context = searchIndex.CreateSearchContext())
                {
                    var predicate = PredicateBuilder.True<NewsSearchResultItem>();

                    predicate = predicate.And(p => p.TemplateName == "Dekalb News Details Page");

                    if (!String.IsNullOrEmpty(keywords))
                    {
                        var subPredicateByCombination = PredicateBuilder.False<NewsSearchResultItem>();
                        foreach (var subquery in keywords.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            var subPredicateByWords = PredicateBuilder.True<NewsSearchResultItem>();
                            foreach (var term in subquery.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                string term1 = term;
                                subPredicateByWords = subPredicateByWords.And(
                                   p =>
                                       p.PressReleaseTitle.Contains(term1) || p.PressReleaseContent.Contains(term1)
                                        || p.LinkTitle.Contains(term1) || p.Name.Contains(term1));
                            }

                            subPredicateByCombination = subPredicateByCombination.Or(subPredicateByWords);
                        }

                        predicate = predicate.And(subPredicateByCombination);
                    }

                    if (categories != null && categories.Any())
                    {
                        var categorySubpredicate = PredicateBuilder.True<NewsSearchResultItem>();

                        categorySubpredicate = PredicateBuilder.False<NewsSearchResultItem>();
                        foreach (var category in categories)
                        {
                            Guid category1 = category;
                            categorySubpredicate = categorySubpredicate.Or(x => x.PressReleaseCategories.Contains(category1));
                        }

                        predicate = predicate.And(categorySubpredicate);
                    }

                    if (serviceLines != null && serviceLines.Any())
                    {
                        var serviceLinesSubpredicate = PredicateBuilder.True<NewsSearchResultItem>();

                        serviceLinesSubpredicate = PredicateBuilder.False<NewsSearchResultItem>();
                        foreach (var serviceLine in serviceLines)
                        {
                            Guid serviceLine1 = serviceLine;
                            serviceLinesSubpredicate = serviceLinesSubpredicate.Or(x => x.ServiceLines.Contains(serviceLine1));
                        }

                        predicate = predicate.And(serviceLinesSubpredicate);
                    }


                    var query = context.GetQueryable<NewsSearchResultItem>().Where(predicate);

                    if (dateFrom != null)
                    {
                        query = query.Where(x => x.PressReleaseDate >= dateFrom);
                    }
                    if (dateTo != null)
                    {
                        query = query.Where(x => x.PressReleaseDate <= dateTo);
                    }

                    query = query.OrderByDescending(x => x.PressReleaseDate);

                    totalRows = query.Count();
                    var querable = query.Skip(pageSize * pageIndex).Take(pageSize);
                    var searchResults = querable.GetResults();
                    documents = searchResults.Hits.Select(h => h.Document).ToList();
                }
            }
            catch (Exception exception)
            {
                Log.Error(string.Format("Failed to find news by {0} query. Details: {1}", keywords, exception.Message), exception, this);
            }

            var result = new NewsSearchResultsDto
            {
                Items = documents,
                Pager = new PagedInfo
                {
                    Current = pageIndex,
                    Size = pageSize,
                    TotalRows = totalRows
                }
            };

            return result;
        }
    }
}