﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace DeKalbMedical.Data.Settings
{
    public static class NewsSettings
    {
        public static readonly ID ItemId = ID.Parse("{9DCBAEC1-B6A8-41C3-A5D1-679330EBE66E}");

        public const string NewsYearTemplateFieldName = "News Year Template";
        public const string NewsLandingPageFieldName = "News Landing Page";
        public const string NewsCategoriesSourceFieldName = "News Categories Source";

        private static Item _settingsItem;
        public static Item SettingsItem
        {
            get
            {
                if (_settingsItem != null) return _settingsItem;
                _settingsItem = Sitecore.Context.Database.GetItem(ItemId);
                return _settingsItem;
            }
        }

        public static ID NewsYearTemplate
        {
            get { return ID.Parse(SettingsItem[NewsYearTemplateFieldName]); }
        }
    }
}
