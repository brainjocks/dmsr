﻿using System;
using System.ComponentModel;
using System.Globalization;
using DeKalbMedical.Data.DTO.DataPart;

namespace DeKalbMedical.Data.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class GeoLocationItemConverter : TypeConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sourceType"></param>
        /// <returns></returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }

            return base.CanConvertFrom(context, sourceType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                return true;
            }
            return base.CanConvertTo(context, destinationType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var coords = (string)value;

            if (!string.IsNullOrWhiteSpace(coords))
            {
                var coordsSplit = coords.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                switch (coordsSplit.Length)
                {
                    case 2:
                        return new GeoLocationItem(Convert.ToDouble(coordsSplit[0], CultureInfo.InvariantCulture), Convert.ToDouble(coordsSplit[1], CultureInfo.InvariantCulture), string.Empty);
                    case 3:
                        return new GeoLocationItem(Convert.ToDouble(coordsSplit[0], CultureInfo.InvariantCulture), Convert.ToDouble(coordsSplit[1], CultureInfo.InvariantCulture), coordsSplit[2]);
                }
            }

            return null;
        }
    }
}
