﻿using Sitecore.Data;
using Sitecore.Publishing.Pipelines.Publish;

namespace DeKalbMedical.Data
{
    public static class Consts
    {
        public static class Fields
        {
            public const string SearchBoxPlaceholderTextField = "Search Box Placeholder Text";
            public const string SearchBoxUrlField = "Search Box Url";
            public const string SearchBoxParameterNameField = "Search Box Parameter Name";
            public const string SearchBoxErrorMessageField = "Search Box Error Message";
            public const string SearchBoxNameField = "Search Box Name";

            public const string ResultsItemsPerPageField = "Results Items Per Page";
            public const string ResultsErrorMessageField = "Results Error Message";
            public const string ResultsTitleField = "Results Title";
            public const string ResultsReadMoreField = "Read More Label";
            public const string ResultsLabelForSearchBoxField = "Results Label For Search Box";
            public const string ResultsLabelForDropDownField = "Results Label For Drop Down";
            public const string ResultsTextForSearchButtonField = "Results Text For Search Button";
            public const string ResultsMoreLinkTextField = "Results More Link Text";
            public const string ResultsSummaryTitleTemplateField = "Results Summary Title Template";
            public const string ResultsPagerTemplateField = "Results Pager Template";
            public const string ResultsNoResultsTemplateField = "Results No Results Template";
            public const string ResultsDropDownOptionsListField = "Results Drop Down Options List";
            public const string ResultsDropDownEmptyItemTextField = "Results Drop Down Empty Item Text";
            public const string ResultsSearchResultsUrlField = "Search Results Url";

            public const string ShowInSearchResults = "Show in Search Results";
            public const string DefaultTitle = "Default Title";
            public const string DefaultButtonNumberField = "Default Button Number";
            public const string ZoomValueField = "Zoom Value";
            public const string ButtonTextField = "Button Text";
            public const string GeoLocationField = "GeoLocation";
            public const string MarkerField = "Marker";
            public const string TextForMobileViewField = "Text For Mobile View";
            public const string GeolocationPlaceNameField = "Geolocation Place Name";        

            //news
            public const string PressReleaseTitleField = "Press Release Title";
            public const string PressReleaseDateField = "Press Release Date";
            public const string PressReleaseImageField = "Press Release Image";
            public const string PressReleaseContentField = "Press Release Content";
            public const string PressReleaseCategory = "Press Release Category";
            public const string PressReleaseServiceLine = "Service Line";
            public const string PressRelaseMetaDescriptionField = "Meta Description";
            //news feed
            public const string NewsFeedTitleLabelField = "Title Label";
            public const string NewsFeedReadMoreLabelField = "Read More Label";
            public const string NewsFeedPressRoomLabelField = "Press Room Label";
            public const string NewsFeedPressRoomUrlField = "Press Room Url";
            public const string NewsFeedNumberOfShowedNewsField = "Number Of Showed News";
            public const string NewsFeedServiceLinesField = "Service Lines";
            //news search
            public const string NewsSearchTitleLabelField = "Title Label";
            public const string NewsSearchKeywordLabelField = "Keyword Label";
            public const string NewsSearchKeywordPlaceholderTextField = "Keyword Placeholder Text";
            public const string NewsSearchCategoryLabelField = "Category Label";
            public const string NewsSearchSelectCategoryPlaceholderTextField = "Select Category Placeholder Text";
            public const string NewsSearchDateFromLabelField = "Date From Label";
            public const string NewsSearchDateToLabelField = "Date To Label";
            public const string NewsSearchDateFromPlaceholderTextField = "Date From Placeholder Text";
            public const string NewsSearchDateToPlaceholderTextField = "Date To Placeholder Text";
            public const string NewsSearchSearchButtonLabelField = "Search Button Label";
            //news search widget
            public const string NewsSearchCategories = "Categories";
            public const string NewsSearchYears = "Years";
            public const string NewsCategoryLabel = "Refine By Category Label";
            public const string NewsArchiveLabel = "Refine by Year Label";
            public const string NewsCategoryCheckboxLabel = "Show Refine by Category";
            public const string NewsArchiveCheckboxLabel = "Show Refine by Year";

            //location search
            public const string LocationNameLabelField = "Location Name Label";
            public const string SearchButtonLabelField = "Search Button Label";
            public const string LocationNamePlaceholderTextField = "Location Name Placeholder Text";
            public const string CityLabelField = "City Label";
            public const string ServicesLabelField = "Services Label";
            public const string ZipLabelField = "Zip Label";
            public const string ZipPlaceholderTextField = "Zip Placeholder Text";
            public const string RangeLabelField = "Range Label";
            public const string SelectRadiusLabelField = "Select a Radius Label";
            public const string SelectServiceLabelField = "Select a Service Label";
            public const string SelectCityLabelField = "Select a City Label";
            public const string EnterCriteriaField = "Enter criteria";

            public const string LastNameLabelField = "Last Name Label";
            public const string LastNamePlaceholderTextField = "Last Name Placeholder Text";
            public const string FirstNameLabelField = "First Name Label";
            public const string FirstNamePlaceholderTextField = "First Name Placeholder Text";
            public const string GenderLabelField = "Gender Label";
            public const string GenderPlaceholderTextField = "Gender Placeholder Text";
            public const string CityPlaceholderTextField = "City Placeholder Text";
            public const string SpecialtyLabelField = "Specialty Label";
            public const string SpecialtyPlaceholderTextField = "Specialty Placeholder Text";
            public const string LanguageLabelField = "Language Label";
            public const string LanguagePlaceholderTextField = "Language Placeholder Text";
            public const string AffiliatedLocationLabelField = "Affiliated Location Label";
            public const string AffiliatedLocationPlaceholderTextField = "Affiliated Location Placeholder Text";
            public const string RangePlaceholderTextField = "Range Placeholder Text";
            //public const string DirectionField = "Direction Url";
            public const string TotalResultsTemplateField = "Total Results Template";
            public const string WebsiteLinkTextField = "Website Link Text";
            public const string DirectionsLinkTextField = "Directions Link Text";
            public const string ReadMoreTextField = "Read More Link";

            public const string PleaseEnterACriteriaField = "Please Enter A Criteria";
            public const string PleaseEnterZipCodeField = "Please Enter Zip Code";

            public const string LanguagesOptionsListField = "Languages Options List";
            public const string SpecialitiesOptionsListField = "Specialities Options List";
            public const string RangesOptionsListField = "Ranges Options List";
            public const string CitiesOptionsListField = "Cities Options List";
            public const string GendersOptionsListField = "Genders Options List";
            public const string FacilitiesOptionsListField = "Facilities Options List";

            public const string ValueField = "Value";
            public const string DisplayedTextField = "Displayed Text";
            public const string SortOrderField = "__Sortorder";

            public const string DmpgOptionsListField = "DMPG Options List";
            public const string DefaultDmpgField = "Default DMPG";
            public const string DefaultRangeField = "Default Range";
            public const string DefaultZipField = "Default Zip";
            public const string CategoryPlaceholderTextField = "Category Placeholder Text";
            public const string CategoryLabelField = "Category Label";
            public const string DefaultPopupImageField = "Default Popup Image";
            public const string EmptyMapPlaceholderTextField = "Empty Map Placeholder Text";
            public const string DmpgLogoField = "DMPG Logo";
            public const string InsuranceLabelField = "Insurance Label";
            public const string InsurancePlaceholderTextField = "Insurance Placeholder Text";
            public const string InsurancesOptionsListField = "Insurances Options List";
            public const string InsurancePlansLabelField = "Insurance Plans Label";
            public const string InsurancePlansPlaceholderTextField = "Insurance Plans Placeholder Text";

            public const string SearchResultsUrlField = "Search Results Url";
            public const string OfficesTitleTextField = "Offices Title Text";
            public const string NewsSearchResultsUrl = "News Search Results URL";
        }

        public static class Guids
        {
            public static readonly ID SearchBoxTemplateId = ID.Parse("{2DE1D9BF-D5CB-4A77-B468-24D4C6FA6C6A}");
            public static readonly ID SearchResultsTemplateId = ID.Parse("{E9D31677-6121-419A-9EE8-F1C86937B65C}");
            public static readonly ID MlmpTemplateId = ID.Parse("{7C1F6621-A9A1-4848-8943-5D56AF9D71C1}");
            public static readonly ID MainLocationMapTemplateId = ID.Parse("{61CEA981-A40B-4971-A113-5E38026AED3D}");
            public static readonly ID DmpgLocationMapTemplateId = ID.Parse("{32973F14-3EB9-4440-8FB4-70CD55ADD259}");
            public static readonly ID LocationSearchTemplateId = ID.Parse("{7B752E88-D220-40FB-806C-1E384BD28E48}");
            public static readonly ID ReferralSearchTemplateId = ID.Parse("{A73AF28C-4270-431F-B099-12B3265B6A5B}");
            public static readonly ID LocationSearchResultsTemplateId = ID.Parse("{5F84E497-A76B-4B92-BAAA-928E102ED6C9}");
            public static readonly ID ReferralSearchResultsTemplateId = ID.Parse("{52FCE8FF-98F3-4BEE-ACBA-288629F1002F}");
            public static readonly ID DoctorSearchTemplateId = ID.Parse("{EBAB5E5B-3ACF-40A8-9965-8717FF938899}");
            public static readonly ID EmployedDekalbPhysicianTypeId = ID.Parse("{4BFDED47-3C41-4A78-92C1-C1A1C65515F1}");
            public static readonly ID NewsPageTemplateId = ID.Parse("{6A356D44-5B0E-4745-B2A6-BD2D80226752}");
            public static readonly ID NewsFeedTemplateId = ID.Parse("{EB9793F9-5E45-4557-AC5E-9C9C9393F53C}");
            public static readonly ID NewsSearchTemplateId = ID.Parse("{BFB9DC64-41F4-42FF-A7BE-E3CC35270DB8}");
            public static readonly ID DoctorSearchResultsTemplateId = ID.Parse("{DC7307F7-0DAC-4EE5-B053-292260FFC992}");
        }

        public static class Names
        {
            public const string DekalbWebIndex = "dekalb_web_index";
            public const string GoogleMapsApiKey = "Score.Google.ApiKey.Maps";
        }
    }
}
