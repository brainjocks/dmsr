﻿using System.Collections.Generic;
using System.Linq;
using DeKalbMedical.Data.DTO.DataPart;
using Score.Data.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace DeKalbMedical.Data.DatasourceItems
{
    /// <summary>
    /// 
    /// </summary>
    public class ReferralSearchDatasource : CustomItem
    {
        public string SelectServiceFieldName = "Select Service";
        public string EnterKeywordFieldName = "Enter keyword";
        public string SearchByLabelFieldName = "Search By Label";
        public string SearchServiceButtonLabelFieldName = "Search Service Button Label";
        public string SearchKeywordButtonLabelFieldName = "Search Keyword Button Label";
        public string KeywordPlaceholderTextFieldName = "Keyword Placeholder Text";
        public string KeywordLabelFieldName = "Keyword Label";
        public string SelectServiceLabelFieldName = "Select a Service Label";
        public string SearchResultsUrlField = "Search Results Url";

        public string SelectService
        {
            get { return InnerItem.Fields[SelectServiceFieldName].Value; }
        }

        public string EnterKeyword
        {
            get { return InnerItem.Fields[EnterKeywordFieldName].Value; }
        }

        public string SearchByLabel
        {
            get { return InnerItem.Fields[SearchByLabelFieldName].Value; }
        }

        public string SearchServiceButtonLabel
        {
            get { return InnerItem.Fields[SearchServiceButtonLabelFieldName].Value; }
        }

        public string SearchKeywordButtonLabel
        {
            get { return InnerItem.Fields[SearchKeywordButtonLabelFieldName].Value; }
        }

        public string KeywordPlaceholderText
        {
            get { return InnerItem.Fields[KeywordPlaceholderTextFieldName].Value; }
        }

        public string KeywordLabel
        {
            get { return InnerItem.Fields[KeywordLabelFieldName].Value; }
        }

        public string SelectServiceLabel
        {
            get { return InnerItem.Fields[SelectServiceLabelFieldName].Value; }
        }

        /// <summary>
        /// Same implementation as for SearchBox component. Most likely will be moved to common template for both components
        /// </summary>
        public string SearchUrl
        {
            get
            {
                var searchPage = string.Empty;
                var searchResultsField = (ReferenceField)this.InnerItem.Fields[SearchResultsUrlField];

                if (searchResultsField != null)
                {
                    var searchPageItem = searchResultsField.TargetItem;
                    if (searchPageItem != null)
                    {
                        searchPage = LinkManager.GetItemUrl(searchPageItem);
                        if (string.IsNullOrEmpty(searchPage))
                        {
                            Log.Error("ReferralSearchDatasource - Cannot load SearchUrl", this);
                        }
                    }
                    else
                    {
                        Log.Error(string.Concat("ReferralSearchDatasource - Cannot load SearchUrl Item, Actual value - ", searchResultsField.Value), this);
                    }
                }
                else
                {
                    Log.Error("ReferralSearchDatasource - Cannot load SearchUrl field", this);
                }

                return searchPage;
            }
        }

        public ReferralSearchDatasource(Item innerItem) : base(innerItem)
        {
        }

        /// <summary>
        /// TODO: use dynamic source
        /// </summary>
        public List<OptionsListItem> Services
        {
            get
            {
                var result = new List<OptionsListItem>();

                var serviceFolder = Sitecore.Context.Database.GetItem("/sitecore/content/DeKalbMedical/Data/Referral Services");
                if (serviceFolder != null)
                {
                    var services = Sitecore.Context.Database.SelectItems(string.Concat(serviceFolder.Paths.Path, "//*[@@templatename='Referral Service']"));
                    if (services != null && services.Any())
                    {
                        foreach (var service in services)
                        {
                            var prefixLevel = service.Axes.Level - serviceFolder.Axes.Level - 1;
                            var value = service.Fields["Service or Specialty Name"].Value;
                            result.Add(new OptionsListItem
                            {
                                Class = prefixLevel > 0 ? "subcategory" : string.Empty,
                                Text = value,
                                Value = value
                            });
                        }
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="datasourceItem"></param>
        /// <returns></returns>
        public static bool TryParse(Item item, out ReferralSearchDatasource datasourceItem)
        {
            if (item == null || item.IsDerived(Consts.Guids.ReferralSearchTemplateId) == false)
            {
                datasourceItem = null;
            }
            else
            {
                datasourceItem = new ReferralSearchDatasource(item);
            }

            return datasourceItem != null;
        }
    }
}
