﻿using Score.UI.Data.DatasourceItems;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace DeKalbMedical.Data.DatasourceItems
{
    public class DekalbPanelItem: AnchorableItem
    {
        public new static readonly ID Template = new ID("{4E4F4372-F670-4C49-A84B-10D24F391BD2}");
        public const string TitleFieldName = "Panel Title";
        public const string SubTitleFieldLabel = "Panel Subtitle Label";
        public const string SubTitleFieldText = "Panel Subtitle Text";
        public const string FooterFieldName = "Panel Footer";
        public const string TooltipFieldName = "Tooltip";

        public TextField TitleField { get; private set; }

        public TextField SubTitleFieldLbl { get; private set; }

        public TextField SubTitleField { get; private set; }

        public TextField FooterField { get; private set; }

        public virtual bool HasTitle
        {
            get
            {
                if (this.TitleField != null)
                    return !string.IsNullOrWhiteSpace(this.TitleField.Value);
                return false;
            }
        }

        public virtual bool HasSubTitleLabel
        {
            get
            {
                if (this.SubTitleFieldLbl != null)
                    return !string.IsNullOrWhiteSpace(this.SubTitleFieldLbl.Value);
                return false;
            }
        }

        public virtual bool HasSubTitle
        {
            get
            {
                if (this.SubTitleField != null)
                    return !string.IsNullOrWhiteSpace(this.SubTitleField.Value);
                return false;
            }
        }

        public virtual bool HasFooter
        {
            get
            {
                if (this.FooterField != null)
                    return !string.IsNullOrWhiteSpace(this.FooterField.Value);
                return false;
            }
        }

        public DekalbPanelItem(Item item)
            : base(item)
        {
            this.TitleField = (TextField)item.Fields[TitleFieldName];
            this.SubTitleFieldLbl = (TextField)item.Fields[SubTitleFieldLabel];
            this.SubTitleField = (TextField)item.Fields[SubTitleFieldText];
            this.FooterField = (TextField)item.Fields[FooterFieldName];
        }

        public static implicit operator DekalbPanelItem(Item innerItem)
        {
            if (innerItem == null)
                return (DekalbPanelItem)null;
            return new DekalbPanelItem(innerItem);
        }

        public static implicit operator Item(DekalbPanelItem customItem)
        {
            if (customItem == null)
                return (Item) null;
            return customItem.InnerItem;
        }
    }
}
