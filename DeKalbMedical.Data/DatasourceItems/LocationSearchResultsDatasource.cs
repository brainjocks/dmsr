﻿using System;
using Score.Data.Extensions;
using Sitecore.Data.Items;

namespace DeKalbMedical.Data.DatasourceItems
{
    /// <summary>
    /// 
    /// </summary>
    public class LocationSearchResultsDatasource : CustomItem
    {
        public string DirectionUrlFieldName = "Direction Url";
        public string ReadMoreFieldName = "Read More Link";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="innerItem"></param>
        public LocationSearchResultsDatasource(Item innerItem) : base(innerItem)
        {
        }

        public string ResultsErrorMessage
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsErrorMessageField].Value; }
        }

        public int ResultsPerPage
        {
            get { return Convert.ToInt32(this.InnerItem.Fields[Consts.Fields.ResultsItemsPerPageField].Value); }
        }

        public string ResultsTitle
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsTitleField].Value; }
        }

        public string ResultsMoreLinkText
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsMoreLinkTextField].Value; }
        }

        public string WebsiteLinkText
        {
            get { return this.InnerItem.Fields[Consts.Fields.WebsiteLinkTextField].Value; }
        }

        public string DirectionsLinkText
        {
            get { return this.InnerItem.Fields[Consts.Fields.DirectionsLinkTextField].Value; }
        }

        public string ReadMoreLinkText
        {
            get { return this.InnerItem.Fields[Consts.Fields.ReadMoreTextField].Value; }
        }

        public string ResultsPagerTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsPagerTemplateField].Value; }
        }

        public string ResultsNoResultsTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsNoResultsTemplateField].Value; }
        }

        public string DirectionUrl
        {
            get { return this.InnerItem.Fields[DirectionUrlFieldName].Value; }
        }

        public string TotalResultsTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.TotalResultsTemplateField].Value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="datasourceItem"></param>
        /// <returns></returns>
        public static bool TryParse(Item item, out LocationSearchResultsDatasource datasourceItem)
        {
            if (item == null || item.IsDerived(Consts.Guids.LocationSearchResultsTemplateId) == false)
            {
                datasourceItem = null;
            }
            else
            {
                datasourceItem = new LocationSearchResultsDatasource(item);
            }

            return datasourceItem != null;
        }
    }
}
