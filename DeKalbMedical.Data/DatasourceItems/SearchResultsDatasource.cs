﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeKalbMedical.Data.DTO.DataPart;
using Score.Data.Extensions;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace DeKalbMedical.Data.DatasourceItems
{
    public class SearchResultsDatasource : SearchBoxDatasource
    {
        public SearchResultsDatasource(Item item) : base(item) { }

        public string ResultsErrorMessage
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsErrorMessageField].Value; }
        }

        public int ResultsPerPage
        {
            get { return Convert.ToInt32(this.InnerItem.Fields[Consts.Fields.ResultsItemsPerPageField].Value); }
        }

        public string ResultsTitle
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsTitleField].Value; }
        }

        public string ResultsLabelForSearchBox
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsLabelForSearchBoxField].Value; }
        }

        public string ResultsLabelForDropDown
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsLabelForDropDownField].Value; }
        }

        public string ResultsTextForSearchButton
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsTextForSearchButtonField].Value; }
        }

        public string ResultsMoreLinkText
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsMoreLinkTextField].Value; }
        }

        public string ResultsSummaryTitleTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsSummaryTitleTemplateField].Value; }
        }

        public string ResultsPagerTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsPagerTemplateField].Value; }
        }

        public string ResultsNoResultsTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsNoResultsTemplateField].Value; }
        }

        public List<OptionsListItem> ResultsDropDownOptionsList
        {
            get
            {
                var selectedIdsArray = this.InnerItem.Fields[Consts.Fields.ResultsDropDownOptionsListField].Value.Split('|');
                int i = 0;
                return (selectedIdsArray.Select(selectedId => Sitecore.Context.Database.GetItem(selectedId))
                    .Where(item => item[Consts.Fields.ShowInSearchResults] == "1")
                    .Select(item => new OptionsListItem { Order = i++, Value = item.ID.ToString(), Text = item[Consts.Fields.DefaultTitle] }))
                    .ToList();
            }
        }

        public string ResultsDropDownEmptyItemText
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsDropDownEmptyItemTextField].Value; }
        }
        

        public static bool TryParse(Item item, out SearchResultsDatasource parsedItem)
        {
            parsedItem = item == null || item.IsDerived(Consts.Guids.SearchResultsTemplateId) == false
                ? null
                : new SearchResultsDatasource(item);

            return parsedItem != null;
        }

        public static implicit operator SearchResultsDatasource(Item innerItem)
        {
            return innerItem != null && innerItem.IsDerived(Consts.Guids.SearchResultsTemplateId)
                ? new SearchResultsDatasource(innerItem)
                : null;
        }

        public static implicit operator Item(SearchResultsDatasource customItem)
        {
            return customItem != null
                ? customItem.InnerItem
                : null;
        }
    }
}
