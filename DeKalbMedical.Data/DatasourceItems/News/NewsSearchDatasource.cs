﻿using System.Collections.Generic;
using System.Linq;
using Score.Custom.FieldTypes;
using Score.Data.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace DeKalbMedical.Data.DatasourceItems.News
{
    public class NewsSearchDatasource : CustomItem
    {
        public NewsSearchDatasource(Item item) : base(item) { }

        public string TitleLabel
        {
            get { return InnerItem.Fields[Consts.Fields.NewsSearchTitleLabelField].Value; }
        }
        public string KeywordLabel
        {
            get { return InnerItem.Fields[Consts.Fields.NewsSearchKeywordLabelField].Value; }
        }
        public string KeywordPlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.NewsSearchKeywordPlaceholderTextField].Value; }
        }
        public string CategoryLabel
        {
            get { return InnerItem.Fields[Consts.Fields.NewsSearchCategoryLabelField].Value; }
        }
        public string SelectCategoryPlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.NewsSearchSelectCategoryPlaceholderTextField].Value; }
        }
        public string DateFromLabel
        {
            get { return InnerItem.Fields[Consts.Fields.NewsSearchDateFromLabelField].Value; }
        }
        public string DateFromPlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.NewsSearchDateFromPlaceholderTextField].Value; }
        }
        public string DateToLabel
        {
            get { return InnerItem.Fields[Consts.Fields.NewsSearchDateToLabelField].Value; }
        }
        public string DateToPlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.NewsSearchDateToPlaceholderTextField].Value; }
        }
        public string SearchButtonLabel
        {
            get { return InnerItem.Fields[Consts.Fields.NewsSearchSearchButtonLabelField].Value; }
        }
       
        public int ResultsItemsPerPage
        {
            get
            {
                NumberField field = InnerItem.Fields[Consts.Fields.ResultsItemsPerPageField];
                return field.ValueAsInt ?? 10; // default if field not set
            }
        }
        public List<KeyValuePair<string, string>> Categories
        {
            get
            {
                var result = new List<KeyValuePair<string, string>>();
                var categories = Sitecore.Context.Database.SelectItems("/sitecore/content/DeKalbMedical/Data/News/News Categories/*[@@templatename='News Category']");
                if (categories != null && categories.Any())
                {
                    result.AddRange(categories.Select(x => new KeyValuePair<string, string>(x.ID.ToString().Replace("-","").Replace("{", "").Replace("}",""), x.Fields["Name"].Value)));
                }
                return result;
            }
        }


        public string ResultsTitle
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsTitleField].Value; }
        }
        public string ResultsMoreLabel
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsReadMoreField].Value; }
        }
        public string ResultsMoreLinkText
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsMoreLinkTextField].Value; }
        }
        public string ResultsErrorMessage
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsErrorMessageField].Value; }
        }
        public string ResultsPagerTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsPagerTemplateField].Value; }
        }
        public string ResultsNoResultsTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsNoResultsTemplateField].Value; }
        }
        public string TotalResultsTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.TotalResultsTemplateField].Value; }
        }

        public string ReadMoreLabel { get { return this.InnerItem.Fields[Consts.Fields.ResultsReadMoreField].Value; } }
        public string SearchUrl
        {
            get
            {
                var searchPage = string.Empty;
                LinkField searchResultsField = this.InnerItem.Fields[Consts.Fields.ResultsSearchResultsUrlField];

                if (searchResultsField != null)
                {
                    var searchPageItem = searchResultsField.TargetItem;
                    if (searchPageItem != null)
                    {
                        searchPage = LinkManager.GetItemUrl(searchPageItem);
                        if (string.IsNullOrEmpty(searchPage))
                        {
                            Log.Error("NewsSearchDatasource - Cannot load SearchUrl", this);
                        }
                    }
                    else
                    {
                        Log.Error(string.Concat("NewsSearchDatasource - Cannot load SearchUrl Item, Actual value - ", searchResultsField.Value), this);
                    }
                }
                else
                {
                    Log.Error("NewsSearchDatasource - Cannot load SearchUrl field", this);
                }

                return searchPage;
            }
        }

        public static bool TryParse(Item item, out NewsSearchDatasource parsedItem)
        {
            parsedItem = item == null || item.IsDerived(Consts.Guids.NewsSearchTemplateId) == false
                ? null
                : new NewsSearchDatasource(item);
        
            return parsedItem != null;
        }
        
        public static implicit operator NewsSearchDatasource(Item innerItem)
        {
            return innerItem != null && innerItem.IsDerived(Consts.Guids.NewsSearchTemplateId)
                ? new NewsSearchDatasource(innerItem)
                : null;
        }
        
        public static implicit operator Item(NewsSearchDatasource customItem)
        {
            return customItem != null
                ? customItem.InnerItem
                : null;
        }
    }
}
