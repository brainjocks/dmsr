﻿using System;
using Score.Data.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;

namespace DeKalbMedical.Data.DatasourceItems.News
{
    public class NewsDetailsDatasource : CustomItem
    {
        public NewsDetailsDatasource(Item innerItem) : base(innerItem) { }

        public string PressReleaseTitle
        {
            get { return this.InnerItem.Fields[Consts.Fields.PressReleaseTitleField].Value; }
        }

        public string PressMetaDescription
        {
            get { return this.InnerItem.Fields[Consts.Fields.PressRelaseMetaDescriptionField].Value; }
        }

        public DateTime PressReleaseDate
        {
            get
            {
                DateField field = this.InnerItem.Fields[Consts.Fields.PressReleaseDateField];
                return field.DateTime;
            }
        }

//        public string PressReleaseImageUrl
//        {
//            get { return FieldRenderer.Render(this.InnerItem, Consts.Fields.PressReleaseDateField); }
//        }

        public static bool TryParse(Item item, out NewsDetailsDatasource parsedItem)
        {
            parsedItem = item == null || !item.IsDerived(Consts.Guids.NewsPageTemplateId)
                ? null
                : new NewsDetailsDatasource(item);

            return parsedItem != null;
        }

        public static implicit operator NewsDetailsDatasource(Item innerItem)
        {
            return innerItem != null && innerItem.IsDerived(Consts.Guids.NewsPageTemplateId)
                ? new NewsDetailsDatasource(innerItem)
                : null;
        }

        public static implicit operator Item(NewsDetailsDatasource customItem)
        {
            return customItem != null
                ? customItem.InnerItem
                : null;
        }
    }
}