﻿using System;
using Score.Custom.FieldTypes;
using Score.Data.Extensions;
using Sitecore.Data.Items;

namespace DeKalbMedical.Data.DatasourceItems.News
{
    /// <summary>
    /// 
    /// </summary>
    public class NewsSearchResultsDatasource : CustomItem
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="innerItem"></param>
        public NewsSearchResultsDatasource(Item innerItem) : base(innerItem)
        {
        }

        public int ResultsItemsPerPage
        {
            get
            {
                NumberField field = InnerItem.Fields[Consts.Fields.ResultsItemsPerPageField];
                return field.ValueAsInt ?? 10; // default if field not set
            }
        }
        public string ResultsTitle
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsTitleField].Value; }
        }
        public string ResultsMoreLabel
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsReadMoreField].Value; }
        }
        public string ResultsMoreLinkText
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsMoreLinkTextField].Value; }
        }
        public string ResultsErrorMessage
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsErrorMessageField].Value; }
        }
        public string ResultsPagerTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsPagerTemplateField].Value; }
        }
        public string ResultsNoResultsTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsNoResultsTemplateField].Value; }
        }
        public string TotalResultsTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.TotalResultsTemplateField].Value; }
        }

        public string ReadMoreLabel { get { return this.InnerItem.Fields[Consts.Fields.ResultsReadMoreField].Value; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="datasourceItem"></param>
        /// <returns></returns>
        public static bool TryParse(Item item, out NewsSearchResultsDatasource datasourceItem)
        {
            if (item == null || item.IsDerived(Consts.Guids.NewsSearchTemplateId) == false)
            {
                datasourceItem = null;
            }
            else
            {
                datasourceItem = new NewsSearchResultsDatasource(item);
            }

            return datasourceItem != null;
        }
    }
}
