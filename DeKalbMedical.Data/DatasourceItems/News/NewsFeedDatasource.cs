﻿using Score.Custom.FieldTypes;
using Score.Data.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace DeKalbMedical.Data.DatasourceItems.News
{
    public class NewsFeedDatasource : CustomItem
    {
        public NewsFeedDatasource(Item innerItem) : base(innerItem)
        {

        }

        public string TitleLabel
        {
            get { return InnerItem.Fields[Consts.Fields.NewsFeedTitleLabelField].Value; }
        }

        public string PressRoomLabel
        {
            get { return InnerItem.Fields[Consts.Fields.NewsFeedPressRoomLabelField].Value; }
        }

        public int NumberOfShowedNews
        {
            get
            {
                NumberField field = InnerItem.Fields[Consts.Fields.NewsFeedNumberOfShowedNewsField];
                return field.ValueAsInt ?? 2; // default if field not set
            }
        }

        public Item[] ServiceLines
        {
            get
            {
                MultilistField field = InnerItem.Fields[Consts.Fields.NewsFeedServiceLinesField];
                return field.GetItems();
            }
        }

        public string ReadMoreLabel
        {
            get { return InnerItem.Fields[Consts.Fields.NewsFeedReadMoreLabelField].Value; }
        }

        public string PressRoomUrl
        {
            get
            {
                Assert.ArgumentNotNull(this.InnerItem, "item");

                if (!this.InnerItem.Template.Name.Equals("News Feed"))
                {
                    return null;
                }

                Assert.IsNotNull(this.InnerItem.Fields[Consts.Fields.NewsFeedPressRoomUrlField], "Field is null");
                var url = string.Empty;
                LinkField field = this.InnerItem.Fields[Consts.Fields.NewsFeedPressRoomUrlField];

                if (field != null)
                {
                    var targetItem = field.TargetItem;
                    if (targetItem != null)
                    {
                        url = LinkManager.GetItemUrl(targetItem);
                        if (string.IsNullOrEmpty(url))
                        {
                            Log.Error("NewsFeedDatasource - Cannot load PressRoomUrl", this);
                        }
                    }
                    else
                    {
                        Log.Error(string.Concat("NewsFeedDatasource - Cannot load PressRoomUrl Item, Actual value - ", field.Value + this.InnerItem.ID.ToString()), this);
                    }
                }
                else
                {
                    Log.Error("NewsFeedDatasource - Cannot load PressRoomUrl field", this);
                }

                return url;
            }
        }
        
        public static bool TryParse(Item item, out NewsFeedDatasource datasourceItem)
        {
            datasourceItem = item == null || !item.IsDerived(Consts.Guids.NewsFeedTemplateId)
                ? null
                : new NewsFeedDatasource(item);

            return datasourceItem != null;
        }

        public static implicit operator NewsFeedDatasource(Item innerItem)
        {
            return innerItem != null && innerItem.IsDerived(Consts.Guids.NewsFeedTemplateId)
                ? new NewsFeedDatasource(innerItem)
                : null;
        }

        public static implicit operator Item(NewsFeedDatasource customItem)
        {
            return customItem != null
                ? customItem.InnerItem
                : null;
        }
    }
}
