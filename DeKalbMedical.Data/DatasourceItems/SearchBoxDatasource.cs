﻿using Score.Data.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace DeKalbMedical.Data.DatasourceItems
{
    public class SearchBoxDatasource : CustomItem
    {
        public SearchBoxDatasource(Item item) : base(item) { }

        public string PlaceholderText
        {
            get { return this.InnerItem.Fields[Consts.Fields.SearchBoxPlaceholderTextField].Value; }
        }

        public string SearchUrl
        {
            get
            {
                var searchPage = string.Empty;
                var searchResultsField = (ReferenceField)this.InnerItem.Fields[Consts.Fields.SearchBoxUrlField];

                if (searchResultsField != null)
                {
                    var searchPageItem = searchResultsField.TargetItem;
                    if (searchPageItem != null)
                    {
                        searchPage = LinkManager.GetItemUrl(searchPageItem);
                        if (string.IsNullOrEmpty(searchPage))
                        {
                            Log.Error("SearchBoxDatasource - Cannot load SearchUrl", this);
                        }
                    }
                    else
                    {
                        Log.Error("SearchBoxDatasource - Cannot load SearchUrl Item, Actual value - " + searchResultsField.Value, this);
                    }
                }
                else
                {
                    Log.Error("SearchBoxDatasource - Cannot load SearchUrl field", this);
                }

                return searchPage;
            }
        }

        public string SearchParameterName
        {
            get { return this.InnerItem.Fields[Consts.Fields.SearchBoxParameterNameField].Value; }
        }

        public string ErrorMessage
        {
            get { return this.InnerItem.Fields[Consts.Fields.SearchBoxErrorMessageField].Value; }
        }

        public string SearchBoxName
        {
            get { return this.InnerItem.Fields[Consts.Fields.SearchBoxNameField].Value; }
        }

        public static bool TryParse(Item item, out SearchBoxDatasource parsedItem)
        {
            parsedItem = item == null || item.IsDerived(Consts.Guids.SearchBoxTemplateId) == false
                ? null
                : new SearchBoxDatasource(item);

            return parsedItem != null;
        }

        public static implicit operator SearchBoxDatasource(Item innerItem)
        {
            return innerItem != null && innerItem.IsDerived(Consts.Guids.SearchBoxTemplateId)
                ? new SearchBoxDatasource(innerItem)
                : null;
        }

        public static implicit operator Item(SearchBoxDatasource customItem)
        {
            return customItem != null
                ? customItem.InnerItem
                : null;
        }

    }
}
