﻿using System.Collections.Generic;
using System.Linq;
using DeKalbMedical.Data.DTO.DataPart;
using Score.Data.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace DeKalbMedical.Data.DatasourceItems
{
    public class DoctorSearchDatasource : CustomItem
    {
        public DoctorSearchDatasource(Item innerItem) : base(innerItem)
        { }

        #region Form Controls
        public string LastNameLabel
        {
            get { return InnerItem.Fields[Consts.Fields.LastNameLabelField].Value; }
        }

        public string LastNamePlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.LastNamePlaceholderTextField].Value; }
        }

        public string FirstNameLabel
        {
            get { return InnerItem.Fields[Consts.Fields.FirstNameLabelField].Value; }
        }

        public string FirstNamePlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.FirstNamePlaceholderTextField].Value; }
        }

        public string GenderLabel
        {
            get { return InnerItem.Fields[Consts.Fields.GenderLabelField].Value; }
        }

        public string GenderPlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.GenderPlaceholderTextField].Value; }
        }

        public string CityLabel
        {
            get { return InnerItem.Fields[Consts.Fields.CityLabelField].Value; }
        }

        public string CityPlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.CityPlaceholderTextField].Value; }
        }

        public string SpecialtyLabel
        {
            get { return InnerItem.Fields[Consts.Fields.SpecialtyLabelField].Value; }
        }

        public string SpecialtyPlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.SpecialtyPlaceholderTextField].Value; }
        }

        public string LanguageLabel
        {
            get { return InnerItem.Fields[Consts.Fields.LanguageLabelField].Value; }
        }

        public string LanguagePlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.LanguagePlaceholderTextField].Value; }
        }

        public string AffiliatedLocationLabel
        {
            get { return InnerItem.Fields[Consts.Fields.AffiliatedLocationLabelField].Value; }
        }

        public string AffiliatedLocationPlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.AffiliatedLocationPlaceholderTextField].Value; }
        }

        public string ZipLabel
        {
            get { return InnerItem.Fields[Consts.Fields.ZipLabelField].Value; }
        }

        public string ZipPlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.ZipPlaceholderTextField].Value; }
        }

        public string RangeLabel
        {
            get { return InnerItem.Fields[Consts.Fields.RangeLabelField].Value; }
        }

        public string RangePlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.RangePlaceholderTextField].Value; }
        }

        public string InsuranceLabel
        {
            get { return InnerItem.Fields[Consts.Fields.InsuranceLabelField].Value; }
        }

        public string InsurancePlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.InsurancePlaceholderTextField].Value; }
        }

        public string InsurancePlansLabel
        {
            get { return InnerItem.Fields[Consts.Fields.InsurancePlansLabelField].Value; }
        }

        public string InsurancePlansPlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.InsurancePlansPlaceholderTextField].Value; }
        }

        public string SearchButtonLabel
        {
            get { return InnerItem.Fields[Consts.Fields.SearchButtonLabelField].Value; }
        }
        
        #endregion Form Controls

        #region Validation
        public string PleaseEnterACriteria
        {
            get { return InnerItem.Fields[Consts.Fields.PleaseEnterACriteriaField].Value; }
        }

        public string PleaseEnterZipCode
        {
            get { return InnerItem.Fields[Consts.Fields.PleaseEnterZipCodeField].Value; }
        }
        #endregion Validation
        
        #region Drop Downs
        public List<OptionsListItem> LanguagesOptionsList
        {
            get
            {
                var selectedIdsArray = this.InnerItem.Fields[Consts.Fields.LanguagesOptionsListField].Value.Split('|');
                int i = 0;
                return (selectedIdsArray.Select(selectedId => Sitecore.Context.Database.GetItem(selectedId))
                    .Select(item => new OptionsListItem { Order = i++, Value = item.ID.ToString(), Text = item.DisplayName })).ToList();
            }
        }

        public List<OptionsListItem> SpecialitiesOptionsList
        {
            get
            {
                var selectedIdsArray = this.InnerItem.Fields[Consts.Fields.SpecialitiesOptionsListField].Value.Split('|');
                int i = 0;
                return (selectedIdsArray.Select(selectedId => Sitecore.Context.Database.GetItem(selectedId))
                    .Select(item => new OptionsListItem { Order = i++, Value = item.ID.ToString(), Text = item.DisplayName })).ToList();
            }
        }

        public List<OptionsListItem> RangesOptionsList
        {
            get
            {
                var selectedIdsArray = this.InnerItem.Fields[Consts.Fields.RangesOptionsListField].Value.Split('|');
                int i;
                return (selectedIdsArray.Select(selectedId => Sitecore.Context.Database.GetItem(selectedId))
                    .Select(item => new OptionsListItem { Order = int.TryParse(item.Fields[Consts.Fields.SortOrderField].Value, out i) ? i : 0, Value = item.Fields[Consts.Fields.ValueField].Value, Text = item.Fields[Consts.Fields.DisplayedTextField].Value })).ToList();
            }
        }

        public List<OptionsListItem> CitiesOptionsList
        {
            get
            {
                var selectedIdsArray = this.InnerItem.Fields[Consts.Fields.CitiesOptionsListField].Value.Split('|');
                int i;
                return (selectedIdsArray.Select(selectedId => Sitecore.Context.Database.GetItem(selectedId))
                    .Select(item => new OptionsListItem { Order = int.TryParse(item.Fields[Consts.Fields.SortOrderField].Value, out i) ? i : 0, Value = item.Name.ToLower(), Text = item.DisplayName })).ToList();
            }
        }

        public List<OptionsListItem> GendersOptionsList
        {
            get
            {
                var selectedIdsArray = this.InnerItem.Fields[Consts.Fields.GendersOptionsListField].Value.Split('|');
                int i = 0;
                return (selectedIdsArray.Select(selectedId => Sitecore.Context.Database.GetItem(selectedId))
                    .Select(item => new OptionsListItem { Order = i++, Value = item.Name, Text = item.DisplayName })).ToList();
            }
        }

        public List<OptionsListItem> FacilitiesOptionsList
        {
            get
            {
                var selectedIdsArray = this.InnerItem.Fields[Consts.Fields.FacilitiesOptionsListField].Value.Split('|');
                int i;
                return (selectedIdsArray.Select(selectedId => Sitecore.Context.Database.GetItem(selectedId))
                    .Select(item => new OptionsListItem { Order = int.TryParse(item.Fields[Consts.Fields.SortOrderField].Value, out i) ? i : 0, Value = item.ID.ToString(), Text = item.DisplayName })).ToList();
            }
        }

        public List<OptionsListItem> InsurancesOptionsList
        {
            get
            {
                var selectedIdsArray = this.InnerItem.Fields[Consts.Fields.InsurancesOptionsListField].Value.Split('|');
                int i;
                return (selectedIdsArray.Select(selectedId => Sitecore.Context.Database.GetItem(selectedId))
                    .Select(item => new OptionsListItem { Order = int.TryParse(item.Fields[Consts.Fields.SortOrderField].Value, out i) ? i : 0, Value = item.ID.ToString(), Text = item.DisplayName })).ToList();
            }
        }
        #endregion Drop Downs

        #region Search

        public string SearchUrl
        {
            get
            {
                var searchPage = string.Empty;
                var searchResultsField = (ReferenceField)this.InnerItem.Fields[Consts.Fields.SearchResultsUrlField];

                if (searchResultsField != null)
                {
                    var searchPageItem = searchResultsField.TargetItem;
                    if (searchPageItem != null)
                    {
                        searchPage = LinkManager.GetItemUrl(searchPageItem);
                        if (string.IsNullOrEmpty(searchPage))
                        {
                            Log.Error("DoctorSearchDatasource - Cannot load SearchUrl", this);
                        }
                    }
                    else
                    {
                        Log.Error(string.Concat("DoctorSearchDatasource - Cannot load SearchUrl Item, Actual value - ", searchResultsField.Value), this);
                    }
                }
                else
                {
                    Log.Error("DoctorSearchDatasource - Cannot load SearchUrl field", this);
                }

                return searchPage;
            }
        }

        #endregion Search

        public static bool TryParse(Item item, out DoctorSearchDatasource parsedItem)
        {
            parsedItem = item == null || item.IsDerived(Consts.Guids.DoctorSearchTemplateId) == false
                ? null
                : new DoctorSearchDatasource(item);

            return parsedItem != null;
        }

        public static implicit operator DoctorSearchDatasource(Item innerItem)
        {
            return innerItem != null && innerItem.IsDerived(Consts.Guids.DoctorSearchTemplateId)
                ? new DoctorSearchDatasource(innerItem)
                : null;
        }

        public static implicit operator Item(DoctorSearchDatasource customItem)
        {
            return customItem != null
                ? customItem.InnerItem
                : null;
        }
    }
}
