﻿using System;
using Score.Data.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace DeKalbMedical.Data.DatasourceItems
{
    /// <summary>
    /// 
    /// </summary>
    public class ReferralSearchResultsDatasource : CustomItem
    {
        public string ReturnToSearchPageLabelFieldName = "Return to Search Page Label";
        public string ComprehensiveServicesLabelFieldName = "Comprehensive Services Label";
        public string ServiceLabelFieldName = "Service Label";
        public string KeywordLabelFieldName = "Keyword Label";
        public string SearchPageFieldName = "Search Page Url";
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="innerItem"></param>
        public ReferralSearchResultsDatasource(Item innerItem) : base(innerItem)
        {
        }

        public string ResultsErrorMessage
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsErrorMessageField].Value; }
        }

        public int ResultsPerPage
        {
            get { return Convert.ToInt32(this.InnerItem.Fields[Consts.Fields.ResultsItemsPerPageField].Value); }
        }

        public string ResultsTitle
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsTitleField].Value; }
        }

        public string ResultsMoreLinkText
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsMoreLinkTextField].Value; }
        }

        public string ReturnToSearchPageLabel
        {
            get { return this.InnerItem.Fields[ReturnToSearchPageLabelFieldName].Value; }
        }

        public string ComprehensiveServicesLabel
        {
            get { return this.InnerItem.Fields[ComprehensiveServicesLabelFieldName].Value; }
        }

        public string ResultsPagerTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsPagerTemplateField].Value; }
        }

        public string ResultsNoResultsTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsNoResultsTemplateField].Value; }
        }

        public string TotalResultsTemplate
        {
            get { return this.InnerItem.Fields[Consts.Fields.TotalResultsTemplateField].Value; }
        }

        public string KeywordLabel
        {
            get { return this.InnerItem.Fields[KeywordLabelFieldName].Value; }
        }

        public string ServiceLabel
        {
            get { return this.InnerItem.Fields[ServiceLabelFieldName].Value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string SearchPageUrl
        {
            get
            {
                var searchPage = string.Empty;
                var searchResultsField = (ReferenceField)this.InnerItem.Fields[SearchPageFieldName];

                if (searchResultsField != null)
                {
                    var searchPageItem = searchResultsField.TargetItem;
                    if (searchPageItem != null)
                    {
                        searchPage = LinkManager.GetItemUrl(searchPageItem);
                        if (string.IsNullOrEmpty(searchPage))
                        {
                            Log.Error("ReferralSearchResultsDatasource - Cannot load Search page Url", this);
                        }
                    }
                    else
                    {
                        Log.Error(string.Concat("ReferralSearchResultsDatasource - Cannot load SearchUrl Item, Actual value - ", searchResultsField.Value), this);
                    }
                }
                else
                {
                    Log.Error("ReferralSearchResultsDatasource - Cannot load SearchUrl field", this);
                }

                return searchPage;
            }
        }

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="datasourceItem"></param>
        /// <returns></returns>
        public static bool TryParse(Item item, out ReferralSearchResultsDatasource datasourceItem)
        {
            if (item == null || item.IsDerived(Consts.Guids.ReferralSearchResultsTemplateId) == false)
            {
                datasourceItem = null;
            }
            else
            {
                datasourceItem = new ReferralSearchResultsDatasource(item);
            }

            return datasourceItem != null;
        }
    }
}
