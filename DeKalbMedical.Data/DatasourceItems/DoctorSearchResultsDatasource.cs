﻿using System;
using Score.Data.Extensions;
using Sitecore.Data.Items;

namespace DeKalbMedical.Data.DatasourceItems
{
    public class DoctorSearchResultsDatasource : CustomItem
    {
        public DoctorSearchResultsDatasource(Item innerItem)
            : base(innerItem)
        {
        }

        public string ResultsErrorMessage
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsErrorMessageField].Value; }
        }

        public int ResultsPerPage
        {
            get { return Convert.ToInt32(this.InnerItem.Fields[Consts.Fields.ResultsItemsPerPageField].Value); }
        }

        public string ResultsTitle
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsTitleField].Value; }
        }

        public string WebsiteLinkText
        {
            get { return this.InnerItem.Fields[Consts.Fields.WebsiteLinkTextField].Value; }
        }
        public string OfficesTitleText
        {
            get { return this.InnerItem.Fields[Consts.Fields.OfficesTitleTextField].Value; }
        }

        public string DirectionsLinkText
        {
            get { return this.InnerItem.Fields[Consts.Fields.DirectionsLinkTextField].Value; }
        }

        public string ResultsMoreLinkText
        {
            get { return this.InnerItem.Fields[Consts.Fields.ResultsMoreLinkTextField].Value; }
        }

        public static bool TryParse(Item item, out DoctorSearchResultsDatasource datasourceItem)
        {
            if (item == null || item.IsDerived(Consts.Guids.DoctorSearchResultsTemplateId) == false)
            {
                datasourceItem = null;
            }
            else
            {
                datasourceItem = new DoctorSearchResultsDatasource(item);
            }

            return datasourceItem != null;
        }
    }
}
