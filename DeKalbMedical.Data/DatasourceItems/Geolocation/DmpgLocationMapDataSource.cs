﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeKalbMedical.Data.DTO.DataPart;
using Score.Data.Extensions;
using Sitecore.Data.Items;

namespace DeKalbMedical.Data.DatasourceItems.Geolocation
{
    public class DmpgLocationMapDataSource : CustomItem
    {
        public DmpgLocationMapDataSource(Item innerItem) : base(innerItem) { }

        #region Data
        public string ZoomValue
        {
            get { return this.InnerItem.Fields[Consts.Fields.ZoomValueField].Value; }
        }

        public string DefaultDmpg
        {
            get { return this.InnerItem.Fields[Consts.Fields.DefaultDmpgField].Value; }
        }
        
        public string DefaultRange
        {
            get { return this.InnerItem.Fields[Consts.Fields.DefaultRangeField].Value; }
        }

        public string DefaultZip
        {
            get { return this.InnerItem.Fields[Consts.Fields.DefaultZipField].Value; }
        }

        public string DmpgCategoriesPath
        {
            get
            {
                var path = new StringBuilder(InnerItem.Fields[Consts.Fields.DmpgOptionsListField].Source);
                if (!string.IsNullOrWhiteSpace(path.ToString()))
                {
                    path = path.Replace("query:", string.Empty);
                    if (path.ToString().EndsWith("/*"))
                    {
                        path = path.Replace("/*", string.Empty);
                    }
                }

                return path.ToString();
            }
        }
        #endregion Data

        #region Drop Downs
        public List<OptionsListItem> RangesOptionsList
        {
            get
            {
                var selectedIdsArray = this.InnerItem.Fields[Consts.Fields.RangesOptionsListField].Value.Split('|');
                int i;
                return (selectedIdsArray.Select(selectedId => Sitecore.Context.Database.GetItem(selectedId))
                    .Select(item => new OptionsListItem { Order = int.TryParse(item.Fields[Consts.Fields.SortOrderField].Value, out i) ? i : 0, Value = item.Fields[Consts.Fields.ValueField].Value, Text = item.Fields[Consts.Fields.DisplayedTextField].Value })).ToList();
            }
        }

        public List<OptionsListItem> DmpgOptionsList
        {
            get
            {
                var selectedIdsArray = this.InnerItem.Fields[Consts.Fields.DmpgOptionsListField].Value.Split('|');
                int i;
                return (selectedIdsArray.Select(selectedId => Sitecore.Context.Database.GetItem(selectedId))
                    .Select(item => new OptionsListItem { Order = int.TryParse(item.Fields[Consts.Fields.SortOrderField].Value, out i) ? i : 0, Value = item.Fields[Consts.Fields.ValueField].Value, Text = item.DisplayName })).ToList();
            }
        }
        #endregion Drop Downs

        #region Form Controls
        public string CategoryLabel
        {
            get { return this.InnerItem.Fields[Consts.Fields.CategoryLabelField].Value; }
        }

        public string CategoryPlaceholderText
        {
            get { return this.InnerItem.Fields[Consts.Fields.CategoryPlaceholderTextField].Value; }
        }

        public string ZipLabel
        {
            get { return this.InnerItem.Fields[Consts.Fields.ZipLabelField].Value; }
        }

        public string ZipPlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.ZipPlaceholderTextField].Value; }
        }

        public string RangeLabel
        {
            get { return this.InnerItem.Fields[Consts.Fields.RangeLabelField].Value; }
        }

        public string RangePlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.RangePlaceholderTextField].Value; }
        }

        public string EmptyMapPlaceholderText
        {
            get { return InnerItem.Fields[Consts.Fields.EmptyMapPlaceholderTextField].Value; }
        }
        #endregion Form Controls

        #region Validation
        public string PleaseEnterZipCode
        {
            get { return InnerItem.Fields[Consts.Fields.PleaseEnterZipCodeField].Value; }
        }

        public string ResultsErrorMessage
        {
            get { return InnerItem.Fields[Consts.Fields.ResultsErrorMessageField].Value; }
        }
        
        #endregion Validation

        #region Pager
        public int ResultsPerPage
        {
            get { return Convert.ToInt32(this.InnerItem.Fields[Consts.Fields.ResultsItemsPerPageField].Value); }
        }
        #endregion Pager

        public static bool TryParse(Item item, out DmpgLocationMapDataSource parsedItem)
        {
            parsedItem = item == null || item.IsDerived(Consts.Guids.DmpgLocationMapTemplateId) == false
                ? null
                : new DmpgLocationMapDataSource(item);

            return parsedItem != null;
        }

        public static implicit operator DmpgLocationMapDataSource(Item innerItem)
        {
            return innerItem != null && innerItem.IsDerived(Consts.Guids.DmpgLocationMapTemplateId)
                ? new DmpgLocationMapDataSource(innerItem)
                : null;
        }

        public static implicit operator Item(DmpgLocationMapDataSource customItem)
        {
            return customItem != null
                ? customItem.InnerItem
                : null;
        }
    }
}
