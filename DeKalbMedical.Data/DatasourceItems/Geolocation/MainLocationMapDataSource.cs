﻿using Score.Data.Extensions;
using Sitecore.Data.Items;

namespace DeKalbMedical.Data.DatasourceItems.Geolocation
{
    public class MainLocationMapDataSource : CustomItem
    {
        public MainLocationMapDataSource(Item innerItem) : base(innerItem) { }

        public string DefaultButtonNumber
        {
            get { return this.InnerItem.Fields[Consts.Fields.DefaultButtonNumberField].Value; }
        }

        public string ZoomValue
        {
            get { return this.InnerItem.Fields[Consts.Fields.ZoomValueField].Value; }
        }
        
        public string TextForMobileView
        {
            get { return this.InnerItem.Fields[Consts.Fields.TextForMobileViewField].Value; }
        }
        
        public static bool TryParse(Item item, out MainLocationMapDataSource parsedItem)
        {
            parsedItem = item == null || item.IsDerived(Consts.Guids.MainLocationMapTemplateId) == false
                ? null
                : new MainLocationMapDataSource(item);

            return parsedItem != null;
        }

        public static implicit operator MainLocationMapDataSource(Item innerItem)
        {
            return innerItem != null && innerItem.IsDerived(Consts.Guids.MainLocationMapTemplateId)
                ? new MainLocationMapDataSource(innerItem)
                : null;
        }

        public static implicit operator Item(MainLocationMapDataSource customItem)
        {
            return customItem != null
                ? customItem.InnerItem
                : null;
        }
    }
}
