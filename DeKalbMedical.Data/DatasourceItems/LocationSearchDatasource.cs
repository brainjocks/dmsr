﻿using System.Collections.Generic;
using System.Linq;
using DeKalbMedical.Data.DTO.DataPart;
using Score.Data.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace DeKalbMedical.Data.DatasourceItems
{
    /// <summary>
    /// 
    /// </summary>
    public class LocationSearchDatasource : CustomItem
    {
        public string LocationNameLabelField = "Location Name Label";
        public string SearchButtonLabelField = "Search Button Label";
        public string LocationNamePlaceholderTextField = "Location Name Placeholder Text";
        public string CityLabelField = "City Label";
        public string ServicesLabelField = "Services Label";
        public string ZipLabelField = "Zip Label";
        public string ZipPlaceholderTextField = "Zip Placeholder Text";
        public string RangeLabelField = "Range Label";
        public string SelectRadiusLabelField = "Select a Radius Label";
        public string SelectServiceLabelField = "Select a Service Label";
        public string SelectCityLabelField = "Select a City Label";
        public string SearchResultsUrlField = "Search Results Url";
        public string ServicesForDropdown = "Services";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="innerItem"></param>
        public LocationSearchDatasource(Item innerItem) : base(innerItem)
        {

        }

        public string LocationNameLabel
        {
            get { return InnerItem.Fields[LocationNameLabelField].Value; }
        }

        public string SearchButtonLabel
        {
            get { return InnerItem.Fields[SearchButtonLabelField].Value; }
        }

        public string LocationNamePlaceholderText
        {
            get { return InnerItem.Fields[LocationNamePlaceholderTextField].Value; }
        }

        public string ZipPlaceholderText
        {
            get { return InnerItem.Fields[ZipPlaceholderTextField].Value; }
        }

        public string CityLabel
        {
            get { return InnerItem.Fields[CityLabelField].Value; }
        }

        public string SelectServiceLabel
        {
            get { return InnerItem.Fields[SelectServiceLabelField].Value; }
        }

        public string SelectCityLabel
        {
            get { return InnerItem.Fields[SelectCityLabelField].Value; }
        }

        public string SelectRadiusLabel
        {
            get { return InnerItem.Fields[SelectRadiusLabelField].Value; }
        }

        public string ServicesLabel
        {
            get { return InnerItem.Fields[ServicesLabelField].Value; }
        }

        public string ZipLabel
        {
            get { return InnerItem.Fields[ZipLabelField].Value; }
        }

        public string RangeLabel
        {
            get { return InnerItem.Fields[RangeLabelField].Value; }
        }

        public string EnterCriteriaMessage
        {
            get { return InnerItem.Fields[Consts.Fields.PleaseEnterACriteriaField].Value; }
        }

        public string PleaseEnterZipCode
        {
            get { return InnerItem.Fields[Consts.Fields.PleaseEnterZipCodeField].Value; }
        }

        /// <summary>
        /// TODO: use dynamic source
        /// </summary>
        public List<OptionsListItem> Services
        {
            get
            {
                List<OptionsListItem> servicesList = new List<OptionsListItem>();
                int i = 0;
                
                if(!string.IsNullOrEmpty(InnerItem.Fields[ServicesForDropdown].Value))
                {
                    var services = InnerItem.Fields[ServicesForDropdown].Value.Split('|');//Sitecore.Context.Database.SelectItems("/sitecore/content/DeKalbMedical/Data/Meta/Services/*[@@templatename='Service Line']");
                    servicesList = (List<OptionsListItem>) services.Select(selectedId => Sitecore.Context.Database.GetItem(selectedId))
                        .Select(item => new OptionsListItem { Order = i++, Value = item.ID.ToString(), Text = item.DisplayName }).ToList();
                }
                
                return servicesList;
            }
        }

        /// <summary>
        /// TODO: use dynamic source
        /// </summary>
        public List<KeyValuePair<string, string>> Ranges
        {
            get
            {
                var result = new List<KeyValuePair<string, string>>();
                var ranges = Sitecore.Context.Database.SelectItems("/sitecore/content/DeKalbMedical/Data/Meta/Ranges/*[@@templatename='Range']");
                if (ranges != null && ranges.Any())
                {
                    result.AddRange(ranges.Select(x => new KeyValuePair<string, string>(x.Fields["Value"].Value, x.Fields["Displayed Text"].Value)));
                }
                return result;
            }
        }

        /// <summary>
        /// TODO: use dynamic source
        /// </summary>
        public List<KeyValuePair<string, string>> Cities
        {
            get
            {
                var result = new List<KeyValuePair<string, string>>();
                var locations = Sitecore.Context.Database.SelectItems("/sitecore/content/DeKalbMedical/home/locations/Locations Module//*[@@templatename='Location']");
                if (locations != null && locations.Any())
                {
                    var cities = locations.Where(x => x.Fields["City"].Value != string.Empty).Select(x => x.Fields["City"].Value).Distinct().OrderBy(x => x);

                    result.AddRange(cities.Select(x => new KeyValuePair<string, string>(x, x)));
                }
                return result;
            }
        }

        /// <summary>
        /// Same implementation as for SearchBox component. Most likely will be moved to common template for both components
        /// </summary>
        public string SearchUrl
        {
            get
            {
                var searchPage = string.Empty;
                var searchResultsField = (ReferenceField)this.InnerItem.Fields[SearchResultsUrlField];

                if (searchResultsField != null)
                {
                    var searchPageItem = searchResultsField.TargetItem;
                    if (searchPageItem != null)
                    {
                        searchPage = LinkManager.GetItemUrl(searchPageItem);
                        if (string.IsNullOrEmpty(searchPage))
                        {
                            Log.Error("LocationSearchDatasource - Cannot load SearchUrl as searchpage is null for page" + searchPageItem.ID.ToString(), this);
                        }
                    }
                    else
                    {
                        Log.Error(string.Concat("LocationSearchDatasource - Cannot load SearchUrl Item, Actual value - ", searchResultsField.Value), this);
                    }
                }
                else
                {
                    Log.Error("LocationSearchDatasource - Cannot load SearchUrl field", this);
                }

                return searchPage;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="datasourceItem"></param>
        /// <returns></returns>
        public static bool TryParse(Item item, out LocationSearchDatasource datasourceItem)
        {
            if (item == null || item.IsDerived(Consts.Guids.LocationSearchTemplateId) == false)
            {
                datasourceItem = null;
            }
            else
            {
                datasourceItem = new LocationSearchDatasource(item);
            }

            return datasourceItem != null;
        }
    }
}
