define([
    "jquery",
    "underscore",
    "score_ccf/ModuleLoader",
    "scorebootstrap",
    "matchHeight",
    "bootstrapSelect"
],
function ($, _, moduleLoader, scorebootstrap) {
    "use strict";

    //prevents dropdown from closing when clicked inside
    $(document).on("click", ".score-megamenu .dropdown-menu", function (e) {
        e.stopPropagation();
    });

    $('.dekalb-disabled').click(function (e) {
        e.stopImmediatePropagation();
    });


    $('.navbar-toggle').addClass('collapsed');

    // match height style boxes within a container with a special "signal" class
    $(".matchheight-stylebox .score-style-box").matchHeight(true);

    $(".matchheight-stylebox .score-button").matchHeight(true);

    // turn select elements into Bootstrap buttons
    _.each($(".selectpicker"), function (dd) {
        if (dd.hasAttribute("data-style")) {
            $(dd).selectpicker({ size: 4 });
        } else {
            $(dd).selectpicker({ style: "btn-blue", size: 4 });
        }
    });

    //Patient stories
    // add ddl button
    $('.nav-tabs-wrapper-inner').prepend('<div class="patient-stories-ddl"><span class="panel-text"></span><span class="panel-icon"></span></div>');
    
    // event to open / close ddl
    $('.patient-stories-ddl').on('click', function () {
        $('.nav-tabs-wrapper-inner').toggleClass('open');
    });
    
    // if you need to monkey patch anything in Score Bootstrap Initialization logic
    // here's where you would do it. Before calling to init(). Something like:
    // scorebootstrap.Accordion.init = function() { ... }

    // init SCORE Bootstrap components (Tabsets, Accordeons, Carousels, etc.)
    scorebootstrap.init();

    moduleLoader.loadPendingModules().done(function () {
        // anything that neeeds to run globally when all modules are loaded goes here
        // fill first item
        $('.patient-stories-ddl .panel-text').html($('.nav-tabs-wrapper-inner .nav.nav-tabs').find('[aria-expanded="true"]').html());
        // on click in ddl, copy selected to button and close ddl
        $('.nav-tabs-wrapper-inner .nav.nav-tabs li a').on('click', function (e) {
            $('.patient-stories-ddl .panel-text').html($(this).html());
            $('.nav-tabs-wrapper-inner').removeClass('open');
        });
    });

});
