﻿(function (global) {
    define([
        "jquery",
        "underscore",
        "eventManager",
        "dekalbmedical/Components/Pagination"
    ], function ($, _, eventManager, Pagination) {

        function DoctorSearchResults(args) {
            this.perPage = args.perPage;
            this.resultsErrorMessage = args.resultsErrorMessage;
            this.container = $(args.scope);
            this.isExpEditor = args.isExperienceEditor;
            this.divTarget = $("#results", args.scope);
            this.elResultsMessage = $("#results-message", args.scope);
            this.divPager = $("#pager", args.scope);
            this.resultsRowTemplate = $("#doctor-search-template", args.scope).html();
            this.resultsSummaryTemplate = $("#doctor-search-summary-template", args.scope).html();
            this.resultsPagerTemplate = $("#results-pager-template", args.scope).html();
            this.optionsListTemplate = $("#options-list-template", args.scope).html();
            var btnTexts = { First: "&nbsp;", Previous: "&nbsp;", Next: "&nbsp;", Last: "&nbsp;" };
            this.pagination = new Pagination(this.divPager, this.perPage, btnTexts);
            if (args.isExperienceEditor) {
                this.initPE(args.scope);
            } else {
                this.init();
            }
            this.getSearchResults();
        }

        DoctorSearchResults.prototype.init = function () {
            eventManager.subscribe("page-changed-" + this.pagination.container.attr("id"), function (pageIndex) {
                this.getSearchResults(pageIndex);
            }.bind(this));
        }

        DoctorSearchResults.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        DoctorSearchResults.prototype.getUrlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            else {
                return results[1] || 0;
            }
        }

        DoctorSearchResults.prototype.getSearchResults = function (pageIndex) {
            if (this.isExpEditor) { return; }
            pageIndex = pageIndex || 0;
            var data = location.search.substring(1);
            data += "&pageIndex=" + pageIndex + "&pageSize=" + this.perPage;

            $.ajax({
                url: "/SearchResults/GetDoctorSearch",
                type: "POST",
                data: data,
                traditional: true,
                dataType: "json"
            })
            .done(function (response) {
                this.divTarget.empty();
                this.elResultsMessage.empty();
                this.divPager.empty();
                this.renderResponse(response);
            }.bind(this))
            .fail(console.warn);
        }

        DoctorSearchResults.prototype.renderTemplate = function ($target, $template, data) {
            var tmpl = _.template($template);
            $target.html(tmpl(data));
        }

        DoctorSearchResults.prototype.getProtocol = function () {
            var url = window.location.href;
            var arr = url.split("/");
            return arr[0].replace(":", "");
        }

        DoctorSearchResults.prototype.renderResponse = function (response) {
            if (response.Items && response.Items.length > 0) {
                _.mapObject(response.Items, function (item) {
                    return item.LinkTitle = item.LinkTitle.split("|", 1);
                });
                _.each(response.Items, function (item) {
                    if (item.Offices) {
                        for (var ofs in item.Offices) {
                            var addressLines = item.Offices[ofs].Address.split("\r\n");
                            var geolocation = item.Offices[ofs].Geolocation;
                            if (geolocation) {
                                var gArr = geolocation.split(",");
                                geolocation = "https://www.google.com/maps/dir/Current+Location/" + item.Offices[ofs].Address.replace("\r\n", " ") + "/@" + gArr[0] + "," + gArr[1];
                            } else {
                                geolocation = "https://www.google.com/maps/dir/Current+Location/" + item.Offices[ofs].Address.replace("\r\n", " ");
                            }
                            var website = item.Offices[ofs].Website;
                            if (website && website.startsWith(":")) {
                                website = this.getProtocol() + website;
                            }
                            _.extend(item.Offices[ofs], { AddressLine1: addressLines[0], AddressLine2: addressLines[1], AddressLine3: addressLines[2], GetDirectionLink: geolocation, Website: website });
                        }
                    }
                }.bind(this));
                var pagerObj = this.pagination.refresh(response.Pager.TotalRows);
                this.renderTemplate(this.elResultsMessage, this.resultsSummaryTemplate, { pager: pagerObj });
                this.renderTemplate(this.divTarget, this.resultsRowTemplate, { items: response.Items });
                this.renderTemplate(this.divPager, this.resultsPagerTemplate, pagerObj);
                $("html, body").animate({ scrollTop: this.divTarget.prev().prev().offset().top }, 500);
            } else {
                this.divTarget.html(this.resultsErrorMessage);
            }
        }

        return function init(args) {
            return new DoctorSearchResults(args);
        };
    });
})(this);