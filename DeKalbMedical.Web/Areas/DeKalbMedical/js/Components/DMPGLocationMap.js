﻿(function (global) {
    define([
        "jquery",
        "underscore",
        "eventManager",
        "dekalbmedical/Components/Pagination",
        "googleMapsApiLoader",
        "jqueryValidateUnobtrusive" 
    ], function ($, _, eventManager, Pagination, googleMapsApiLoader) {

        function DmpgLocationMap(args) {
            this.divMap = $("#map", args.scope);
            if (args.isExperienceEditor) {
                this.initPE(args.scope);
            } else {
                this.googleMapApiKey = args.googleMapApiKey;
                googleMapsApiLoader.KEY = this.googleMapApiKey;
                googleMapsApiLoader.LIBRARIES = ["places"];
                googleMapsApiLoader.load();
                this.loadedAsMobile = !this.divMap.is(":visible");
                this.zoom = parseInt(args.zoom) || 16;
                this.perPage = args.perPage || 3;
                this.zipCodeErrorMessage = args.pleaseEnterZipCode;
                this.resultsErrorMessage = args.resultsErrorMessage;
                this.mapEmpty = $("#mapEmpty", args.scope);
                this.duCategory = $("#category", args.scope);
                this.tbZip = $("#Zip", args.scope);
                this.btnZip = $("#findWithZip", args.scope);
                this.duRange = $("#range", args.scope);
                this.form = $("form", args.scope);
                this.divTarget = $("#results", args.scope);
                this.elResultsMessage = $("p.results", args.scope);
                this.divPager = $("#pager", args.scope);
                this.resultsTitle = $(".score-section-header", args.scope);
                this.gmapPinPopupTemplate = $("#gmap-pin-popup-template", args.scope).html();
                this.resultsPagerTemplate = $("#results-pager-template", args.scope).html();
                this.resultsSummaryTemplate = $("#results-summary-template", args.scope).html();
                this.resultsRowTemplate = $("#site-search-template", args.scope).html();
                this.resultsTitleTemplate = $("#results-title-template", args.scope).html();
                this.imgMarker = $("img.marker-img", args.scope)[0].src;
                this.imgDefaultPopupImage = $("img.default-popup-img", args.scope)[0].src;
                var btnTexts = { First: "&nbsp;", Previous: "&nbsp;", Next: "&nbsp;", Last: "&nbsp;" };
                this.pagination = new Pagination(this.divPager, this.perPage, btnTexts);
                this.infowindow = null;
                googleMapsApiLoader.onLoad(function (google) {
                    this.geocoder = new google.maps.Geocoder;
                    this.infowindow = new google.maps.InfoWindow;
                    this.init();
                }.bind(this));   
            }
        }

        DmpgLocationMap.prototype.init = function () {
            if (!this.map) {
                google.maps.event.addListener(this.infowindow, "domready", function () {
                    var iwOuter = $(".gm-style-iw", this.divMap);
                    var iwBackground = iwOuter.prev();
                    iwBackground.children(":nth-child(2)").css({ "display": "none" });
                    iwBackground.children(':nth-child(4)').css({ "display": "none" });
                    iwOuter.addClass("dekalb");
                });

                this.onWindowResize();
                this.duCategory.on("change", function () { this.startSearch(); }.bind(this));
                this.duRange.on("change", function () { this.startSearch(); }.bind(this));

                this.tbZip.on("keydown", function (e) {
                    if (e.which >= 48 && e.which <= 57) {
                        return true;
                    }
                    if (e.which >= 96 && e.which <= 105) {
                        return true;
                    }
                    if (e.which === 8 || e.which === 9 || e.which === 13 || e.which === 46) {
                        return true;
                    }
                    return false;
                });

                this.form.submit(function () { return false; });

                this.btnZip.on("click", function () {
                    if (this.form.valid()) {
                        this.startSearch();
                    }
                }.bind(this));

                eventManager.subscribe("page-changed-" + this.pagination.container.attr("id"), function (pageIndex) {
                    this.getSearchResults(pageIndex);
                }.bind(this));
            }

            this.startSearch();
        }

        DmpgLocationMap.prototype.showError = function(inputNameFor, message) {
            var errorArray = {};
            errorArray[inputNameFor] = message;
            this.form.validate().showErrors(errorArray);
        }

        DmpgLocationMap.prototype.startSearch = function() {
            this.pagination.reset();
            this.getSearchResults();
        }

        DmpgLocationMap.prototype.applyPartneredPhysiciansRule = function() {
            if (this.duCategory.val() === "partneredphysicians") {
                this.tbZip.attr("disabled", true);
                this.btnZip.attr("disabled", true);
                this.duRange.attr("disabled", true).selectpicker("refresh");
                if (this.divMap.is(":visible")) {
                    $("html, body").animate({ scrollTop: this.divTarget.prev().prev().offset().top }, 500);
                }
            } else {
                this.tbZip.attr("disabled", false);
                this.btnZip.attr("disabled", false);
                this.duRange.attr("disabled", false).selectpicker("refresh");
            }
        }

        DmpgLocationMap.prototype.getSearchResults = function (pageIndex) {
            this.applyPartneredPhysiciansRule();
            pageIndex = pageIndex || 0;
            var data = this.form.serialize();
            data += "&pageIndex=" + pageIndex + "&pageSize=" + this.perPage;

            $.ajax({
                url: "SearchResults/GetDmpgLocation",
                type: "POST",
                data: data,
                traditional: true,
                dataType: "json"
            })
            .done(function (response) {
                switch (response.Message) {
                    case "enterzipcode":
                        this.showError("Zip", this.zipCodeErrorMessage);
                        break;
                    default:
                        this.renderResponse(response);
                        break;
                    }
                }.bind(this))
            .fail(console.warn);
        }

        DmpgLocationMap.prototype.renderTemplate = function ($target, $template, data) {
            var tmpl = _.template($template);
            $target.html(tmpl(data));
        }

        DmpgLocationMap.prototype.getProtocol = function () {
            var url = window.location.href;
            var arr = url.split("/");
            return arr[0].replace(":", "");
        }

        DmpgLocationMap.prototype.getDirectionsLink = function (item) {
            if (!item.Geolocation) {
                return "";
            }
            return "https://www.google.com/maps/dir/Current+Location/" + this.getFormattedAddress(item) + "/@" + item.Geolocation.lat + "," + item.Geolocation.lng + "," + this.zoom + "z";
        }

        DmpgLocationMap.prototype.getDirectionsLinkMobile = function (item) {
            if (!item.Geolocation) {
                return "";
            }
            return "http://maps.apple.com/?daddr=" + this.getFormattedAddress(item) + "&dirflg=d&t=m";
        }

        DmpgLocationMap.prototype.getFormattedAddress = function (item) {
            return item.AddressFirst + ", " + item.AddressSecond + ", " + item.City + ", " + item.State + " " + item.Zip;
        }

        DmpgLocationMap.prototype.renderResponse = function (response) {
            _.mapObject(response.Items, function (item) {
                if (item.Url.startsWith(":")) { item.Url = this.getProtocol() + item.Url; } return item.Url;
            }.bind(this));
            _.each(response.Items, function (item) {
                _.extend(item, { DirectionsLink: this.getDirectionsLink(item), DirectionsLinkMobile: this.getDirectionsLinkMobile(item) });
            }.bind(this));
            this.renderTemplate(this.resultsTitle, this.resultsTitleTemplate, { selectedMenu: this.duCategory.find("option:selected").text() });
            this.renderTemplate(this.divTarget, this.resultsRowTemplate, { items: response.Items, protocol: this.getProtocol() });
            this.renderTemplate(this.elResultsMessage, this.resultsSummaryTemplate, { totalResults: response.Pager.TotalRows });
            if (response.Items && response.Items.length > 0) {
                this.renderTemplate(this.divPager, this.resultsPagerTemplate, this.pagination.refresh(response.Pager.TotalRows));
                this.divMap.show();
                this.mapEmpty.hide();
                this.createMap(response);
            } else {
                this.divPager.html(this.resultsErrorMessage);
                this.divMap.hide();
                this.mapEmpty.show();
            }
        }

        DmpgLocationMap.prototype.getDetailsImageSrc = function (place, item, onsuccess) {
            if (place && place.photos) {
                onsuccess(place.photos[0].getUrl({ "maxWidth": 240, "maxHeight": 100 }));
            } else {
                var sws = new google.maps.StreetViewService();
                sws.getPanoramaByLocation({ lat: item.Geolocation.lat, lng: item.Geolocation.lng }, 50,
                    function(data, status) {
                        onsuccess(status === google.maps.StreetViewStatus.OK
                            ? "https://maps.googleapis.com/maps/api/streetview?size=240x100&location=" + item.Geolocation.lat + "," + item.Geolocation.lng + "&heading=235&key=" + this.googleMapApiKey
                            : this.imgDefaultPopupImage);
                }.bind(this));
            }
        }

        DmpgLocationMap.prototype.openInfoWindow = function (marker, item) {
            if (item.Geolocation.placeId) {
                var service = new google.maps.places.PlacesService(this.map);
                service.getDetails({ placeId: item.Geolocation.placeId }, function(place, status) {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        this.getDetailsImageSrc(place, item, function(imageSrc) {
                            this.makeInfoWindow(item, imageSrc, function() {
                                this.infowindow.open(this.map, marker);
                            }.bind(this));
                        }.bind(this));
                    }
                }.bind(this));
            } else {
                this.getDetailsImageSrc(null, item, function (imageSrc) {
                    this.makeInfoWindow(item, imageSrc, function() {
                        this.infowindow.open(this.map, marker);
                    }.bind(this));
                }.bind(this));
            }
        }

        DmpgLocationMap.prototype.makeInfoWindow = function (item, imageSrc, onsuccess) {
            var url = item.Url || "https://www.google.com/maps/place/" + this.getFormattedAddress(item) + "/@" + item.Geolocation.lat + "," + item.Geolocation.lng + "," + this.zoom + "z";
            var tmpl = _.template(this.gmapPinPopupTemplate);
            onsuccess(this.infowindow.setOptions({
                content: tmpl({
                    'imageSrc': imageSrc,
                    'url': url,
                    'name': item.Name,
                    'address': this.getFormattedAddress(item),
                    'phone': item.Phone,
                    'getDirectionsLink': this.getDirectionsLink(item)
                })
            }));
        }

        DmpgLocationMap.prototype.createMap = function (response) {
            if (!this.divMap.is(":visible") && !this.mapEmpty.is(":visible")) { return; }
            if (response.Items) {
                var markers = [];
                this.map = new google.maps.Map(this.divMap[0]);
                _.each(response.Items, function (item) {
                    if (item.Geolocation) {
                        var marker = new google.maps.Marker({
                            position: { lat: parseFloat(item.Geolocation.lat), lng: parseFloat(item.Geolocation.lng) },
                            map: this.map,
                            icon: this.imgMarker
                        });
                        markers.push(marker);

                        marker.addListener("click", function (e) { this.openInfoWindow(marker, item) }.bind(this));
                    };
                }.bind(this));

                if (markers.length > 0) {
                    var bounds = new google.maps.LatLngBounds();
                    for (var i = 0; i < markers.length; i++) {
                        bounds.extend(markers[i].getPosition());
                    }
                    this.map.fitBounds(bounds);
                    google.maps.event.trigger(markers[0], "click");
                } else {
                    this.divMap.hide();
                    this.mapEmpty.show();
                }
            }
        }

        DmpgLocationMap.prototype.onWindowResize = function () {
            $(window).resize(function () {
                if (this.loadedAsMobile && this.divMap.is(":visible")) {
                    this.init();
                    this.loadedAsMobile = false;
                } else if (!this.loadedAsMobile && !this.divMap.is(":visible")) {
                    this.loadedAsMobile = true;
                }
            }.bind(this));
        }

        DmpgLocationMap.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        return function init(args) {
            return new DmpgLocationMap(args);
        };
    });
})(this);