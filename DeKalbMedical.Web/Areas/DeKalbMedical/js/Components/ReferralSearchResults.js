﻿(function (global) {
    define([
        "jquery",
        "underscore",
        "eventManager",
        "dekalbmedical/Components/Pagination",
        "bootstrapSelect"
    ], function ($, _, eventManager, Pagination) {

        function ReferralSearchResults(args) {

            this.perPage = args.perPage;
            this.resultsErrorMessage = args.resultsErrorMessage;
            this.errorMessage = args.errorMessage;
            this.container = $(args.scope);
            this.isExpEditor = args.isExpEditor;
            this.elResultsMessage = $("#searchCriteria", args.scope);
            this.alert = $("span.alert", args.scope);
            this.divTarget = $(".result-list", args.scope);

            this.resultsRowTemplate = $("#referral-search-template", args.scope).html();
            this.divPager = $("#pager", args.scope);

            this.serviceLabel = args.serviceLabel;
            this.keywordLabel = args.keywordLabel;

            this.totalResultsTemplate = $("#total-results-template", args.scope).html();
            this.resultsPagerTemplate = $("#site-search-pager-template", args.scope).html();
            this.resultsNoResultsTemplate = $("#site-search-noresults-template", args.scope).html();
            var btnTexts = { First: "&nbsp;", Previous: "&nbsp;", Next: "&nbsp;", Last: "&nbsp;" };
            this.pagination = new Pagination(this.divPager, this.perPage, btnTexts);
            this.initPE(args.scope);

            this.init();

            this.getSearchResults(0);
        }

        ReferralSearchResults.prototype.init = function () {
            eventManager.subscribe("page-changed-" + this.pagination.container.attr("id"), function (pageIndex) {
                this.getSearchResults(pageIndex);
            }.bind(this));
        }

        ReferralSearchResults.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        ReferralSearchResults.prototype.getUrlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            else {
                return results[1] || 0;
            }
        }

        ReferralSearchResults.prototype.getSearchResults = function (pageIndex) {

            pageIndex = pageIndex || 0;
            if (this.isExpEditor) {
                return;
            }
            $.ajax({
                url: "/SearchResults/GetReferralSearchResults",
                type: "POST",
                data: {
                    service: this.getUrlParam("service"),
                    keyword: this.getUrlParam("keyword"),
                    pageIndex: pageIndex,
                    pageSize: this.perPage
                },
                traditional: true,
                dataType: "json"
            })
            .done(function (response) {
                debugger;
                var viewHelpers = {
                    getFormattedString: function (contactinfo) {
                        var regex = /[\r\n]+(?=[^\r\n])/g;
                        var html = contactinfo.split(regex);
                        var updcontactinfo = "";
                        $.each(html, function (key, value) {
                            if (value.match(/^[a-z,&]+(\s+[a-z,&]+)*$/i)) {
                                updcontactinfo += "<li><p>" + value + "</p>";
                            } else {
                                updcontactinfo += "<p>" + value + "</p>";
                            }
                        });
                        
                        return updcontactinfo;
                    }
                }
                var data = { items: response.Items };
                _.extend(data, viewHelpers);
                var tmpl = _.template(this.resultsRowTemplate);
                this.divPager.before(tmpl(data));

                if (response.Items.length > 0) {
                    var mTmpl = _.template(this.totalResultsTemplate);
                    var criteriasFormat = "";
                    if (response.Service != "") {
                        criteriasFormat = this.serviceLabel + ": " + decodeURIComponent(response.Service);
                    } else {
                        criteriasFormat = this.keywordLabel + ": " + response.Keyword;
                    }
                    this.elResultsMessage.html(mTmpl({ criterias: criteriasFormat }));

                    var pagerData = this.pagination.refresh(response.Pager.TotalRows);
                    var pTmpl = _.template(this.resultsPagerTemplate);
                    this.divPager.html(pTmpl(pagerData));
                } else {
                    var nrTmpl = _.template(this.resultsNoResultsTemplate);
                    this.divPager.html(nrTmpl({ message: this.resultsErrorMessage }));
                }
            }.bind(this))
            .fail(console.warn);
        }

        return function init(args) {
            return new ReferralSearchResults(args);
        };
    });
})(this);