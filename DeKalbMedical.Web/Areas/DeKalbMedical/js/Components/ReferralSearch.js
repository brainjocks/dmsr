﻿(function (global) {
    define([
        "jquery", "eventManager", "jqueryValidateUnobtrusive"
    ], function ($, eventManager) {

        function ReferralSearch(args) {
            this.url = args.url;
            this.btnServiceSearch = $(".refer-patient-search .score-left .score-button", args.scope);
            this.btnKeywordSearch = $(".refer-patient-search .score-right .score-button", args.scope);
            this.keyword = $(".refer-patient-search input[id*='keyword']", args.scope).first();
            this.service = $("#service", args.scope);
            this.initPE(args.scope);
            this.init();
        }

        ReferralSearch.prototype.init = function () {
            this.btnServiceSearch.on("click", function (e) {
                e.preventDefault();
                this.redirectToSearchResultsByService();

            }.bind(this));

            this.btnKeywordSearch.on("click", function (e) {
                e.preventDefault();
                this.redirectToSearchResultsByKeyword();

            }.bind(this));
        }

        ReferralSearch.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        ReferralSearch.prototype.redirectToSearchResultsByService = function () {

            var selectedService = this.getValueFromSelectPicker(this.service);

            if (selectedService === "") {
                $("#serviceCriteriaError").show().delay(5000).fadeOut();
                return;
            }

            var redirectUrl = this.url + "?service=" + encodeURIComponent(selectedService);
            window.location = redirectUrl;
        }

        ReferralSearch.prototype.redirectToSearchResultsByKeyword = function () {

            var keyword = this.keyword.val();

            if (keyword === "") {
                $("#keywordCriteriaError").show().delay(5000).fadeOut();
                return;
            }

            var redirectUrl = this.url + "?keyword=" + encodeURIComponent(keyword);
            window.location = redirectUrl;
        }

        ReferralSearch.prototype.getValueFromSelectPicker = function (selectPicker) {
            var selectedOption = $(selectPicker).siblings("div.dropdown-menu").find("ul li.selected");
            if (selectedOption.length > 0) {
                var index = $(selectedOption).first().attr("data-original-index");

                var value = $(selectPicker).find("option").eq(index).attr("value");
                if (value !== undefined) {
                    return value;
                }
            }
            return "";
        }

        return function init(args) {
            return new ReferralSearch(args);
        };
    });
})(this);