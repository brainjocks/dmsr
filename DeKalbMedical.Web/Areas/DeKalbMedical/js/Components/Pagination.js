﻿define([
    "jquery",
    "underscore",
    "eventManager"
],
function ($, _, bus) {
    var numberLength = 7;

    var classes = {
        pageButtonDisabled: "disabled",
        pageButtonActive: "active",
        pageButtonFirst: "first",
        pageButtonPrevious: "previous",
        pageButtonNext: "next",
        pageButtonLast: "last",
        pageButton: "page",
        pageButtonEllipsis: "ellipsis"
    };

    

    function Pagination(element, pageSize, btnTexts) {
        this.container = element;
        this.numberLength = numberLength;
        this.pageSize = pageSize;
        this.btnTexts = {
            First: btnTexts.First || "First",
            Previous: btnTexts.Previous || "Previous",
            Next: btnTexts.Next || "Next",
            Last: btnTexts.Last || "Last"
        };
        this.init();
    }

    function getRange(len, start) {
        var out = [], end;

        if (start === undefined) {
            start = 0;
            end = len;
        } else {
            end = start;
            start = len;
        }
        for (var i = start ; i < end ; i++) {
            out.push(i);
        }

        return out;
    };

    function getNumberForButtons(page, pages, buttons) {
        var numbers = [], half = Math.floor(buttons / 2);

        if (pages <= buttons) {
            numbers = getRange(0, pages);
        } else if (page <= half) {
            numbers = getRange(0, buttons - 2);
            numbers.push(classes.pageButtonEllipsis);
            numbers.push(pages - 1);
        } else if (page >= pages - 1 - half) {
            numbers = getRange(pages - (buttons - 2), pages);
            numbers.splice(0, 0, classes.pageButtonEllipsis);
            numbers.splice(0, 0, 0);
        } else {
            numbers = getRange(page - half + 2, page + half - 1);
            numbers.push(classes.pageButtonEllipsis);
            numbers.push(pages - 1);
            numbers.splice(0, 0, classes.pageButtonEllipsis);
            numbers.splice(0, 0, 0);
        }

        return numbers;
    }

    Pagination.prototype.init = function () {
        this.container.on("click", "a", function (event) {
            event.preventDefault();
            var li = $(event.target).parent();

            if (!li.hasClass(classes.pageButtonActive) && !li.hasClass(classes.pageButtonDisabled)) {
                if (li.hasClass(classes.pageButtonFirst)) {
                    this.pageIndex = 0;
                } else if (li.hasClass(classes.pageButtonPrevious)) {
                    this.pageIndex--;
                } else if (li.hasClass(classes.pageButtonNext)) {
                    this.pageIndex++;
                } else if (li.hasClass(classes.pageButtonLast)) {
                    this.pageIndex = this.totalPages - 1;
                } else if (li.hasClass(classes.pageButton)) {
                    this.pageIndex = parseInt($(event.target).attr("data-dt-idx"), 10);
                }

                bus.trigger("page-changed-" + this.container.attr("id"), this.pageIndex);
            }
        }.bind(this));
    }

    Pagination.prototype.reset = function () {
        this.pageIndex = 0;
    }

    Pagination.prototype.refresh = function (totalRowCount) {
        var i, ien, button, context = { pages: [] };
        this.pageIndex = parseInt(this.pageIndex, 10) || 0;
        this.totalPages = parseInt(Math.ceil(totalRowCount / this.pageSize), 10);
        this.container.empty();

        if (this.totalPages === 0) {
            return { items: 0, from: 0, to: 0, totalRows: 0, pageIndex: 0, totalPages: 0 };
        }

        var buttons = [classes.pageButtonFirst, classes.pageButtonPrevious].concat(
                            getNumberForButtons(this.pageIndex, this.totalPages, this.numberLength),
                                        [classes.pageButtonNext, classes.pageButtonLast]);

        for (i = 0, ien = buttons.length ; i < ien ; i++) {
            button = buttons[i];
            var btnDisplay, btnClass = "";

            switch (button) {
                case classes.pageButtonEllipsis:
                    btnDisplay = "...";
                    btnClass = classes.pageButtonDisabled;
                    break;
                case classes.pageButtonFirst:
                    btnDisplay = this.btnTexts.First;
                    btnClass = button + (this.pageIndex > 0 ? "" : " " + classes.pageButtonDisabled);
                    break;
                case classes.pageButtonPrevious:
                    btnDisplay = this.btnTexts.Previous;
                    btnClass = button + (this.pageIndex > 0 ? "" : " " + classes.pageButtonDisabled);
                    break;
                case classes.pageButtonNext:
                    btnDisplay = this.btnTexts.Next;
                    btnClass = button + (this.pageIndex < this.totalPages - 1 ? "" : " " + classes.pageButtonDisabled);
                    break;
                case classes.pageButtonLast:
                    btnDisplay = this.btnTexts.Last;
                    btnClass = button + (this.pageIndex < this.totalPages - 1 ? "" : " " + classes.pageButtonDisabled);
                    break;
                default:
                    btnDisplay = button + 1;
                    btnClass = this.pageIndex == button ? classes.pageButtonActive : classes.pageButton;
                    break;
            }

            context.pages[i] = { "btnClass": btnClass, "btnDisplay": btnDisplay, "button": button };
        }

        context.totalRows = totalRowCount;
        context.from = this.pageIndex * this.pageSize + 1;
        context.to = this.pageIndex * this.pageSize + parseInt(this.pageSize, 10);
        if (context.to > totalRowCount) {
            context.to = totalRowCount;
        }

        this.container.addClass("dataTables_paginate");

        return { items: context.pages, from: context.from, to: context.to, totalRows: context.totalRows, pageIndex: this.pageIndex, totalPages: this.totalPages };
    }

    Pagination.prototype.currentPage = function () {
        return this.pageIndex;
    }

    return Pagination;
});