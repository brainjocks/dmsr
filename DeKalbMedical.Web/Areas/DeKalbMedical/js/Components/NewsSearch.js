﻿(function (global) {
    define([
        "jquery",
        "underscore",
        "eventManager",
        "bootstrapDatepicker"
    ], function ($, _, eventManager) {

        function NewsSearch(args) {
            debugger;
            this.resultsErrorMessage = args.resultsErrorMessage;
            this.url = args.url;
            this.form = $(".news-search form", args.scope);
            this.btnSearch = $("#btnSearch", args.scope);
            this.keyword = $("input[name*='Keyword']", args.scope);
            this.category = $("#category", args.scope);
            this.datefrom = $("#datefrom", args.scope);
            this.dateto = $("#dateto", args.scope);
            this.form = $("form", args.scope);

            if (args.isExperienceEditor) {
                this.initPE(args.scope);
            } else {
                
                this.init();
            }
        }

        NewsSearch.prototype.init = function () {
            $('.news-search form #datefrom').datepicker({
                autoClose: true
            }).on('changeDate', function(e){
                $(this).datepicker('hide');
            });;
            $('.news-search form #dateto').datepicker({
                autoClose: true
            }).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });;

            this.btnSearch.on("click", function (e) {
                e.stopImmediatePropagation();

                this.redirectToSearchResults();
            }.bind(this));
        }
        

        NewsSearch.prototype.redirectToSearchResults = function () {
            var params = [];
            var redirectUrl = this.url + "?";

            params.push(["keyword", this.keyword.val()]);
            params.push(["category", this.getValueFromSelectPicker(this.category)]);
            params.push(["datefrom", this.datefrom.val().replace(/\//g, "-")]);
            params.push(["dateto", this.dateto.val().replace(/\//g, "-")]);

            for (var i = 0; i < params.length; i++) {
                if (params[i][1] !== "") {
                    redirectUrl += "&" + params[i][0] + "=" + encodeURIComponent(params[i][1]);
                }
            }
            window.location = redirectUrl;
        }

        NewsSearch.prototype.getValueFromSelectPicker = function (selectPicker) {
            var selectedOption = $(selectPicker).siblings("div.dropdown-menu").find("ul li.selected");
            if (selectedOption.length > 0) {
                var index = $(selectedOption).first().attr("data-original-index");

                var value = $(selectPicker).find("option").eq(index).attr("value");
                if (value !== undefined) {
                    return value;
                }
            }
            return "";
        }

        NewsSearch.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        return function init(args) {
            return new NewsSearch(args);
        };
    });
})(this);