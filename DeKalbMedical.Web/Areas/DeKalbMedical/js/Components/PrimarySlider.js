﻿(function (global) {
    define(["jquery", "slick"], function ($) {
        function loadSlider(args) {
            this.container = $(args.scope);

            this.container.on('init', function (e, slider) {
                // do everything after slick slider's initialization
            });

            this.container.slick({
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        }

        return function init(args) {
            return new loadSlider(args);
        };
    });
})(this);


