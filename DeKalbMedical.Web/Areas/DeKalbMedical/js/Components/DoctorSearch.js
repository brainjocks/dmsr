﻿(function (global) {
    define([
        "jquery",
        "underscore",
        "jqueryValidateUnobtrusive"
    ], function ($, _) {

        function DoctorSearch(args) {
            this.url = args.url;
            this.perPage = args.perPage;
            this.errorMessage = args.errorMessage;
            this.zipCodeErrorMessage = args.zipCodeErrorMessage;
            this.tbLastName = $("#LastName", args.scope);
            this.tbFirstName = $("#FirstName", args.scope);
            this.tbZip = $("#Zip", args.scope);
            //this.insurancePlansPlaceholderText = args.insurancePlansPlaceholderText;
            //this.duInsurance = $("#insurance", args.scope);
            //this.duInsurancePlan = $("#insurancePlan", args.scope);
            //this.lbInsurancePlan = $("#insurancePlanLabel", args.scope);
            this.btnSearch = $("#btnSearch", args.scope);
            this.form = $("form", args.scope);
            this.optionsListTemplate = $("#options-list-template", args.scope).html();
            if (args.isExperienceEditor) {
                this.initPE(args.scope);
            } else {
                this.init();
            }
        }

        DoctorSearch.prototype.init = function () {
            this.tbLastName.on("keypress", function (e) {
                this.tbKeyPress(e);
            }.bind(this));

            this.tbFirstName.on("keypress", function (e) {
                this.tbKeyPress(e);
            }.bind(this));

            this.tbZip.on("keydown", function (e) {
                if (e.which >= 48 && e.which <= 57) {
                    return true;
                }
                if (e.which >= 96 && e.which <= 105) {
                    return true;
                }
                if (e.which === 8 || e.which === 9 || e.which === 13 || e.which === 46) {
                    return true;
                }
                return false;
            });

            this.tbZip.on("keypress", function (e) {
                this.tbKeyPress(e);
            }.bind(this));

            //this.duInsurance.on("change", function (e) {
            //    $.ajax({
            //        url: "SearchResults/GetPlansForInsurance",
            //        type: "POST",
            //        data: "insuranceId=" + e.target.value,
            //        traditional: true,
            //        dataType: "json"
            //    })
            //    .done(function (response) {  
            //            this.renderSelectpickerOptionTemplate(this.duInsurancePlan, this.optionsListTemplate, { items: response }, this.insurancePlansPlaceholderText);
            //        }.bind(this))
            //    .fail(console.warn);
            //}.bind(this));

            this.btnSearch.on("click", function (e) {
                e.stopImmediatePropagation();
                if (this.form.valid()) {
                    this.redirectToSearchResults();
                }
            }.bind(this));
        }

        DoctorSearch.prototype.tbKeyPress = function (e) {
            e.stopImmediatePropagation();
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode === 13) {
                if (this.form.valid()) {
                    this.redirectToSearchResults();
                }
            }
        }

        DoctorSearch.prototype.showError = function (inputNameFor, message) {
            var errorArray = {};
            errorArray[inputNameFor] = message;
            this.form.validate().showErrors(errorArray);
        }

        DoctorSearch.prototype.redirectToSearchResults = function () {
            var isEmptyForm = !_.some(this.form.serializeArray(), function (input) { return input.value !== "-1" && input.value !== "" });
            var isEnterZipcode = this.form.get(0)["Zip"].value === "" && this.form.get(0)["range"].value !== "-1";
            if (isEmptyForm) {
                this.showError("Zip", this.errorMessage);
                return;
            }
            if (isEnterZipcode) {
                this.showError("Zip", this.zipCodeErrorMessage);
                return;
            } 
            if (!this.form.valid()) {
                return;
            }
            window.location = this.url + "?" + this.form.serialize();
        }

        DoctorSearch.prototype.renderSelectpickerOptionTemplate = function ($target, $template, data, placeholderText) {
            data.items.unshift({ Text: placeholderText, Value: "-1" });
            var tmpl = _.template($template);
            $target.html(tmpl(data)).attr("disabled", data.items.length > 1 ? false : true).selectpicker("refresh");
        }

        DoctorSearch.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        return function init(args) {
            return new DoctorSearch(args);
        };
    });
})(this);