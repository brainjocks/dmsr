﻿(function (global) {
    define(["jquery", "bootstrap"], function ($) {
        function loadTooltip(args) {
            $('.dekalb-accordion').each(function () {
                var $firstPanel = $(".panel", this).first();
                $('a[data-toggle="collapse"]', $firstPanel).addClass("collapsed").attr("aria-expanded", false);
                $('.panel-collapse', $firstPanel).removeClass("in");
            });
            this.span = $('[data-toggle="tooltip"]', args.scope);
            this.init();
        }

        loadTooltip.prototype.init = function () {
            this.span.tooltip({ placement: 'left', html: true });
        }

        return function init(args) {
            return new loadTooltip(args);
        };
    });
})(this);


