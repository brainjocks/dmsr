﻿(function (global) {
    define([
        "jquery",
        "underscore",
        "eventManager",
        "dekalbmedical/Components/Pagination",
        "bootstrapDatepicker"
    ], function ($, _, eventManager, Pagination) {

        function NewsSearchresults(args) {
            this.perPage = args.perPage;
            this.resultsErrorMessage = args.resultsErrorMessage;

            this.divTarget = $("#results", args.scope);
            this.resultsTitle = $("#resultsTitle", args.scope);
            this.elResultsMessage = $("#results-message", args.scope);
            this.divPager = $("#pager", args.scope);
            this.resultsRowTemplate = $("#news-search-template", args.scope).html();
            this.resultsSummaryTemplate = $("#news-search-summary-template", args.scope).html();
            this.resultsPagerTemplate = $("#results-pager-template", args.scope).html();
            var btnTexts = { First: "&nbsp;", Previous: "&nbsp;", Next: "&nbsp;", Last: "&nbsp;" };
            this.pagination = new Pagination(this.divPager, this.perPage, btnTexts);

            if (args.isExperienceEditor) {
                this.initPE(args.scope);
            } else {
                
                this.init();
                this.getSearchResults(0);
            }
        }

        NewsSearchresults.prototype.init = function () {
            eventManager.subscribe("page-changed-" + this.pagination.container.attr("id"), function (pageIndex) {
                this.getSearchResults(pageIndex);
            }.bind(this));
        }

        NewsSearchresults.prototype.getUrlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            else {
                return results[1] || 0;
            }
        }

        NewsSearchresults.prototype.getSearchResults = function (pageIndex) {
            pageIndex = pageIndex || 0;
            if (this.isExpEditor) {
                return;
            }

            var datefrom = this.getUrlParam("datefrom");
            var dateto = this.getUrlParam("dateto");


            $.ajax({
                    url: "/SearchResults/GetNewsSearchResults",
                    type: "POST",
                    data: {
                        keyword: this.getUrlParam("keyword"),
                        category: this.getUrlParam("category"),
                        datefrom: this.getUrlParam("datefrom"),
                        dateto: this.getUrlParam("dateto"),
                        pageIndex: pageIndex,
                        pageSize: this.perPage
                    },
                    traditional: true,
                    dataType: "json"
                })
                .done(function (response) {
                    this.divTarget.empty();
                    this.elResultsMessage.empty();
                    this.divPager.empty();

                    this.resultsTitle.show();

                    if (response.Items && response.Items.length > 0) {
                        _.mapObject(response.Items, function (item) {
                            return item.LinkTitle = item.LinkTitle.split("|", 1);
                        });
                        for (var i = 0; i < response.Items.length; i++) {
                            if (response.Items[i].PressReleaseDate !== "undefined") {
                                response.Items[i].PressReleaseDate = this.getformattedDateString(response.Items[i].PressReleaseDate);
                            }
                        }

                        var pagerObj = this.pagination.refresh(response.Pager.TotalRows);
                        this.renderTemplate(this.elResultsMessage, this.resultsSummaryTemplate, { pager: pagerObj });
                        this.renderTemplate(this.divTarget, this.resultsRowTemplate, { items: response.Items });
                        this.renderTemplate(this.divPager, this.resultsPagerTemplate, pagerObj);
                    } else {
                        this.divTarget.html(this.resultsErrorMessage);
                    }
                }.bind(this))
                .fail(console.warn);
        }

        NewsSearchresults.prototype.getformattedDateString = function (dateString) {

            //get only number from dateString
            var date = new Date(parseInt(dateString.substr(6)));

            var monthNames = [
                  "January", "February", "March",
                  "April", "May", "June", "July",
                  "August", "September", "October",
                  "November", "December"
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            var result = monthNames[monthIndex] + " " + day + ", " + year;
            return result;
        }

        NewsSearchresults.prototype.renderTemplate = function ($target, $template, data) {
            var tmpl = _.template($template);
            $target.html(tmpl(data));
        }

        NewsSearchresults.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        return function init(args) {
            return new NewsSearchresults(args);
        };
    });
})(this);