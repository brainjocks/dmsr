﻿(function (global) {
    define([
        "jquery",
        "underscore",
        "eventManager",
        "bootstrapDatepicker"
    ], function ($, _) {

        function NewsFeed(args) {
            this.perPage = args.perPage;
            this.dsId = args.dsId;
            this.newsFeedTemplate = $("#news-feed-template", args.scope).html();
            this.divTarget = $("#result-news", args.scope);
            if (args.isExperienceEditor) {
                this.initPE(args.scope);
            } else {
                this.init();
            }
        }

        NewsFeed.prototype.getformattedDateString = function (dateString) {
            //get only number from dateString
            var date = new Date(parseInt(dateString.substr(6)));
            var monthNames = [
                  "January", "February", "March",
                  "April", "May", "June", "July",
                  "August", "September", "October",
                  "November", "December"
            ];
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            var result = monthNames[monthIndex] + " " + day + ", " + year;
            return result;
        }

        NewsFeed.prototype.init = function (pageIndex) {
            pageIndex = pageIndex || 0;
            if (this.isExpEditor) {
                return;
            }
            this.divTarget.empty();

            $.ajax({
                url: "/SearchResults/GetLatestNewsFeedSearchResults",
                type: "POST",
                data: {
                    pageIndex: pageIndex,
                    pageSize: this.perPage,
                    dsId: this.dsId
                },
                traditional: true,
                dataType: "json"
            })
            .done(function (response) {
                var data = { items: response.Items };
                var dataItems = data.items;

                for (var i = 0; i < dataItems.length; i++) {
                    if (dataItems[i].PressReleaseDate !== "undefined") {
                        dataItems[i].PressReleaseDate = this.getformattedDateString(dataItems[i].PressReleaseDate);
                    }
                }

                var tmpl = _.template(this.newsFeedTemplate);
                this.divTarget.html(tmpl(data));

                //make press-release-title field editable in PE
                $('#result-news').find('div.news-item p.press-release-title').each(function () {
                    var $this = $(this);
                    $this.html($this.text());
                });
            }.bind(this))
            .fail(console.warn);
        }

        NewsFeed.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        return function init(args) {
            return new NewsFeed(args);
        };
    });
})(this);