﻿(function (global) {
    define([
        "jquery", "eventManager"
    ], function ($, eventManager) {

        function SearchBox(args) {
            this.url = args.url;
            this.parameter = args.parameter;
            this.errorMessage = args.errorMessage;
            this.container = $(args.scope);
            this.tbSearch = $("input[type=text]", args.scope);
            this.btnSearch = $("button", args.scope);
            this.alert = $("span.alert", args.scope);
            this.isBlocked = false;
            this.initPE(args.scope);
            this.init();
        }

        SearchBox.prototype.init = function () {

            this.btnSearch.on("click", function () {
                var winWidth = $(window).width();

                if (winWidth < 768) {

                    //ToDo: it must be moved into js that described navbar 
                    eventManager.subscribe("mobile:btnsearch:click", function (isExpanded) {
                        /*$(".navbar-collapse").animate({
                            marginTop: isExpanded ? "+=35" : "-=35"
                        }, "fast");*/
                    });

                    this.container.toggleClass("expanded");
                    if (this.container.hasClass("expanded")) {
                        this.tbSearch.slideDown("fast");
                        eventManager.trigger("mobile:btnsearch:click", true);
                    } else {
                        this.tbSearch.slideUp("fast");
                        eventManager.trigger("mobile:btnsearch:click", false);
                    }
                } else {
                    this.redirectToSearchResults(this.tbSearch.val());
                }
            }.bind(this));

            this.tbSearch.on("keypress", function (e) {
                var keycode = (e.keyCode ? e.keyCode : e.which);
                if (keycode === 13) {
                    this.redirectToSearchResults(e.target.value);
                } else {
                    this.alert.text("");
                }
            }.bind(this));

            this.tbSearch.on("keyup", function (e) {
                eventManager.trigger("search:synchronization", e.target.value);
            });

            eventManager.subscribe("search:synchronization", function (keyword) {
                this.tbSearch.val(keyword);
            }.bind(this));

            eventManager.subscribe("search:synchronization:block:redirect", function () {
                this.isBlocked = true;
            }.bind(this));
        }

        SearchBox.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        SearchBox.prototype.redirectToSearchResults = function (searchTerm) {
            if (searchTerm.trim().length > 0) {
                this.alert.text();
                if (this.isBlocked) {
                    eventManager.trigger("search:synchronization:results:click");
                } else {
                    window.location = this.url + "#" + this.parameter + "=" + encodeURIComponent(searchTerm);
                }
                
            } else {
                this.alert.text(this.errorMessage);
            }
        }

        return function init(args) {
            return new SearchBox(args);
        };
    });
})(this);