﻿(function (global) {
    define([
        "jquery",
        "underscore",
        "eventManager",
        "jqueryValidateUnobtrusive"
    ], function ($, _, eventManager) {

        function LocationSearch(args) {
            this.url = args.url;
            this.errorMessage = args.errorMessage;
            this.zipCodeErrorMessage = args.zipCodeErrorMessage;
            this.btnSearch = $(".score-button", args.scope);
            this.form = $(".location-search form", args.scope);
            this.locationName = $(".location-search input[id*='LocationName']", args.scope).first();
            this.zip = $(".location-search input[id*='Zip']", args.scope).first();
            this.service = $("#service", args.scope);
            this.city = $("#city", args.scope);
            this.range = $("#radius", args.scope);
            this.form = $("form", args.scope);
            if (args.isExperienceEditor) {
                this.initPE(args.scope);
            } else {
                this.init();
            }
        }

        LocationSearch.prototype.init = function () {

            this.zip.on("keydown", function (e) {
                if (e.which >= 48 && e.which <= 57) {
                    return true;
                }
                if (e.which >= 96 && e.which <= 105) {
                    return true;
                }
                if (e.which === 8 || e.which === 9 || e.which === 13 || e.which === 46) {
                    return true;
                }
                return false;
            });

            this.btnSearch.on("click", function (e) {
                e.preventDefault();
                this.redirectToSearchResults();
            }.bind(this));

            this.zip.on("keypress", function (e) {
                this.tbKeyPress(e);
            }.bind(this));

            this.locationName.on("keypress", function (e) {
                this.tbKeyPress(e);
            }.bind(this));
        }

        LocationSearch.prototype.tbKeyPress = function (e) {
            e.stopImmediatePropagation();
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode === 13) {
                if (this.form.valid()) {
                    this.redirectToSearchResults();
                }
            }
        }

        LocationSearch.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        LocationSearch.prototype.showError = function (inputNameFor, message) {
            var errorArray = {};
            errorArray[inputNameFor] = message;
            this.form.validate().showErrors(errorArray);
        }

        LocationSearch.prototype.redirectToSearchResults = function () {
            var isEmptyForm = !_.some(this.form.serializeArray(), function (input) { return input.value !== "-1" && input.value !== "" });
            var isEnterZipcode = this.form.get(0)["Zip"].value === "" && this.form.get(0)["radius"].value !== "-1";
            if (isEmptyForm) {
                this.showError("ViewModel.Zip", this.errorMessage);
                return;
            }
            if (isEnterZipcode) {
                this.showError("ViewModel.Zip", this.zipCodeErrorMessage);
                return;
            }
            if (!this.form.valid()) {
                return;
            }
            
            //we are all set and ready to request
            var params = [];
            var redirectUrl = this.url + "?";
            params.push(["locationName", this.locationName.val().replace(/\s+/g, "-")]);
            params.push(["zip", this.zip.val()]);
            params.push(["range", this.getValueFromSelectPicker(this.range)]);
            params.push(["city", this.getValueFromSelectPicker(this.city)]);
            params.push(["service", this.getValueFromSelectPicker(this.service)]);

            for (var i = 0; i < params.length; i++) {
                if(params[i][1] != "") {
                    redirectUrl += "&" + params[i][0] + "=" + encodeURIComponent(params[i][1]);
                }
            }
            window.location = redirectUrl;
        }

        LocationSearch.prototype.getValueFromSelectPicker = function (selectPicker) {
            var selectedOption = $(selectPicker).siblings("div.dropdown-menu").find("ul li.selected");
            if (selectedOption.length > 0) {
                var index = $(selectedOption).first().attr("data-original-index");

                var value = $(selectPicker).find("option").eq(index).attr("value");
                if (value !== undefined) {
                    return value;
                }
            }
            return "";
        }

        return function init(args) {
            return new LocationSearch(args);
        };
    });
})(this);