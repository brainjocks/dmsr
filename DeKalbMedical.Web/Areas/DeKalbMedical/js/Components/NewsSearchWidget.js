﻿(function (global) {
    define([
        "jquery",
        "underscore",
        "eventManager",
        "bootstrapDatepicker"
    ], function ($, _, eventManager) {

        function NewsSearchWidget(args) {
            this.url = args.url;

            this.categorylink = $(".SearchCategory", args.scope);
            this.yearlink = $(".SearchYear", args.scope);

            if (args.isExperienceEditor) {
                this.initPE(args.scope);
            } else {

                this.init();
            }
        }

        NewsSearchWidget.prototype.init = function () {
            this.setwidgetValues();
        }

        NewsSearchWidget.prototype.setwidgetValues = function () {
            var redirectUrl = this.url + "?";
           
            this.categorylink.each(function () {
                var categoryURL = $(this).attr("data-value").replace("{", "").replace("}", "").replace(/\-/g, "");
                $(this).attr("href", redirectUrl + "&category=" + categoryURL);
            });

            this.yearlink.each(function () {
                $(this).attr("href", redirectUrl + "&datefrom=01-01-" + $(this).text() + "&dateto=12-31-" + $(this).text());
            });
        }

        NewsSearchWidget.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        return function init(args) {
            return new NewsSearchWidget(args);
        };
    });
})(this);