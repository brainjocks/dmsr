﻿(function (global) {
    define([
        "jquery",
        "underscore",
        "eventManager",
        "dekalbmedical/Components/Pagination",
        "bootstrapSelect"
    ], function ($, _, eventManager, Pagination) {

        function LocationSearchResults(args) {

            this.perPage = args.perPage;
            this.resultsErrorMessage = args.resultsErrorMessage;
            this.errorMessage = args.errorMessage;
            this.container = $(args.scope);
            this.isExpEditor = args.isExpEditor;
            this.elResultsMessage = $(".results", args.scope);
            this.alert = $("span.alert", args.scope);
            this.divTarget = $(".result-list", args.scope);

            this.resultsRowTemplate = $("#site-search-template", args.scope).html();
            this.divPager = $("#pager", args.scope);

            this.totalResultsTemplate = $("#total-results-template", args.scope).html();
            this.resultsPagerTemplate = $("#site-search-pager-template", args.scope).html();
            this.resultsNoResultsTemplate = $("#site-search-noresults-template", args.scope).html();
            var btnTexts = { First: "&nbsp;", Previous: "&nbsp;", Next: "&nbsp;", Last: "&nbsp;" };
            this.pagination = new Pagination(this.divPager, this.perPage, btnTexts);
            this.initPE(args.scope);

            this.init();

            this.getSearchResults(0);
        }

        LocationSearchResults.prototype.init = function () {
            eventManager.subscribe("page-changed-" + this.pagination.container.attr("id"), function (pageIndex) {
                this.getSearchResults(pageIndex);
            }.bind(this));
        }

        LocationSearchResults.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        LocationSearchResults.prototype.getUrlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            else {
                return results[1] || 0;
            }
        }

        LocationSearchResults.prototype.getSearchResults = function (pageIndex) {

            pageIndex = pageIndex || 0;
            if (this.isExpEditor) {
                return;
            }
            $.ajax({
                url: "/SearchResults/GetLocationSearchResults",
                type: "POST",
                data: {
                    locationName: this.getUrlParam("locationName"),
                    city: this.getUrlParam("city"),
                    radius: this.getUrlParam("range"),
                    service: this.getUrlParam("service"),
                    zip: this.getUrlParam("zip"),
                    pageIndex: pageIndex,
                    pageSize: this.perPage
                },
                traditional: true,
                dataType: "json"
            })
            .done(function (response) {
                var data = { items: response.Items };
                var tmpl = _.template(this.resultsRowTemplate);
                this.divTarget.html(tmpl(data));
                
                if (response.Items.length > 0) {
                    var mTmpl = _.template(this.totalResultsTemplate);
                    this.elResultsMessage.html(mTmpl({ totalResults: response.Pager.TotalRows }));

                    var pagerData = this.pagination.refresh(response.Pager.TotalRows);
                    var pTmpl = _.template(this.resultsPagerTemplate);
                    this.divPager.html(pTmpl(pagerData));
                } else {
                    var nrTmpl = _.template(this.resultsNoResultsTemplate);
                    this.divPager.html(nrTmpl({ message: this.resultsErrorMessage }));
                }
            }.bind(this))
            .fail(console.warn);
        }

        return function init(args) {
            return new LocationSearchResults(args);
        };
    });
})(this);