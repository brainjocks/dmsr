﻿(function (global) {
    define([
        "jquery",
        "underscore",
        "eventManager",
        "dekalbmedical/Components/Pagination",
        "bootstrapSelect"
    ], function ($, _, eventManager, Pagination) {

        function SearchResults(args) {
            this.perPage = args.perPage;
            this.resultsErrorMessage = args.resultsErrorMessage;
            this.errorMessage = args.errorMessage;
            this.searchParameter = args.searchParameter;
            this.container = $(args.scope);
            this.tbSearch = $("#keyword", args.scope);
            this.btnSearch = $("#btnSearch", args.scope);
            this.elResultsMessage = $("#results-message", args.scope);
            this.alert = $("span.alert", args.scope);
            this.divTarget = $("#results", args.scope);
            this.resultsRowTemplate = $("#site-search-template", args.scope).html();
            this.divPager = $("#pager", args.scope);
            this.duCategory = $("[data-id=category]", args.scope); 
            this.resultsSummaryTemplate = $("#site-search-summary-template", args.scope).html();
            this.resultsPagerTemplate = $("#site-search-pager-template", args.scope).html();
            this.resultsNoResultsTemplate = $("#site-search-noresults-template", args.scope).html();
            var btnTexts = { First: "&nbsp;", Previous: "&nbsp;", Next: "&nbsp;", Last: "&nbsp;" };
            this.pagination = new Pagination(this.divPager, this.perPage, btnTexts);
            this.initPE(args.scope);
            this.init();
        }

        SearchResults.prototype.init = function () {
            

            this.tbSearch.on("keyup", function(e) {
                eventManager.trigger("search:synchronization", e.target.value);
            });

            eventManager.subscribe("search:synchronization", function (keyword) {
                this.tbSearch.val(keyword);
            }.bind(this));

            this.tbSearch.on("keypress", function(e) {
                var keycode = (e.keyCode ? e.keyCode : e.which);
                if (keycode === 13) {
                    this.pagination.reset();
                    this.getSearchResults(e.target.value);
                } else {
                    this.alert.text("");
                }
            }.bind(this));

            this.btnSearch.on("click", function () {
                this.pagination.reset();
                this.getSearchResults(this.tbSearch.val());
            }.bind(this));

            if (this.getHashValue()) {
                this.tbSearch.val(decodeURIComponent(this.getHashValue()));
                eventManager.trigger("search:synchronization", this.tbSearch.val());
                this.getSearchResults(this.tbSearch.val());
            }

            eventManager.subscribe("page-changed-" + this.pagination.container.attr("id"), function (pageIndex) {
                this.getSearchResults(this.tbSearch.val(), pageIndex);
            }.bind(this));

            eventManager.trigger("search:synchronization:block:redirect");

            eventManager.subscribe("search:synchronization:results:click", function () {
                this.btnSearch.click();
            }.bind(this));
        }

        SearchResults.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        SearchResults.prototype.getHashValue = function () {
            var matches = location.hash.match(new RegExp(this.searchParameter + "=([^&]*)"));
            return matches ? matches[1] : null;
        }

        SearchResults.prototype.getValueFromSelectPicker = function (selectPicker) {
            var selectedText= selectPicker.attr("title");
            var selectedValue = selectPicker.next().next().find("option").filter(function() {
                return $(this).html() === selectedText;
            }).val();
            return selectedText === selectedValue ? null : selectedValue;
        }

        SearchResults.prototype.getSearchResults = function (keyword, pageIndex) {
            if (keyword.trim().length === 0) {
                this.alert.text(this.errorMessage);
                return;
            }
            this.alert.text("loading...");
            window.location.hash = this.searchParameter + "=" + encodeURIComponent(keyword);
            pageIndex = pageIndex || 0;

            $.ajax({
                url: "/SearchResults/GetSearchResults",
                type: "POST",
                data: { keyword: keyword, parentId: this.getValueFromSelectPicker(this.duCategory), pageIndex: pageIndex, pageSize: this.perPage },
                traditional: true,
                dataType: "json"
            })
            .done(function (response) {
                var data = { items: response.Items };
                var tmpl = _.template(this.resultsRowTemplate);
                this.divTarget.html(tmpl(data));
                    
                var mTmpl = _.template(this.resultsSummaryTemplate);
                this.elResultsMessage.html(mTmpl({ keyword: keyword }));

                if (response.Items.length > 0) {
                    var pagerData = this.pagination.refresh(response.Pager.TotalRows);
                    var pTmpl = _.template(this.resultsPagerTemplate);
                    this.divPager.html(pTmpl(pagerData));
                } else {
                    var nrTmpl = _.template(this.resultsNoResultsTemplate);
                    this.divPager.html(nrTmpl({ message: this.resultsErrorMessage }));
                }
                    this.alert.text("");
            }.bind(this))
            .fail(function () {
                this.divTarget.empty();
                var nrTmpl = _.template(this.resultsNoResultsTemplate);
                this.divPager.html(nrTmpl({ message: "Something was wrong. Please try again." }));
                console.warn;
                this.alert.text("Error.");
            }.bind(this));
        }

        return function init(args) {
            return new SearchResults(args);
        };
    });
})(this);
