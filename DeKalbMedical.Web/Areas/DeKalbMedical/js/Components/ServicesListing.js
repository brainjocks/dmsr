﻿(function (global) {
    define([
        "jquery",
        "underscore",
        "jquerypagination"
    ], function ($, _, jquerypagination) {

        function ServiceListing(args) {
            this.perPage = args.perPage;

            this.listings = $(".services-listing", args.scope);
            this.divTarget = $("#results", args.scope);
            this.divPager = $("#pager", args.scope);
            this.serviceListingTemplate = $("#service-listing-template", args.scope).html();

            this.getSearchResults(0);
        }
        ServiceListing.prototype.getSearchResults = function (pageIndex) {
            pageIndex = pageIndex || 1;
            var $this = this;
            var tmpl = _.template($this.serviceListingTemplate);
            var json = {};

            this.listings.each(function (i, a) {
                json.highlights = {};
                $(a).find('.highlight-blue-overlay').each(function (j, b) {
                    var highlightindex = $(b).index();
                    json.highlights[highlightindex] = {};

                    $(b).each(function (k, c) {
                        var $c = $(c),
                            img = $c.find('img').attr('src'),
                            h2 = $c.find('h2').text(),
                            href = $c.find('a').attr('href');
                        json.highlights[highlightindex].img = img;
                        json.highlights[highlightindex].h2 = h2;
                        json.highlights[highlightindex].href = href;
                    });
                });
            });

            var keys = Object.keys(json.highlights).map(function (k) { return json.highlights[k] });
            if (keys && keys.length > 0) {
                this.divPager.pagination({
                    dataSource: keys,
                    pageSize: $this.perPage,
                    pageNumber: pageIndex,
                    showPrevious: true,
                    showNext: true,
                    ulClassName: "pagination",
                    callback: function (data, pagination) {
                        // template method of yourself
                        var alldata = _.groupBy(data, function (val, index) { return index % 2; });
                        $this.divTarget.html(tmpl({ odditems: alldata[0], evenitems: alldata[1] }));
                    }
                });
                this.listings.empty();
            }
        }
        return function init(args) {
            return new ServiceListing(args);
        };
    });
})(this);