﻿(function (global) {
    define(["jquery", "bootstrap"], function ($) {
        function DoctorDetails(args) {
            this.tabInsurance = $(args.scope).next(".score-tab").find("div[data-title='INSURANCE']");
            if (args.isExperienceEditor) {
               this.initPE(args.scope);
            } else {
                this.init();
            }
        }

        DoctorDetails.prototype.init = function () {
            this.TabsVisibility();
        }

        DoctorDetails.prototype.TabsVisibility = function () {
            if (this.tabInsurance.html().trim() === "") {
                var tabContentId = "#" + this.tabInsurance.attr("id");
                $("a[href='" + tabContentId + "']").hide();
            }
        }

        DoctorDetails.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        return function init(args) {
            return new DoctorDetails(args);
        };
    });
})(this);