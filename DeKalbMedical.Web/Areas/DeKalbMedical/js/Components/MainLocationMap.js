﻿(function (global) {
    define([
        "jquery", "underscore", "googleMapsApiLoader"
    ], function ($, _, googleMapsApiLoader) {

        function MainLocationMap(args) {
            this.divMap = $("#map", args.scope);
            if (args.isExperienceEditor) {
                this.initPE(args.scope);
            } else {
                this.googleMapApiKey = args.googleMapApiKey;
                googleMapsApiLoader.KEY = this.googleMapApiKey;
                googleMapsApiLoader.LIBRARIES = ["places"];
                googleMapsApiLoader.load();
                this.loadedAsMobile = !this.divMap.is(":visible");
                this.defaultButtonNumber = parseInt(args.defaultButtonNumber) || 1;
                this.btnPins = $("[data-type=geolocation-pin]", args.scope);
                this.defaultButton = this.btnPins[this.defaultButtonNumber - 1];
                this.gmapPinPopupTemplate = $("#gmap-pin-popup-template", args.scope).html();
                this.zoom = parseInt(args.zoom) || 16;
                this.imgMarker = $("img.marker-img", args.scope)[0].src;
                this.clickCount = 0;

                googleMapsApiLoader.onLoad(function (google) {
                    this.geocoder = new google.maps.Geocoder;
                    this.infowindow = new google.maps.InfoWindow;
                    if (this.divMap.is(":visible")) {
                        this.init();
                    } else {
                        this.initMobile();
                    }
                }.bind(this));  
            }
        }

        MainLocationMap.prototype.init = function () {
            this.clickCount = 0;
            google.maps.event.addListener(this.infowindow, "domready", function () {
                var iwOuter = $(".gm-style-iw", this.divMap);
                var iwBackground = iwOuter.prev();
                iwBackground.children(":nth-child(2)").css({ "display": "none" });
                iwBackground.children(':nth-child(4)').css({ "display": "none" });
                iwOuter.addClass("dekalb");
            });

            this.onWindowResize();
            this.btnPins.off("click");

            this.btnPins.on("click", function (e) {
                this.infowindow.close();
                $(this.btnPins).removeClass("active");
                $(e.target).toggleClass("active");
                this.increaseClickCount();
                var latlng = this.getCoordinates(e.target);
                this.setPin(latlng, e.target);
            }.bind(this));

            this.createMapWithDefaultPosition(this.defaultButton);

            for (var i = 0; i < this.btnPins.length; i++) {
                if (i !== this.defaultButtonNumber - 1) {
                    this.setMarker(this.btnPins[i], true);
                }
            }

            $(this.btnPins).removeClass("active");
            $(this.defaultButton).toggleClass("active");
            this.setMarker(this.defaultButton, false);
        }

        MainLocationMap.prototype.increaseClickCount = function () {
            this.clickCount = this.clickCount+1;
        }

        MainLocationMap.prototype.getCoordinates = function (el) {
            var $btn = $(el);
            var input = $btn.data("geolocation").replace(/\s/g, "");
            var latlngStr = input.split(",", 3);
            return { lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1]), placeId: latlngStr[2] };
        }

        MainLocationMap.prototype.createMapWithDefaultPosition = function (el) {
            var latlng = this.getCoordinates(el);
            this.map = new google.maps.Map(this.divMap[0], {
                zoom: this.zoom,
                center: latlng
            });           
        }

        MainLocationMap.prototype.setMarker = function (el, isVithoutInfoWindow) {
            var latlng = this.getCoordinates(el);
            this.setPin(latlng, el, isVithoutInfoWindow);
        }

        MainLocationMap.prototype.getDetailsImageSrc = function(place, googleMapApiKey, onsuccess) {
            if (place.photos) {
                onsuccess(place.photos[0].getUrl({ "maxWidth": 240, "maxHeight": 100 }));
            } else {
                var sws = new google.maps.StreetViewService();
                sws.getPanoramaByLocation(place.geometry.location, 50, function(data, status) {
                    onsuccess(status === google.maps.StreetViewStatus.OK
                        ? "https://maps.googleapis.com/maps/api/streetview?size=240x100&location=" + data.location.latLng.lat() + "," + data.location.latLng.lng() + "&heading=235&key=" + googleMapApiKey
                        : "http://placehold.it/240x100");
                });
            }
        }

        MainLocationMap.prototype.getDetails = function (placeId) {
            var service = new google.maps.places.PlacesService(this.map);
            service.getDetails({ placeId: placeId }, function (place, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    var getDirectionsLink = "https://www.google.com/maps/dir/Current+Location/" + place.formatted_address + "/@" + place.geometry.location.lat() + "," + place.geometry.location.lng() + "," + this.zoom + "z";

                    var address = "";
                    if (place.formatted_address) {
                        address = place.formatted_address;
                    } else if (!place.name) {
                        address = "" + place.geometry.location.lat() + ":" + place.geometry.location.lng();
                    }

                    this.getDetailsImageSrc(place, this.googleMapApiKey, function (imageSrc) {
                        var tmpl = _.template(this.gmapPinPopupTemplate);
                        this.infowindow.setOptions({
                            content: tmpl({
                                'imageSrc': imageSrc,
                                'url': place.url,
                                'name': place.name,
                                'address': address,
                                'phone': place.formatted_phone_number,
                                'getDirectionsLink': getDirectionsLink
                            })
                        });
                    }.bind(this)); 
                }
            }.bind(this));
        }

        MainLocationMap.prototype.setPin = function (latlng, el, isVithoutInfoWindow) {
            var geoObj = latlng.placeId ? { 'placeId': latlng.placeId } : { 'location': latlng };

            this.geocoder.geocode(geoObj, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: this.map,
                            icon: this.imgMarker
                        });

                        marker.addListener("click", function () {
                            $(el).click();
                        }.bind(this));

                        if (!isVithoutInfoWindow) {
                            this.getDetails(latlng.placeId);

                            if (this.clickCount === 0) {
                                setTimeout(function () { this.map.setCenter(latlng); this.map.panBy(0, -205); this.infowindow.open(this.map, marker); }.bind(this), 600);
                            } else {
                                this.infowindow.open(this.map, marker);
                                setTimeout(function () { this.map.setCenter(latlng); this.map.panBy(0, -205); }.bind(this), 300);
                            }
                        }
                    } else {
                        console.log("No results found");
                    }
                } else {
                    console.log("Geocoder failed due to: " + status);
                }
            }.bind(this));
        }

        MainLocationMap.prototype.onWindowResize = function() {
            $(window).resize(function () {
                if (this.loadedAsMobile && this.divMap.is(":visible")) {
                    this.init();
                    this.loadedAsMobile = false;
                } else if (!this.loadedAsMobile && !this.divMap.is(":visible")) {
                    this.initMobile();
                    this.loadedAsMobile = true;
                }
            }.bind(this));
        }

        MainLocationMap.prototype.initMobile = function () {
            this.onWindowResize();
            $(this.btnPins).removeClass("active");
            $(this.defaultButton).toggleClass("active");
            this.btnPins.off("click");
            this.btnPins.on("click", function (e) {
                var $btn = $(e.target);
                $(this.btnPins).removeClass("active");
                $btn.toggleClass("active");
                var latlng = this.getCoordinates(e.target);
                var q = $btn.data("placename") || $btn.text().trim().replace(/\(/g, "").replace(/\)/g, "").replace(/ /g, "+");
                window.location.href = "http://maps.apple.com/?ll=" + latlng.lat + "," + latlng.lng + "&q=" + encodeURIComponent(q) + "&z=" + this.zoom;
            }.bind(this));
        }

        MainLocationMap.prototype.initPE = function (scope) {
            $(".pe-editable-show-hide", scope).on("click", function (e) {
                var t = e.target;
                $(".pe-editable-drop-down-block", scope).toggle(500, function () {
                    $(t).html($(this).is(":visible") ? "Hide" : "Show");
                });
            });
        }

        return function init(args) {
            return new MainLocationMap(args);
        };
    });
})(this);