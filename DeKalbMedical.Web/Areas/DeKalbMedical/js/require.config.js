;(function(global) {
    global.require = global.require || {};
    global.require.baseUrl = "/";
    global.require.waitSeconds = 0;

    global.require.paths = global.require.paths || {};
    global.require.paths.dekalbmedical = "/Areas/DeKalbMedical/js";
    global.require.paths.bootstrap = "/Areas/ScoreBootstrapUI/js/Vendor/bootstrap.min";
    global.require.paths.matchHeight = "/Areas/ScoreBootstrapUI/js/Vendor/jquery.matchHeight-min";
    global.require.paths.scorebootstrap = "/Areas/ScoreBootstrapUI/js/BootstrapUI";
    global.require.paths.jqueryValidate = "/Areas/ScoreBootstrapUI/js/Vendor/jquery.validate.1.12.0.min";
    global.require.paths.jqueryUnobtrusiveAjax = "/Areas/ScoreBootstrapUI/js/Vendor/jquery.unobtrusive-ajax.min";
    global.require.paths.jqueryValidateUnobtrusive = "/Areas/ScoreBootstrapUI/js/Vendor/jquery.validate.unobtrusive.min";
    global.require.paths.bootstrapSelect = "/Areas/DekalbMedical/js/vendor/bootstrap-select";
    global.require.paths.bootstrapDatepicker = "/Areas/DekalbMedical/js/vendor/bootstrap-datepicker";
    global.require.paths.async = "/Areas/DekalbMedical/js/vendor/async";
    global.require.paths.slick = "/Areas/DekalbMedical/js/vendor/slick/slick.min";
    global.require.paths.jquerypagination = "/Areas/DekalbMedical/js/vendor/pagination.min";
    global.require.paths.googleMapsApiLoader = "/Areas/DekalbMedical/js/vendor/google.min";

    global.require.shim = global.require.shim || {};
    global.require.shim.bootstrap = { deps: ["jquery"] };
    global.require.shim.matchHeight = { deps: ["jquery"] };
    global.require.shim.jqueryValidate = { deps: ["jquery"] };
    global.require.shim.jqueryUnobtrusiveAjax = { deps: ["jquery"] };
    global.require.shim.jqueryValidateUnobtrusive = { deps: ["jqueryValidate"] };
    global.require.shim.scorevalidation = { deps: ["jqueryUnobtrusiveAjax", "jqueryValidateUnobtrusive"] };
    global.require.shim.bootstrapSelect = { deps: ["jquery", "bootstrap"] };
    global.require.shim.slick = { deps: ["jquery"] };
    global.require.shim.jquerypagination = { deps: ["jquery"] };

    if (global.requirejs instanceof Function) {
        global.requirejs.config(global.require);
    }

    if (!(global.require instanceof Function) && global.requirejs instanceof Function) {
        global.require = global.requirejs;
    }
})(this /* window */);
