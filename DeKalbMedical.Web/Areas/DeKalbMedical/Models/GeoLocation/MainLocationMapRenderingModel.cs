﻿using System;
using DeKalbMedical.Data;
using DeKalbMedical.Data.DatasourceItems.Geolocation;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Configuration;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.GeoLocation
{
    public class MainLocationMapRenderingModel : RenderingModelBase<BaseComponentParameters, MainLocationMapDataSource>
    {
        private readonly Lazy<string> _googleMapApiKey;

        public string GoogleMapApiKey => _googleMapApiKey.Value;

        public MainLocationMapRenderingModel()
        {
            _googleMapApiKey = new Lazy<string>(GetGoogleMapApiKey);
        }

        protected override MainLocationMapDataSource InitializeDatasource(Item item)
        {
            MainLocationMapDataSource ds;
            return MainLocationMapDataSource.TryParse(item, out ds) ? ds : null;
        }
        
        private string GetGoogleMapApiKey()
        {
            var googleMapApiKey = Settings.GetSetting(Consts.Names.GoogleMapsApiKey);
            if (string.IsNullOrEmpty(googleMapApiKey))
            {
                Log.Error("MainLocationMapRenderingModel.GetGoogleMapApiKey() - Cannot load the GoogleMapsApiKey from Configuration by path: configuration/sitecore/settings/setting name = 'Score.Google.ApiKey.Maps'", this);
            }

            return googleMapApiKey;
        }
    }
}