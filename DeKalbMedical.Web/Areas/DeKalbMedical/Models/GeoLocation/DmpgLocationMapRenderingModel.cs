﻿using System;
using DeKalbMedical.Data;
using DeKalbMedical.Data.DatasourceItems.Geolocation;
using Score.Data.Extensions.MvcExtensions;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Configuration;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.GeoLocation
{
    public class DmpgLocationMapRenderingModel : RenderingModelBase<BaseComponentParameters, DmpgLocationMapDataSource>
    {
        private readonly Lazy<string> _googleMapApiKey;
        
        [SitecoreRegularExpression("^\\d{5}$", "Zip Error Message")]
        public string Zip { get; set; }

        public string GoogleMapApiKey => _googleMapApiKey.Value;

        public DmpgLocationMapRenderingModel()
        {
            _googleMapApiKey = new Lazy<string>(GetGoogleMapApiKey);
        }

        protected override DmpgLocationMapDataSource InitializeDatasource(Item item)
        {
            DmpgLocationMapDataSource ds;
            return DmpgLocationMapDataSource.TryParse(item, out ds) ? ds : null;
        }

        private string GetGoogleMapApiKey()
        {
            var googleMapApiKey = Settings.GetSetting(Consts.Names.GoogleMapsApiKey);
            if (string.IsNullOrEmpty(googleMapApiKey))
            {
                Log.Error("DmpgLocationMapRenderingModel.GetGoogleMapApiKey() - Cannot load the GoogleMapsApiKey from Configuration by path: configuration/sitecore/settings/setting name = 'Score.Google.ApiKey.Maps'", this);
            }

            return googleMapApiKey;
        }
    }
}