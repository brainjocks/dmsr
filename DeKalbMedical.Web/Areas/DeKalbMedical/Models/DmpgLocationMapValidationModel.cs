﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DeKalbMedical.Web.Extensions;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models
{
    [EnterZipCodeValidation(ErrorMessage = "enterzipcode")]
    public class DmpgLocationMapValidationModel : PagedRquestModel
    {
        public string Categories { get; set; }

        public string Zip { get; set; }

        public string Radiuses { get; set; }

        public string CategoryPath { get; set; }
    }
}