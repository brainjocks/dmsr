﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models
{
    public class PagedRquestModel
    {
        public int PageIndex { get; set; }

        public int PageSize { get; set; }
    }
}