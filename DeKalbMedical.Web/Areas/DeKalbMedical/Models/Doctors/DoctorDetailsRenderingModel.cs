﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DeKalbMedical.Data;
using DeKalbMedical.Web.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Doctors
{
    /// <summary>
    /// 
    /// </summary>
    public class DoctorDetailsRenderingModel : RenderingModel
    {
        private Item _item;
        public string FirstNameFieldName = "First Name";
        public string MiddleInitialFieldName = "Middle Initial";
        public string LastNameFieldName = "Last Name";
        public string SuffixFieldName = "Suffix";
        public string PhotoFieldName = "Photo";
        public string DegreeTitlesFieldName = "Degree Titles";
        public string LanguagesSpokenFieldName = "Languages Spoken";
        public string InsuranceAcceptedFieldName = "Insurance Accepted";
        public string WebsiteField = "Website";
        public string CertificationsFieldName = "Certifications";
        public string LicensesFieldName = "Expertise";
        public string PositionTitlesFieldName = "Position Titles";
        public string AwardsFieldName = "Awards";
        public string PublicationsFieldName = "Publications";
        public string CommunityFieldName = "Community";
        public string FacilitiesNameFieldName = "Facilities";
        public string ProviderTypesFieldName = "Provider Types";
        public string SpecialtiesFieldName = "Specialties";
        public string AreasOfInterestFieldName = "Areas Of Interest";
        public string DmpgLogoFieldName = "DMPG Logo";

        public string OfficeLocationsLabelFieldName = "Office Locations Label";//Office Locations
        public string PrimaryOfficeLabelFieldName = "Primary Office Label";//(primary)
        public string AreasOfInterestLabelFieldName = "Areas Of Interest";
        public string SpecialtiesLabelFieldName = "Specialties Label";//Specialties Label
        public string AreaOfInterestsLabelFieldName = "Area of Interests Label";//Area of Interests Label
        public string CertificationsLabelFieldName = "Certifications Label";//Certifications Label
        public string EducationLabelFieldName = "Education Label";//Education Label
        public string ResidencyLabelFieldName = "Residency Label";//Residency Label
        public string FellowshipLabelFieldName = "Fellowship Label";//Fellowship Label
        public string HospitalAffiliationsLabelFieldName = "Hospital Affiliations Label";//Hospital Affiliations Label
        public string LanguagesLabelFieldName = "Languages Label";//Languages Label
        public string InsuranceAcceptedLabelFieldName = "Insurance Accepted Label";//Insurance Accepted Label
        public string LicensesLabelFieldName = "Licenses Label";//Licenses Label
        public string AwardsLabelFieldName = "Awards Label";//Awards Label
        public string PublicationsLabelFieldName = "Publications Label";//Publications Label
        public string CommunityLabelFieldName = "Community Label";//Community Label
        public string WebsiteLabelFieldName = "Website Label";//Website Label

        public DoctorDetailsRenderingModel()
        {
            
        }
        public Item RenderingItem
        {
            get
            {
                if (_item != null)
                {
                    return _item;
                }
                return this.PageItem;
            }
        }

        #region Fields

        public string FirstName
        {
            get { return this.PageItem.Fields[FirstNameFieldName].Value; }
        }

        public string MiddleInitial
        {
            get { return this.PageItem.Fields[MiddleInitialFieldName].Value; }
        }

        public string LastName
        {
            get { return this.PageItem.Fields[LastNameFieldName].Value; }
        }

        public string Suffix
        {
            get { return this.PageItem.Fields[SuffixFieldName].Value; }
        }

        public string Photo
        {
            get
            {
                ImageField physicianPhoto = this.PageItem.Fields[PhotoFieldName];
                return Sitecore.Resources.Media.MediaManager.GetMediaUrl(physicianPhoto.MediaItem);
            }
        }

        public string PhotoAlt
        {
            get
            {
                ImageField physicianPhoto = this.PageItem.Fields[PhotoFieldName];
                return physicianPhoto.Alt;
            }
        }

        public string DegreeTitles
        {
            get { return this.PageItem.Fields[DegreeTitlesFieldName].Value; }
        }

        public string LanguagesSpoken
        {
            get { return this.PageItem.Fields[LanguagesSpokenFieldName].Value; }
        }

        public string InsuranceAccepted
        {
            get { return this.PageItem.Fields[InsuranceAcceptedFieldName].Value; }
        }

        public string AreasOfInterest
        {
            get { return this.PageItem.Fields[AreasOfInterestFieldName].Value; }
        }

        public string Website
        {
            get { return this.PageItem.Fields[WebsiteLabelFieldName].Value; }
        }

        public string Certifications
        {
            get { return this.PageItem.Fields[CertificationsFieldName].Value; }
        }

        public string Licenses
        {
            get { return this.PageItem.Fields[LicensesFieldName].Value; }
        }

        public string PositionTitles
        {
            get { return this.PageItem.Fields[PositionTitlesFieldName].Value; }
        }

        public string Awards
        {
            get { return this.PageItem.Fields[AwardsFieldName].Value; }
        }

        public string Publications
        {
            get { return this.PageItem.Fields[PublicationsFieldName].Value; }
        }

        public string Community
        {
            get { return this.PageItem.Fields[CommunityFieldName].Value; }
        }

        public string Facilities
        {
            get { return this.PageItem.Fields[FacilitiesNameFieldName].Value; }
        }

        public string ProviderTypes
        {
            get { return this.PageItem.Fields[ProviderTypesFieldName].Value; }
        }

        public string Specialties
        {
            get { return this.PageItem.Fields[SpecialtiesFieldName].Value; }
        }

        public bool IsDekalbEmployed
        {
            get { return ProviderTypes.Contains(Consts.Guids.EmployedDekalbPhysicianTypeId.ToString()); }
        }

        public string DmpgLogo
        {
            get { return this.PageItem.Fields[DmpgLogoFieldName].Value; }
        }

        #endregion


        #region Labels

        public string OfficeLocationsLabel
        {
            get { return this.PageItem.Fields[OfficeLocationsLabelFieldName].Value; }
        }

        public string PrimaryOfficeLabel
        {
            get { return this.PageItem.Fields[PrimaryOfficeLabelFieldName].Value; }
        }

        public string SpecialtiesLabel
        {
            get { return this.PageItem.Fields[SpecialtiesLabelFieldName].Value; }
        }

        public string AreaOfInterestsLabel
        {
            get { return this.PageItem.Fields[AreaOfInterestsLabelFieldName].Value; }
        }

        public string CertificationsLabel
        {
            get { return this.PageItem.Fields[CertificationsLabelFieldName].Value; }
        }

        public string EducationLabel
        {
            get { return this.PageItem.Fields[EducationLabelFieldName].Value; }
        }

        public string ResidencyLabel
        {
            get { return this.PageItem.Fields[ResidencyLabelFieldName].Value; }
        }

        public string FellowshipLabel
        {
            get { return this.PageItem.Fields[FellowshipLabelFieldName].Value; }
        }

        public string HospitalAffiliationsLabel
        {
            get { return this.PageItem.Fields[HospitalAffiliationsLabelFieldName].Value; }
        }

        public string LanguagesLabel
        {
            get { return this.PageItem.Fields[LanguagesLabelFieldName].Value; }
        }

        public string InsuranceAcceptedLabel
        {
            get { return this.PageItem.Fields[InsuranceAcceptedLabelFieldName].Value; }
        }

        public string LicensesLabel
        {
            get { return this.PageItem.Fields[LicensesLabelFieldName].Value; }
        }

        public string AwardsLabel
        {
            get { return this.PageItem.Fields[AwardsLabelFieldName].Value; }
        }

        public string PublicationsLabel
        {
            get { return this.PageItem.Fields[PublicationsLabelFieldName].Value; }
        }

        public string CommunityLabel
        {
            get { return this.PageItem.Fields[CommunityLabelFieldName].Value; }
        }
        #endregion

        /// <summary>
        /// call once
        /// </summary>
        public IEnumerable<OfficeRenderingModel> Offices
        {
            get
            {
                var offices = this.PageItem.Axes.SelectItems("./Offices/*");
                if (offices != null && offices.Any())
                {
                    return offices.Select(x => new OfficeRenderingModel(x));
                }
                return null;
            }
        }


        /// <summary>
        /// call once
        /// </summary>
        public IEnumerable<EducationRenderingModel> Educations
        {
            get
            {
                var educationItems = this.PageItem.Axes.SelectItems("./Education/*");
                if (educationItems != null && educationItems.Any())
                {
                    return educationItems.Select(x => new EducationRenderingModel(x));
                }
                return null;
            }
        }


        /// <summary>
        /// call once
        /// </summary>
        public IEnumerable<InstitutionRenderingModel> Residencies
        {
            get
            {
                var residencyItems = this.PageItem.Axes.SelectItems("./Residency/*");
                if (residencyItems != null && residencyItems.Any())
                {
                    return residencyItems.Select(x => new InstitutionRenderingModel(x));
                }
                return null;
            }
        }

        /// <summary>
        /// call once
        /// </summary>
        public IEnumerable<InstitutionRenderingModel> Fellowships
        {
            get
            {
                var fellowshipItem = this.PageItem.Axes.SelectItems("./Fellowship/*");
                if (fellowshipItem != null && fellowshipItem.Any())
                {
                    return fellowshipItem.Select(x => new InstitutionRenderingModel(x));
                }
                return null;
            }
        }
    }
}