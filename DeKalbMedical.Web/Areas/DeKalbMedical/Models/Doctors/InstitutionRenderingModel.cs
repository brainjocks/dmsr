﻿using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Doctors
{
    /// <summary>
    /// 
    /// </summary>
    public class InstitutionRenderingModel
    {
        protected Item _item;

        /// <summary>
        /// 
        /// </summary>
        public Item RenderingItem
        {
            get { return _item; }
        }

        public string InstitutionNameFieldName = "Institution Name";
        public string YearFieldName = "Year";

        public string InstitutionName { get; set; }
        public string Year { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public InstitutionRenderingModel(Item item)
        {
            _item = item;

            InstitutionName = item[InstitutionNameFieldName];
            Year = item[YearFieldName];
        }
    }
}