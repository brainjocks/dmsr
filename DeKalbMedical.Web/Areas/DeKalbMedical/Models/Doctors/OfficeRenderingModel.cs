﻿using DeKalbMedical.Web.Areas.DeKalbMedical.Models.Locations;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Doctors
{
    /// <summary>
    /// 
    /// </summary>
    public class OfficeRenderingModel
    {
        private Item _item;

        /// <summary>
        /// 
        /// </summary>
        public Item RenderingItem
        {
            get { return _item; }
        }

        public string OfficeIdFieldName = "Office ID";
        public string OfficeNameFieldName = "Office Name";
        public string LocationFieldName = "Location";
        public string OfficePhoneFieldName = "Office Phone";
        public string OfficeFaxFieldName = "Office Fax";
        public string OfficeHoursFieldName = "Office Hours";
        public string EmailAddressFieldName = "Email Address";
        public string AcceptingNewPatientsFieldName = "Accepting New Patients";
        public string PrimaryOfficeFieldName = "Primary Office";

        public string OfficeId { get; set; }
        public string OfficeName { get; set; }
        public LocationDetailsRenderingModel Location { get; set; }
        public string OfficePhone { get; set; }
        public string OfficeFax { get; set; }
        public string OfficeHours { get; set; }
        public string EmailAddress { get; set; }
        public bool AcceptingNewPatients { get; set; }
        public bool PrimaryOffice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public OfficeRenderingModel(Item item)
        {
            _item = item;

            OfficeId = item[OfficeIdFieldName];
            OfficeName = item[OfficeNameFieldName];
            LookupField location = item.Fields[LocationFieldName];
            if (location != null && location.TargetItem != null)
            {
                Location = new LocationDetailsRenderingModel(location.TargetItem);
            }
            OfficePhone = item[OfficePhoneFieldName];
            OfficeFax = item[OfficeFaxFieldName];
            OfficeHours = item[OfficeHoursFieldName];
            EmailAddress = item[EmailAddressFieldName];
            AcceptingNewPatients = item[AcceptingNewPatientsFieldName] == "1";
            PrimaryOffice = item[PrimaryOfficeFieldName] == "1";
        }
    }
}