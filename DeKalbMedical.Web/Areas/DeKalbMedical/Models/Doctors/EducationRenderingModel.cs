﻿using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Doctors
{
    /// <summary>
    /// 
    /// </summary>
    public class EducationRenderingModel : InstitutionRenderingModel
    {
        public string EducationTypeFieldName = "Education Type";

        public string EducationType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public EducationRenderingModel(Item item) : base(item)
        {
            _item = item;

            EducationType = item[EducationTypeFieldName];
        }
    }
}