﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models
{
    public class ModelStateErrorModel
    {
        public string Name { get; set; }
        public string Message { get; set; }
    }
}