﻿using DeKalbMedical.Web.Extensions;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models
{
    [EnterCriteriaValidation(ErrorMessage = "entercriteria")]
    [EnterZipCodeValidation(ErrorMessage = "enterzipcode")]
    public class DoctorSearchValidationModel : PagedRquestModel
	{
        public string LastName { get; set; }

        public string Specialties { get; set; }

        public string FirstName { get; set; }

        public string Languages { get; set; }

        public string Genders { get; set; }

        public string Locations { get; set; }

        public string Cities { get; set; }

        public string Zip { get; set; }

        public string Radiuses { get; set; }

        public string Insurances { get; set; }

        public string InsurancePlans { get; set; }
	}
}