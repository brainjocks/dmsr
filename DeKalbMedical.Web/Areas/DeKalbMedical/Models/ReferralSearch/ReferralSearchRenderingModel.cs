﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeKalbMedical.Data.DatasourceItems;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.ReferralSearch
{
    /// <summary>
    /// 
    /// </summary>
    public class ReferralSearchRenderingModel : RenderingModelBase<BaseComponentParameters, ReferralSearchDatasource>
    {
        /// <summary>
        /// 
        /// </summary>
        public ReferralSearchRenderingModel() : base("referral-search")
        {
            
        }

        protected override ReferralSearchDatasource InitializeDatasource(Item item)
        {
            ReferralSearchDatasource datasource;
            return ReferralSearchDatasource.TryParse(item, out datasource) ? datasource : null;
        }
    }
}