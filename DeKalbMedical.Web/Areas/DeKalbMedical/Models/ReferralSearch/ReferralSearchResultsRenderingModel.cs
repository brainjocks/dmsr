﻿using DeKalbMedical.Data.DatasourceItems;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.ReferralSearch
{
    /// <summary>
    /// 
    /// </summary>
    public class ReferralSearchResultsRenderingModel : RenderingModelBase<BaseComponentParameters, ReferralSearchResultsDatasource>
    {
        /// <summary>
        /// 
        /// </summary>
        public ReferralSearchResultsRenderingModel() : base("referral-search-results")
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override ReferralSearchResultsDatasource InitializeDatasource(Item item)
        {
            ReferralSearchResultsDatasource datasource;
            return ReferralSearchResultsDatasource.TryParse(item, out datasource) ? datasource : null;
        }
    }
}