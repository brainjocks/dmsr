﻿using DeKalbMedical.Data.DatasourceItems;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Forms
{
    /// <summary>
    /// 
    /// </summary>
    public class LocationSearchRenderingModel : RenderingModelBase<BaseComponentParameters, LocationSearchDatasource>
    {
        public LocationSearchRenderingModel() : base("location-search")
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override LocationSearchDatasource InitializeDatasource(Item item)
        {
            LocationSearchDatasource datasource;
            return LocationSearchDatasource.TryParse(item, out datasource) ? datasource : null;
        }

        public LocationSearchViewModel ViewModel { get; set; }
    }
}