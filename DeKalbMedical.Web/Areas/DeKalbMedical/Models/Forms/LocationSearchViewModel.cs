﻿using Score.Data.Extensions.MvcExtensions;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Forms
{
    /// <summary>
    /// 
    /// </summary>
    public class LocationSearchViewModel
    {
        public string LocationName { get; set; }

        public string City { get; set; }

        public string  Service { get; set; }

        [SitecoreRegularExpression("\\d{5}", "Wrong zip code")]
        public string Zip { get; set; }

        public int Radius { get; set; }
    }
}