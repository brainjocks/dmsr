﻿using DeKalbMedical.Data.DatasourceItems;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Forms
{
    /// <summary>
    /// 
    /// </summary>
    public class LocationSearchResultsRenderingModel : RenderingModelBase<BaseComponentParameters, LocationSearchResultsDatasource>
    {
        public LocationSearchResultsRenderingModel() : base("location-search-results")
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override LocationSearchResultsDatasource InitializeDatasource(Item item)
        {
            LocationSearchResultsDatasource datasource;
            return LocationSearchResultsDatasource.TryParse(item, out datasource) ? datasource : null;
        }
    }
}