﻿using DeKalbMedical.Data.DatasourceItems;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Forms
{
    public class SearchResultsRenderingModel : RenderingModelBase<BaseComponentParameters, SearchResultsDatasource>
    {
        public SearchResultsRenderingModel() : base("search-results") { }

        protected override SearchResultsDatasource InitializeDatasource(Item item)
        {
            SearchResultsDatasource ds;
            return SearchResultsDatasource.TryParse(item, out ds) ? ds : null;
        }
    }
}