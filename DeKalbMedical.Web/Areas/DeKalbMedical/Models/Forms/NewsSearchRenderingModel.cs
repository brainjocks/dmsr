﻿using DeKalbMedical.Data.DatasourceItems;
using DeKalbMedical.Data.DatasourceItems.News;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Forms
{
    public class NewsSearchRenderingModel : RenderingModelBase<BaseComponentParameters, NewsSearchDatasource>
    {
        public NewsSearchRenderingModel() : base("news-search") { }

        protected override NewsSearchDatasource InitializeDatasource(Item item)
        {
            NewsSearchDatasource ds;
            return NewsSearchDatasource.TryParse(item, out ds) ? ds : null;
        }

        public NewsSearchViewModel ViewModel { get; set; }
    }
}