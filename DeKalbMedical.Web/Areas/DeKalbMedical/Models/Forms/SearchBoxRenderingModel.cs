﻿using DeKalbMedical.Data.DatasourceItems;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Forms
{  
    public class SearchBoxRenderingModel : RenderingModelBase<BaseComponentParameters, SearchBoxDatasource>
    {
        public SearchBoxRenderingModel() : base("search-box") { }
  
        protected override SearchBoxDatasource InitializeDatasource(Item item)
        {
            SearchBoxDatasource ds;
            return SearchBoxDatasource.TryParse(item, out ds) ? ds : null;
        }
    }
}   
