﻿using System.ComponentModel.DataAnnotations;
using DeKalbMedical.Data.DatasourceItems;
using DeKalbMedical.Web.Extensions;
using Score.Data.Extensions.MvcExtensions;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Forms
{
    public class DoctorSearchRenderingModel : RenderingModelBase<BaseComponentParameters, DoctorSearchDatasource>
    {
        public DoctorSearchValidationModel Form;

        public DoctorSearchRenderingModel()
        {
            this.Form = new DoctorSearchValidationModel();
        }

        protected override DoctorSearchDatasource InitializeDatasource(Item item)
        {
            DoctorSearchDatasource datasource;
            return DoctorSearchDatasource.TryParse(item, out datasource) ? datasource : null;
        }

        [SitecoreRegularExpression("^\\d{5}$", "Zip Error Message")]
        public string Zip { get; set; }      
    }
}