﻿using System;
using Score.Data.Extensions.MvcExtensions;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Forms
{
    /// <summary>
    /// 
    /// </summary>
    public class NewsSearchViewModel
    {
        public string Keyword { get; set; }

        public Guid Category { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }
    }
}