﻿using System.Collections.Generic;
using DeKalbMedical.Data;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Forms
{
    public class NewsSearchWidgetRenderingModel : RenderingModelBase
    {
        public bool ShowCategory
        {
            get { return this.Item.Fields[Consts.Fields.NewsCategoryCheckboxLabel].Value == "1"; }
        }

        public bool ShowArchive
        {
            get { return this.Item.Fields[Consts.Fields.NewsArchiveCheckboxLabel].Value == "1"; }
        }

        public Item[] CategoryItems
        {
            get
            {
                MultilistField categoryitems = this.Item.Fields[Consts.Fields.NewsSearchCategories];
                return categoryitems.GetItems();
            }
        }
        public Item[] ArchiveItems
        {
            get
            {
                MultilistField archiveitems = this.Item.Fields[Consts.Fields.NewsSearchYears];
                return archiveitems.GetItems();
            }
        }

        public string GetSearchResultUrl
        {
            get
            {
                var url = string.Empty;
                LinkField field = this.Item.Fields[Consts.Fields.NewsSearchResultsUrl];

                if (field != null)
                {
                    var targetItem = field.TargetItem;
                    if (targetItem != null)
                    {
                        url = LinkManager.GetItemUrl(targetItem);
                    }
                }
                return url;
            }
        }
    }
}