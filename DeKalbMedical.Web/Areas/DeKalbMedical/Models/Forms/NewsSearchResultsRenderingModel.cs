﻿using DeKalbMedical.Data.DatasourceItems;
using DeKalbMedical.Data.DatasourceItems.News;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Forms
{
    /// <summary>
    /// 
    /// </summary>
    public class NewsSearchResultsRenderingModel : RenderingModelBase<BaseComponentParameters, NewsSearchResultsDatasource>
    {
        public NewsSearchResultsRenderingModel() : base("news-search-results")
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override NewsSearchResultsDatasource InitializeDatasource(Item item)
        {
            NewsSearchResultsDatasource datasource;
            return NewsSearchResultsDatasource.TryParse(item, out datasource) ? datasource : null;
        }
    }
}