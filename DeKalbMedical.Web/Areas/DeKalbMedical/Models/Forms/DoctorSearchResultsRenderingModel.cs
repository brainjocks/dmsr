﻿using DeKalbMedical.Data.DatasourceItems;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Forms
{
    /// <summary>
    /// 
    /// </summary>
    public class DoctorSearchResultsRenderingModel : RenderingModelBase<BaseComponentParameters, DoctorSearchResultsDatasource>
    {
        public DoctorSearchResultsRenderingModel() : base("doctor-search-results")
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override DoctorSearchResultsDatasource InitializeDatasource(Item item)
        {
            DoctorSearchResultsDatasource datasource;
            return DoctorSearchResultsDatasource.TryParse(item, out datasource) ? datasource : null;
        }
    }
}