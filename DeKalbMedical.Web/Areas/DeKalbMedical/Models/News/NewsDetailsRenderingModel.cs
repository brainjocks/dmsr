﻿using DeKalbMedical.Data.DatasourceItems.News;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.News
{
    public class NewsDetailsRenderingModel : RenderingModelBase<BaseComponentParameters, NewsDetailsDatasource>
    {
        protected override NewsDetailsDatasource InitializeDatasource(Item item)
        {
            NewsDetailsDatasource ds;
            return NewsDetailsDatasource.TryParse(item, out ds) ? ds : null;
        }

             
    }
}