﻿using DeKalbMedical.Data.DatasourceItems.News;
using Score.Custom.Rules.ItemHierarchy.Conditions;
using Score.Data.Extensions;
using Score.UI.Data.RenderingParameters;
using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;
using Sitecore.Mvc.Extensions;
using Sitecore.Mvc.Presentation;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.News
{
    public class NewsFeedRenderingModel : RenderingModelBase<BaseComponentParameters, NewsFeedDatasource>
    {
        protected override NewsFeedDatasource InitializeDatasource(Item item)
        {
            NewsFeedDatasource ds;
            return NewsFeedDatasource.TryParse(item, out ds) ? ds : null;
        }

        public override void Initialize(Rendering rendering)
        {
            base.Initialize(rendering);
            if (!string.IsNullOrEmpty(rendering.Parameters.GetUserFriendlyValue("News Feed Style")))
            {
                this.Classes.Add(rendering.Parameters.GetUserFriendlyValue("News Feed Style"));
            }
        }
    }
}