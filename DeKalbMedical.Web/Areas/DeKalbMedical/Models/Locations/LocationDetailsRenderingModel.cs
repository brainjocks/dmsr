﻿using Sitecore.Mvc.Presentation;
using DeKalbMedical.Web.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Locations
{
    /// <summary>
    /// 
    /// </summary>
    public class LocationDetailsRenderingModel : RenderingModel
    {
        private Item _item;

        public string CityFieldName = "City";
        public string PhotoFieldName = "Photo";
        public string ServicesFieldName = "Services";
        public string StateFieldName = "State";
        public string Address1FieldName = "Address 1";
        public string Address2FieldName = "Address 2";
        public string LocationNameFieldName = "Location Name";
        public string ZipFieldName = "Zip";
        public string AdminPhoneFieldName = "Main Phone";
        public string AdminPhoneLabelFieldName = "Main Phone Label";
        public string AlternatePhoneFieldName = "Alternate Phone";
        public string AlternatePhoneLabelFieldName = "Alternate Phone Label";
        public string FaxFieldLabelName = "Fax Label";
        public string FaxFieldName = "Main Fax";
        public string WebsiteFieldName = "Website";
        public string WebsiteFieldLabelName = "Website Label";
        public string OfficeHoursFieldName = "Office Hours";
        public string OfficeHoursFieldLabelName = "Office Hours Label";
        public string DirectionUrlFieldName = "Directions Url";
        public string OfficeInformationFieldLabelName = "Office Information Label";
        public string MapsAndDirectionsFieldLabelName = "Maps and Directions Label";
        public string GeoLocationFieldName = "GeoLocation";

        /// <summary>
        /// 
        /// </summary>
        public Item RenderingItem
        {
            get
            {
                if (_item != null)
                {
                    return _item;
                }
                return this.PageItem;
            }
        }

        public LocationDetailsRenderingModel()
        {
            
        }

        public LocationDetailsRenderingModel(Item item)
        {
            _item = item;
        }

        #region Fields

        public string LocationName
        {
            get { return RenderingItem.Fields[LocationNameFieldName].Value; }
        }

        public string City
        {
            get { return RenderingItem.Fields[CityFieldName].Value; }
        }

        public string State
        {
            get
            {
                LookupField stateField = RenderingItem.Fields[StateFieldName];
                if (stateField != null)
                {
                    var stateItem = stateField.TargetItem;
                    if (stateItem != null)
                    {
                        return stateItem.Fields["Abbreviation"].Value;
                    }
                }
                return string.Empty;
            }
        }

        public string Photo
        {
            get { return RenderingItem.Fields[PhotoFieldName].Value; }
        }

        public string Services
        {
            get { return RenderingItem.Fields[ServicesFieldName].Value; }
        }

        public string Address1
        {
            get { return RenderingItem.Fields[Address1FieldName].Value; }
        }

        public string Address2
        {
            get { return RenderingItem.Fields[Address2FieldName].Value; }
        }

        public string Zip
        {
            get { return RenderingItem.Fields[ZipFieldName].Value; }
        }

        public string AdminPhone
        {
            get { return RenderingItem.Fields[AdminPhoneFieldName].Value; }
        }

        public string AlternatePhone
        {
            get { return RenderingItem.Fields[AlternatePhoneFieldName].Value; }
        }

        public string Fax
        {
            get { return RenderingItem.Fields[FaxFieldName].Value; }
        }

        public string Website
        {

            get
            {
                LinkField lf = RenderingItem.Fields[WebsiteFieldName];
                if (lf != null)
                {
                    return lf.Url.ApplyLinkFormat(System.Web.HttpContext.Current.Request.Url);
                }
                return string.Empty;
            }
        }

        public string AdminPhoneLabel
        {
            get { return RenderingItem.Fields[AdminPhoneLabelFieldName].Value; }
        }

        public string AlternatePhoneLabel
        {
            get { return RenderingItem.Fields[AlternatePhoneLabelFieldName].Value; }
        }

        public string FaxLabel
        {
            get { return RenderingItem.Fields[FaxFieldLabelName].Value; }
        }

        public string MapsDirectionsLabel
        {
            get { return RenderingItem.Fields[MapsAndDirectionsFieldLabelName].Value; }
        }

        public string WebsiteLabel
        {

            get { return RenderingItem.Fields[WebsiteFieldLabelName].Value; }
        }

        public string OfficeHours
        {
            get { return RenderingItem.Fields[OfficeHoursFieldName].Value; }
        }

        public string OfficeInformationLabel
        {
            get { return RenderingItem.Fields[OfficeInformationFieldLabelName].Value; }
        }

        public string OfficeHoursLabel
        {
            get { return RenderingItem.Fields[OfficeHoursFieldLabelName].Value; }
        }

        public string DirectionsUrl
        {
            get { return RenderingItem.Fields[DirectionUrlFieldName].Value; }
        }

        #endregion
    }
}