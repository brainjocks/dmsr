﻿using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Content.Button
{
    public class ImageButtonRenderingModel : Score.BootstrapUI.Web.Areas.ScoreBootstrapUI.Models.Content.Buttons.ImageButtonRenderingModel
    {
        public IHtmlString GetButtonLink()
        {
            IHtmlString facebookUrl = null;
            if (this.PageItem.Fields["Facebook Link"] != null && !string.IsNullOrEmpty(this.PageItem.Fields["Facebook Link"].Value))
            {
                var facebookField = (LinkField)this.PageItem.Fields["Facebook Link"];

                if (facebookField != null)
                {
                    facebookUrl = new HtmlString("href='" + facebookField.Url + "'");
                }
                else
                {
                    Log.Error("facebookField - Cannot load facebookField" + this.PageItem.ID.ToString(), this);
                }
            }
            else
            {
                facebookUrl = this.Datasource.LinkFieldTagAttributes;
            }

            return facebookUrl;
        }
    }
}