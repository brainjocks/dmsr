﻿using Score.UI.Web.Areas.ScoreUI.Models;
using Sitecore.Data.Items;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Models.Content.Collections
{
    public class ServiceListingsRenderingModel : RenderingModelBase
    {
        private Item _item;
        public string ServicesListingfieldname = "ServicesListing";

        public Item RenderingItem
        {
            get
            {
                if (_item != null)
                {
                    return _item;
                }
                return this.PageItem;
            }
        }

        public string ServicesListingUrl { get { return RenderingItem.Fields[ServicesListingfieldname].Value; } }
        public bool HasDatasource { get; set; }

        public ServiceListingsRenderingModel()
        {
            
        }

       public ServiceListingsRenderingModel(Item item)
        {
            _item = item;
        }

    }
}