using System.Web.Mvc;

namespace DeKalbMedical.Web.Areas.DeKalbMedical
{
    public class DeKalbMedicalAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DeKalbMedical";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            // Register your MVC routes in here
            context.MapRoute("SearchResults", "SearchResults/GetSearchResults", new { controller = "Search", action = "GetSearchResults" });
            context.MapRoute("DoctorSearch", "SearchResults/GetDoctorSearch", new { controller = "Search", action = "GetDoctorSearchResults" });
            context.MapRoute("GetPlansForInsurance", "SearchResults/GetPlansForInsurance", new { controller = "Search", action = "GetPlansForInsurance" });
            context.MapRoute("DmpgLocationMapSearch", "SearchResults/GetDmpgLocation", new { controller = "Search", action = "GetDmpgLocationResults" });

            context.MapRoute("LocationSearchResults", "SearchResults/GetLocationSearchResults", new { controller = "Search", action = "GetLocationSearchResults" });

            context.MapRoute("ReferralSearchResults", "SearchResults/GetReferralSearchResults", new { controller = "Search", action = "GetReferralSearchResults" });
            context.MapRoute("NewsSearchResults", "SearchResults/GetNewsSearchResults", new { controller = "Search", action = "GetNewsSearchResults" });
            context.MapRoute("NewsWidgetSearchResults", "SearchResults/GetNewsWidgetSearchResults", new { controller = "Search", action = "GetNewsWidgetSearchResults" });
            context.MapRoute("LatestNewsFeedSearchResults", "SearchResults/GetLatestNewsFeedSearchResults", new { controller = "Search", action = "GetLatestNewsFeedSearchResults" });
        }
    }
}
