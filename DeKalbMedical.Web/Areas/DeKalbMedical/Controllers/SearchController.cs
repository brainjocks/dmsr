﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DeKalbMedical.Data;
using DeKalbMedical.Data.Abstractions;
using DeKalbMedical.Data.DatasourceItems.News;
using DeKalbMedical.Data.DTO;
using DeKalbMedical.Web.Areas.DeKalbMedical.Models;
using DeKalbMedical.Web.Areas.DeKalbMedical.Models.Forms;
using DeKalbMedical.Web.Extensions;
using Sitecore.Data.Items;
using Sitecore.Data;
using Sitecore.Mvc.Controllers;
using Sitecore.Web.UI.WebControls;

namespace DeKalbMedical.Web.Areas.DeKalbMedical.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class SearchController : SitecoreController
    {
        private readonly ISearchService _searchService;

        public SearchController(ISearchService searchService)
        {
            _searchService = searchService;
        }

        public Database ContextDatabase
        {
            get
            {
                return Sitecore.Context.Database;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="parentId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetSearchResults(string keyword, string parentId, int pageIndex, int pageSize)
        {
            var dto = _searchService.GetPaged(keyword, parentId, pageIndex, pageSize);

            foreach (var item in dto.Items)
            {
                item.LinkHref = item.LinkHref.ApplyLinkFormat(ControllerContext.HttpContext.Request.Url);
            }

            return Json(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        public JsonResult GetDoctorSearchResults(DoctorSearchValidationModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return GetBadResponse();
            }

            var dto = _searchService.GetDoctors(requestModel.FirstName, requestModel.LastName, requestModel.Genders,
                                    requestModel.Cities, requestModel.Specialties, requestModel.Languages, requestModel.Locations, 
                                    requestModel.Zip, requestModel.Radiuses, requestModel.Insurances, requestModel.InsurancePlans, requestModel.PageIndex, requestModel.PageSize);

            foreach (var item in dto.Items)
            {
                item.LinkHref = item.LinkHref.ApplyLinkFormat(ControllerContext.HttpContext.Request.Url);
            }

            return Json(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetDmpgLocationResults(DmpgLocationMapValidationModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return GetBadResponse();
            }

            var dto = _searchService.GetDmpgLocations(requestModel.Categories, requestModel.Zip, requestModel.Radiuses, requestModel.CategoryPath, requestModel.PageIndex, requestModel.PageSize);

            return Json(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private JsonResult GetBadResponse()
        {
            var errorResult = new ModelStateErrorModel {Message = "Uknown error", Name = "Uknown error"};

            foreach (var val in ModelState)
            {
                if (val.Value.Errors.Count > 0)
                {
                    errorResult.Message = val.Value.Errors[0].ErrorMessage;
                    errorResult.Name = val.Key;

                    if (!errorResult.Message.Equals("entercriteria") && !errorResult.Message.Equals("enterzipcode"))
                    {
                        Response.StatusCode = (int) HttpStatusCode.BadRequest;
                        Response.TrySkipIisCustomErrors = true;
                    }

                    break;
                }
            }

            return Json(errorResult);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetLocationSearchResults(LocationSearchViewModel model, int pageIndex, int pageSize)
        {
            var dto = _searchService.GetPagedLocations(model.LocationName, HttpUtility.UrlDecode(model.Service), model.City, model.Zip, model.Radius, pageIndex, pageSize);

            foreach (var item in dto.Items)
            {
                item.LinkHref = item.LinkHref.ApplyLinkFormat(ControllerContext.HttpContext.Request.Url);
                //item.Website = item.Website.ApplyLinkFormat(ControllerContext.HttpContext.Request.Url);
                //item.ReadMoreLink = item.ReadMoreLink.ApplyLinkFormat(ControllerContext.HttpContext.Request.Url);
            }

            return Json(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetReferralSearchResults(string service, string keyword, int pageIndex, int pageSize)
        {
            var dto = _searchService.GetPagedReferralServices(HttpUtility.UrlDecode(service), keyword, pageIndex, pageSize);

            dto.Service = service;
            dto.Keyword = keyword;

            foreach (var item in dto.Items)
            {
                item.LinkHref = item.LinkHref.ApplyLinkFormat(ControllerContext.HttpContext.Request.Url);
            }

            return Json(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="insuranceId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetPlansForInsurance(string insuranceId)
        {
           var dto = _searchService.GetPlansForInsurance(insuranceId);

           return Json(dto);
        }

        [HttpPost]
        public JsonResult GetNewsWidgetSearchResults(NewsSearchViewModel model, Guid category, int pageIndex, int pageSize)
        {
            var categories = (category == Guid.Empty) ? null : new List<Guid> { new ID(category).ToGuid() };
            NewsSearchResultsDto dto = _searchService.GetNews(model.Keyword, categories, null, model.DateFrom, model.DateTo, pageIndex, pageSize);

            foreach (var item in dto.Items)
            {
                item.LinkHref = item.LinkHref.ApplyLinkFormat(ControllerContext.HttpContext.Request.Url);
                item.PressReleaseContent = null;    //not necessary to send content
            }

            return Json(dto);
        }

        [HttpPost]
        public JsonResult GetNewsSearchResults(NewsSearchViewModel model, int pageIndex, int pageSize)
        {
            var categories = (model.Category == Guid.Empty) ? null : new List<Guid> { new ID(model.Category).ToGuid() };
            NewsSearchResultsDto dto = _searchService.GetNews(model.Keyword, categories, null, model.DateFrom, model.DateTo, pageIndex, pageSize);

            foreach (var item in dto.Items)
            {
                item.LinkHref = item.LinkHref.ApplyLinkFormat(ControllerContext.HttpContext.Request.Url);
                item.PressReleaseContent = null;    //not necessary to send content
            }

            return Json(dto);
        }

        [HttpPost]
        public JsonResult GetLatestNewsFeedSearchResults(int pageIndex, int pageSize, Guid dsId)
        {
            Item dsItem = ContextDatabase.GetItem(new ID(dsId));
            NewsFeedDatasource ds;
            IList<Guid> serviceLines = null;
            if (NewsFeedDatasource.TryParse(dsItem, out ds))
            {
                serviceLines = ds.ServiceLines.Select(x => x.ID.Guid).ToList();
            }

            NewsSearchResultsDto dto = _searchService.GetNews(null, null, serviceLines, null, null, pageIndex, pageSize);

            if (Sitecore.Context.PageMode.IsExperienceEditorEditing)
            {
                dto.Items.ForEach(x =>
                {
                    Item item = this.ContextDatabase.GetItem(x.ItemId);
                    string pressReleaseTitle = FieldRenderer.Render(item, Consts.Fields.PressReleaseTitleField);
                    x.PressReleaseTitle = pressReleaseTitle;
                });
            }

            foreach (var item in dto.Items)
            {
                item.LinkHref = item.LinkHref.ApplyLinkFormat(ControllerContext.HttpContext.Request.Url);
            }

            return Json(dto);
        }
    }
}