﻿using System;
using System.Text.RegularExpressions;

namespace DeKalbMedical.Web.Extensions
{
    public static class HttpContextHelperExtensions
    {
        public static string ApplyLinkFormat(this string link, Uri url)
        {
            if (string.IsNullOrWhiteSpace(link))
            {
                return string.Empty;
            }

            var areaName = link.Split('/')[1];
            if (string.IsNullOrWhiteSpace(areaName))
            {
                return link;
            }
            var pureLink = link.Replace(string.Format("/{0}/", areaName), string.Empty);

            return string.Format("{0}://{1}/{2}", url.Scheme, url.Host, pureLink.ToLower());
        }

        public static string OnlyDigits(this string content)
        {
            var regexObj = new Regex(@"[^\d]");
            return regexObj.Replace(content, string.Empty);
        }
    }
}