﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DeKalbMedical.Web.Extensions
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EnterCriteriaValidationAttribute : ValidationAttribute , IClientValidatable
    {
        public override bool IsValid(object value)
        {
            var result = false;
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(value);
            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.PropertyType != typeof(string)) { continue; }
                string propValue = (string) prop.GetValue(value);
                if (!string.IsNullOrEmpty(propValue) && propValue!="-1")
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ValidationType = "entercriteria",
                ErrorMessage = FormatErrorMessage(metadata.DisplayName),

            };

            yield return rule;
        }
    }
}