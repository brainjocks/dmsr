﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using DeKalbMedical.Data.DTO.DataPart;
using Score.Data.Extensions.MvcExtensions;
using Sitecore.Data.Items;
using Sitecore.Mvc;

namespace DeKalbMedical.Web.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class HtmlHelperExtensions
    {
		public static string DateFormat = "MMMM dd, yyyy";
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="html"></param>
        /// <param name="fieldName"></param>
        /// <param name="dataSourceName"></param>
        /// <returns></returns>
        public static IHtmlString Ternary<TModel>(this HtmlHelper<TModel> html, string fieldName, string dataSourceName)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing 
                ? html.Raw(string.Format("<span class='pe-editable-field'>{0}</span>", html.Sitecore().Field(fieldName)))
                : html.Raw(dataSourceName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="html"></param>
        /// <param name="fieldName"></param>
        /// <param name="dataSourceName"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static IHtmlString Ternary<TModel>(this HtmlHelper<TModel> html, string fieldName, string dataSourceName, Item item)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<span class='pe-editable-field'>{0}</span>", html.Sitecore().Field(fieldName, item)))
                : html.Raw(dataSourceName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="html"></param>
        /// <param name="fieldName"></param>
        /// <param name="dataSourceName"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static IHtmlString TernaryPeDiv<TModel>(this HtmlHelper<TModel> html, string fieldName, string dataSourceName, Item item)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<div class='pe-editable-field'>{0}</div>", html.Sitecore().Field(fieldName, item)))
                : html.Raw(dataSourceName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="html"></param>
        /// <param name="fieldLabel"></param>
        /// <param name="fieldName"></param>
        /// <param name="parameters"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        public static IHtmlString PeField<TModel>(this HtmlHelper<TModel> html, string fieldLabel, string fieldName, object parameters = null, string className = null)
        {
            return html.Raw(
                parameters != null 
                    ? string.Format("<span class='label'>{0}: </span><span class='pe-editable-field{2}'>{1}</span>", fieldLabel, html.Sitecore().Field(fieldName, parameters), className) 
                    : string.Format("<span class='label'>{0}: </span><span class='pe-editable-field{2}'>{1}</span>", fieldLabel, html.Sitecore().Field(fieldName), className));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="html"></param>
        /// <param name="fieldLabel"></param>
        /// <param name="fieldName"></param>
        /// <param name="item"></param>
        /// <param name="parameters"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        public static IHtmlString PeField<TModel>(this HtmlHelper<TModel> html, string fieldLabel, string fieldName, Item item, object parameters = null, string className = null)
        {
            return html.Raw(
                parameters != null
                    ? string.Format("<span class='label'>{0}: </span><span class='pe-editable-field{2}'>{1}</span>", fieldLabel, html.Sitecore().Field(fieldName, item, parameters), className)
                    : string.Format("<span class='label'>{0}: </span><span class='pe-editable-field{2}'>{1}</span>", fieldLabel, html.Sitecore().Field(fieldName, item), className));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="html"></param>
        /// <param name="fieldName"></param>
        /// <param name="id"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static IHtmlString TernaryInputText<TModel>(this HtmlHelper<TModel> html, string fieldName, string id, object parameters)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<div class='pe-input-replacer'><span class='pe-editable-field'>{0}</span></div>", html.Sitecore().Field(fieldName)))
                : html.TextBox(id, null, parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="html"></param>
        /// <param name="fieldName"></param>
        /// <param name="expression"></param>
        /// <param name="dataSourceName"></param>
        /// <returns></returns>
        public static IHtmlString TernaryInputTextFor<TModel, TProperty>(this HtmlHelper<TModel> html, string fieldName, Expression<Func<TModel, TProperty>> expression, string dataSourceName)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<div class='pe-input-replacer'><span class='pe-editable-field'>{0}</span></div>", html.Sitecore().Field(fieldName)))
                : html.TextBoxFor(expression, new { placeholder = dataSourceName });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="html"></param>
        /// <param name="fieldName"></param>
        /// <param name="expression"></param>
        /// <param name="parameters"></param>
        /// <param name="peClass"></param>
        /// <returns></returns>
        public static IHtmlString TernaryInputTextFor<TModel, TProperty>(this HtmlHelper<TModel> html, string fieldName, Expression<Func<TModel, TProperty>> expression, object parameters, string peClass = null)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<div class='pe-input-replacer{1}'><span class='pe-editable-field'>{0}</span></div>", html.Sitecore().Field(fieldName), string.IsNullOrEmpty(peClass) ? string.Empty : peClass))
                : html.TextBoxFor(expression, parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="html"></param>
        /// <param name="fieldName"></param>
        /// <param name="name"></param>
        /// <param name="list"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static IHtmlString TernaryDropDown<TModel>(this HtmlHelper<TModel> html, string fieldName, string name, IEnumerable<SelectListItem> list , object htmlAttributes, string peClass = null)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<div class='btn-group bootstrap-select'><button type='button' class='btn dropdown-toggle{1}'><span class='filter-option pull-left'>"
                + "<span class='pe-editable-field'>{0}</span></span>&nbsp;<span class='bs-caret'><span class='caret'></span></span></button></div>", html.Sitecore().Field(fieldName), peClass ?? " btn-blue"))
                : html.DropDownList(name, list, htmlAttributes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="html"></param>
        /// <param name="expression"></param>
        /// <param name="fieldName"></param>
        /// <param name="dataSourceName"></param>
        /// <returns></returns>
        public static IHtmlString TernaryTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, string fieldName, string dataSourceName)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<div class='pe-input-replacer'><span class='pe-editable-field'>{0}</span></div>", html.Sitecore().Field(fieldName)))
                : html.TextBoxFor(expression, new { placeholder = dataSourceName });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="html"></param>
        /// <param name="expression"></param>
        /// <param name="fieldName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static IHtmlString TernaryTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, string fieldName, object parameters)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<div class='pe-input-replacer'><span class='pe-editable-field'>{0}</span></div>", html.Sitecore().Field(fieldName)))
                : html.TextBoxFor(expression, parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="html"></param>
        /// <param name="expression"></param>
        /// <param name="fieldName"></param>
        /// <param name="list"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static IHtmlString TernaryDropDownFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, string fieldName, IEnumerable<SelectListItem> list, object htmlAttributes)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<div class='btn-group bootstrap-select'><button type='button' class='btn dropdown-toggle btn-blue'><span class='filter-option pull-left'>"
                    + "<span class='pe-editable-field'>{0}</span></span>&nbsp;<span class='bs-caret'><span class='caret'></span></span></button></div>", html.Sitecore().Field(fieldName)))
                : html.DropDownListFor(expression, list, htmlAttributes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="helper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString TernaryValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
        {
            TagBuilder containerDivBuilder = new TagBuilder("div");
            if (!Sitecore.Context.PageMode.IsExperienceEditorEditing)
            {
                containerDivBuilder.AddCssClass("error-block");
            }

            containerDivBuilder.InnerHtml = helper.EditableValidationMessageFor(expression).ToString();

            return MvcHtmlString.Create(containerDivBuilder.ToString(TagRenderMode.Normal));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="html"></param>
        /// <param name="expression"></param>
        /// <param name="fieldName"></param>
        /// <param name="datasource"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static IHtmlString TernaryLabelFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, string fieldName, string datasource, object htmlAttributes = null)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<span class='pe-editable-field'>{0}</span>", html.Sitecore().Field(fieldName)))
                : html.LabelFor(expression, datasource, htmlAttributes);
        }
		
		public static IHtmlString TernaryDate<TModel>(this HtmlHelper<TModel> html, string fieldName, DateTime date)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<span class='pe-editable-field'>{0}</span>", html.Sitecore().Field(fieldName)))
                : html.Raw(date.ToString(DateFormat));
        }

        public static IHtmlString TernaryField<TModel>(this HtmlHelper<TModel> html, string fieldName)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<span class='pe-editable-field'>{0}</span>", html.Sitecore().Field(fieldName)))
                : html.Sitecore().Field(fieldName);
        }

        public static IHtmlString CustomListField<TModel>(this HtmlHelper<TModel> html, string fieldName, Item item, object parameters)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
            ? html.Raw(string.Format("<div class='pe-editable-field'>{0}</div>", html.Sitecore().Field(fieldName, item, parameters)))
            : html.Raw(string.Format("<ul><li>{0}</li></ul>", html.Sitecore().Field(fieldName, item, parameters).ToString().Trim().Replace(",", "</li><li>")));
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="html"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString StateDropDownListFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            Dictionary<string, string> stateList = new Dictionary<string, string>()
            {
                {"AL"," Alabama"},
                {"AK"," Alaska"},
                {"AZ"," Arizona"},
                {"AR"," Arkansas"},
                {"CA"," California"},
                {"CO"," Colorado"},
                {"CT"," Connecticut"},
                {"DE"," Delaware"},
                {"FL"," Florida"},
                {"GA"," Georgia"},
                {"HI"," Hawaii"},
                {"ID"," Idaho"},
                {"IL"," Illinois"},
                {"IN"," Indiana"},
                {"IA"," Iowa"},
                {"KS"," Kansas"},
                {"KY"," Kentucky"},
                {"LA"," Louisiana"},
                {"ME"," Maine"},
                {"MD"," Maryland"},
                {"MA"," Massachusetts"},
                {"MI"," Michigan"},
                {"MN"," Minnesota"},
                {"MS"," Mississippi"},
                {"MO"," Missouri"},
                {"MT"," Montana"},
                {"NE"," Nebraska"},
                {"NV"," Nevada"},
                {"NH"," New Hampshire"},
                {"NJ"," New Jersey"},
                {"NM"," New Mexico"},
                {"NY"," New York"},
                {"NC"," North Carolina"},
                {"ND"," North Dakota"},
                {"OH"," Ohio"},
                {"OK"," Oklahoma"},
                {"OR"," Oregon"},
                {"PA"," Pennsylvania"},
                {"RI"," Rhode Island"},
                {"SC"," South Carolina"},
                {"SD"," South Dakota"},
                {"TN"," Tennessee"},
                {"TX"," Texas"},
                {"UT"," Utah"},
                {"VT"," Vermont"},
                {"VA"," Virginia"},
                {"WA"," Washington"},
                {"WV"," West Virginia"},
                {"WI"," Wisconsin"},
                {"WY"," Wyoming"},
                {"AS"," American Samoa"},
                {"DC"," District of Columbia"},
                {"FM"," Federated States of Micronesia"},
                {"MH"," Marshall Islands"},
                {"MP"," Northern Mariana Islands"},
                {"PW"," Palau"},
                {"PR"," Puerto Rico"},
                {"VI"," Virgin Islands"},
                {"GU"," Guam"}
            };

            return html.DropDownListFor(expression, new SelectList(stateList, "key", "value"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="html"></param>
        /// <param name="fieldName"></param>
        /// <param name="name"></param>
        /// <param name="defaultElementLabel"></param>
        /// <param name="list"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static IHtmlString CustomTernaryDropDown<TModel>(this HtmlHelper<TModel> html, string fieldName, string name, string defaultElementLabel, IEnumerable<OptionsListItem> list, object htmlAttributes, string peClass = null)
        {
            return Sitecore.Context.PageMode.IsExperienceEditorEditing
                ? html.Raw(string.Format("<div class='btn-group bootstrap-select'><button type='button' class='btn dropdown-toggle{1}'><span class='filter-option pull-left'>"
                + "<span class='pe-editable-field'>{0}</span></span>&nbsp;<span class='bs-caret'><span class='caret'></span></span></button></div>", html.Sitecore().Field(fieldName), peClass ?? " btn-blue"))
                : html.CustomDropdownList(null, name, defaultElementLabel, list, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="list"></param>
        /// <param name="optionLabel"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString CustomDropdownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<OptionsListItem> list, string optionLabel, object htmlAttributes = null)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
            string name = ExpressionHelper.GetExpressionText((LambdaExpression)expression);
            return CustomDropdownList(htmlHelper, metadata, name, optionLabel, list, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="metadata"></param>
        /// <param name="name"></param>
        /// <param name="optionLabel"></param>
        /// <param name="list"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        private static MvcHtmlString CustomDropdownList(this HtmlHelper htmlHelper, ModelMetadata metadata, string name, string optionLabel, IEnumerable<OptionsListItem> list, IDictionary<string, object> htmlAttributes)
        {
            string fullName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);
            if (string.IsNullOrEmpty(fullName))
            {
                throw new ArgumentException("name");
            }

            TagBuilder dropdown = new TagBuilder("select");
            dropdown.Attributes.Add("name", fullName);
            dropdown.MergeAttributes(htmlAttributes);

            StringBuilder options = new StringBuilder();

            if (optionLabel != null)
            {
                options = options.Append(string.Format("<option value=''>{0}</option>", optionLabel));
            }

            foreach (var item in list)
            {
                options = options.Append(string.Format("<option value='{0}'", item.Value));
                if (!string.IsNullOrWhiteSpace(item.Class))
                {
                    options = options.Append(string.Format(" class='{0}'", item.Class));
                }
                options = options.Append(string.Format(">{0}</option>", item.Text));
            }

            dropdown.InnerHtml = options.ToString();
            return MvcHtmlString.Create(dropdown.ToString(TagRenderMode.Normal));
        }
    }
}