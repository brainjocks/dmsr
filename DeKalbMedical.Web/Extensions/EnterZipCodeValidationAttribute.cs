﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DeKalbMedical.Web.Extensions
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EnterZipCodeValidationAttribute : ValidationAttribute, IClientValidatable
    {
        public override bool IsValid(object value)
        {
            var result = true;
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(value);
            object zipValue = properties.Find("Zip", true).GetValue(value);
            object radiusesValue = properties.Find("Radiuses", true).GetValue(value);
            if ((string)radiusesValue != "-1" && !string.IsNullOrEmpty((string)radiusesValue) && string.IsNullOrEmpty((string)zipValue))
            {
                result = false;
            }

            return result;
        }
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ValidationType = "enterzipcode",
                ErrorMessage = FormatErrorMessage(metadata.DisplayName),

            };

            yield return rule;
        }
    }
}