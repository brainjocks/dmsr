﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SimpleInjector;

namespace DeKalbMedical.Web.Pipelines.Mvc
{
    public class CustomDependencyResolver : IDependencyResolver
    {
        private IDependencyResolver wrappedResolver;

        public CustomDependencyResolver(IDependencyResolver wrappedResolver)
        {
            this.wrappedResolver = wrappedResolver;
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return this.wrappedResolver.GetService(serviceType);
            }
            catch
            {
                return Sitecore.Mvc.Helpers.TypeHelper.CreateObject<IController>(serviceType, new object[0]);
            }
        }
 
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this.wrappedResolver.GetServices(serviceType);
        }
    }
}