﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;
using AutoMapper;
using DeKalbMedical.Data.Abstractions;
using DeKalbMedical.Data.DTO;
using DeKalbMedical.Data.DTO.DataPart;
using DeKalbMedical.Data.Services;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using Sitecore.Pipelines;

namespace DeKalbMedical.Web.Pipelines.Mvc 
{
    public class RegisterSimpleInjector
    {
        public void Process(PipelineArgs args)
        {
            var container = new Container();

            container.Options.AllowOverridingRegistrations = true;

            var lifestyle = new WebRequestLifestyle();

            container.Register<ISearchService, SearchService>(lifestyle);

            container.Register<IGoogleMapsService, GoogleMapService>(lifestyle);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.RegisterMvcIntegratedFilterProvider();

            container.Verify();
            
            DependencyResolver.SetResolver(new CustomDependencyResolver(new SimpleInjectorDependencyResolver(container)));
        }
    }
}
