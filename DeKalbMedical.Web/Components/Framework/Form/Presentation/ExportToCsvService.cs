﻿using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sitecore.Configuration;
using Sitecore.Data;
using WFFM8.To.SQL.Model;
using FormRepository = DeKalbMedical.Custom.WFFM.FormRepository;

namespace DeKalbMedical.Web.Components.Framework.Form.Presentation
{
    public class ExportToCsvService
    {
        private readonly CsvColumnRepository _csvColumnRepository = new CsvColumnRepository();
        private readonly FieldRepository _fieldRepository = new FieldRepository();
        private readonly FormRepository _formRepository = new FormRepository();

        internal void ExportToCsv(HttpResponse response, ID formId, string formName, DateTime from, DateTime to)
        {
            Assert.ArgumentNotNull((object)response, "response");
            Assert.ArgumentNotNull((object)formId, "formId");
            IEnumerable<Custom.WFFM.Form> forms = (IEnumerable<Custom.WFFM.Form>)Enumerable.ToList<Custom.WFFM.Form>(this._formRepository.Get(formId, from, to));
            this.ExportToCsv(response, formId, formName, forms);
        }

        internal void ExportToCsv(HttpResponse response, ID formId, string formName)
        {
            Assert.ArgumentNotNull((object)response, "response");
            Assert.ArgumentNotNull((object)formId, "formId");
            IEnumerable<Custom.WFFM.Form> forms = (IEnumerable<Custom.WFFM.Form>)Enumerable.ToList<Custom.WFFM.Form>(this._formRepository.Get(formId));
            this.ExportToCsv(response, formId, formName, forms);
        }

        internal void ExportToCsv(HttpResponse response, ID formId, string formName, IEnumerable<Custom.WFFM.Form> forms)
        {
            if (!Enumerable.Any<Custom.WFFM.Form>(forms))
            {
                response.Write(string.Format("No data for form with id:{0}", (object)formId));
            }
            else
            {
                StringBuilder csvString = new StringBuilder();
                Dictionary<Guid, string> columns = this._csvColumnRepository.Get(forms);
                this.AddHeader(csvString, columns);
                this.AddRows(csvString, forms, columns);
                GenerateCsvResponseService.GenerateCsvResponse(response, formName, csvString.ToString());
            }
        }

        private void AddRows(StringBuilder csvString, IEnumerable<Custom.WFFM.Form> forms, Dictionary<Guid, string> columns)
        {
            Assert.ArgumentNotNull((object)csvString, "csvString");
            Assert.ArgumentNotNull((object)forms, "forms");
            Assert.ArgumentNotNull((object)columns, "columns");
            foreach (Custom.WFFM.Form form in forms)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(form.Timestamp.ToString("yyyy-MM-dd HH:mm:ss"));
                string str = string.Empty;
                foreach (KeyValuePair<Guid, string> keyValuePair in columns)
                {            
                    Field field = (Field) this._fieldRepository.Get(form, keyValuePair.Key);
                    if (field != null)
                        str += CsvEncodeService.CsvEncode(field.Value) + CsvEncodeService.CsvDelimiter;
                }
                if (form.FormStorageName != null)
                    str += CsvEncodeService.CsvEncode(form.FormStorageName);
                if (stringBuilder.Length > 0)
                    stringBuilder.Append(CsvEncodeService.CsvDelimiter);
                stringBuilder.Append(str);
                csvString.AppendLine(stringBuilder.ToString());
            }
        }

        private void AddHeader(StringBuilder csvString, Dictionary<Guid, string> columns)
        {
            Assert.ArgumentNotNull((object)csvString, "csvString");
            Assert.ArgumentNotNull((object)columns, "columns");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Timestamp");
            foreach (KeyValuePair<Guid, string> keyValuePair in columns)
            {
                if (stringBuilder.Length > 0)
                    stringBuilder.Append(CsvEncodeService.CsvDelimiter);
                stringBuilder.Append(keyValuePair.Value);
            }
            csvString.AppendLine(stringBuilder.ToString());
        }
    }

    internal class CsvEncodeService
    {
        private static string _delimiter = (string)null;

        internal static string CsvDelimiter
        {
            get
            {
                return CsvEncodeService._delimiter ?? (CsvEncodeService._delimiter = Settings.GetSetting("WFFM.SQLServer.SaveToDatabase.CsvDelimiter", ";"));
            }
        }

        internal static string CsvEncode(string input)
        {
            input = input ?? string.Empty;
            return input.Replace(CsvEncodeService.CsvDelimiter, " ");
        }
    }

    internal class GenerateCsvResponseService
    {
        internal static void GenerateCsvResponse(HttpResponse response, string formName, string data)
        {
            Assert.ArgumentNotNull((object)response, "response");
            Assert.ArgumentNotNullOrEmpty(formName, "formName");
            Assert.ArgumentNotNullOrEmpty(data, "data");
            response.Clear();
            response.ContentType = "text/csv; charset=UTF-8";
            response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", (object)GenerateFileNameService.GenerateFileName(formName)));
            response.Charset = "UTF-8";
            response.BinaryWrite(Encoding.UTF8.GetPreamble());
            response.Write(data);
            response.End();
        }
    }

    internal class GenerateFileNameService
    {
        internal static string GenerateFileName(string formName)
        {
            return string.Format("{0}.{1}.{2}", (object)formName.Replace(" ", "_"), (object)DateTime.UtcNow.ToShortDateString(), (object)"csv");
        }
    }
}
