﻿using System;
using System.Linq;
using WFFM8.To.SQL.Model;

namespace DeKalbMedical.Web.Components.Framework.Form.Presentation
{
    public class FieldRepository
    {
        public IField Get(IForm form, Guid fieldId)
        {
            return form == null ? (IField)null : Enumerable.FirstOrDefault<IField>(form.Field, (Func<IField, bool>)(field => field.FieldId == fieldId));
        }
    }
}
