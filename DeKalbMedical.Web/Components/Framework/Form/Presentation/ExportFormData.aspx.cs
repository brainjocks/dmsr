﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DeKalbMedical.Custom.WFFM;
using Sitecore.Configuration;
using Sitecore.Data;

namespace DeKalbMedical.Web.Components.Framework.Form.Presentation
{
    public partial class ExportFormData : Page
    {
        private string _itemName = (string)null;
        private string _itemID = (string)null;
        private readonly ExportToCsvService _exportToCsvService = new ExportToCsvService();
        private readonly FormRepository _formRepository = new FormRepository();
        protected HtmlForm form1;
        protected PlaceHolder NotLoggedLoggedInPlaceHolder;
        protected PlaceHolder WithDateRange;
        protected TextBox txtFrom;
        protected Label warnFrom;
        protected TextBox txtTo;
        protected Label warnTo;
        protected Button btnExport;

        private string ItemName
        {
            get
            {
                if (this._itemName == null)
                    this._itemName = HttpContext.Current.Server.UrlDecode(this.Request.QueryString["itemName"]) ?? string.Empty;
                return this._itemName;
            }
        }

        private string ItemID
        {
            get
            {
                if (this._itemID == null)
                    this._itemID = HttpContext.Current.Server.UrlDecode(this.Request.QueryString["itemId"]) ?? string.Empty;
                return this._itemID;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.ItemID) || string.IsNullOrEmpty(this.ItemName))
                this.NotLoggedLoggedInPlaceHolder.Visible = true;
            else if (Settings.GetBoolSetting("WFFM.SQLServer.SaveToDatabase.ExportOptions", false))
            {
                this.WithDateRange.Visible = true;
                if (this.IsPostBack)
                    return;
                TextBox textBox1 = this.txtFrom;
                DateTime dateTime = this._formRepository.OldestFormSubmission(new ID(this.ItemID));
                string str1 = dateTime.ToString("d");
                textBox1.Text = str1;
                TextBox textBox2 = this.txtTo;
                dateTime = DateTime.Now;
                string str2 = dateTime.ToString("d");
                textBox2.Text = str2;
            }
            else
                this._exportToCsvService.ExportToCsv(this.Response, new ID(this.ItemID), this.ItemName);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            DateTime result1;
            if (!DateTime.TryParse(this.txtFrom.Text, out result1))
            {
                this.warnFrom.Text = "Incorrect DateTime format.";
            }
            else
            {
                DateTime result2;
                if (!DateTime.TryParse(this.txtTo.Text, out result2))
                {
                    this.warnTo.Text = "Incorrect DateTime format.";
                }
                else
                {
                    if (result2.Hour == 0 && result2.Minute == 0 && result2.Second == 0)
                        result2 = result2.Add(new TimeSpan(23, 59, 59));
                    this._exportToCsvService.ExportToCsv(this.Response, new ID(this.ItemID), this.ItemName, result1, result2);
                }
            }
        }
    }
}