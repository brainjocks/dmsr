﻿using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using WFFM8.To.SQL.Model;

namespace DeKalbMedical.Web.Components.Framework.Form.Presentation
{
    public class CsvColumnRepository
    {
        internal Dictionary<Guid, string> Get(IEnumerable<IForm> forms)
        {
            Assert.ArgumentNotNull((object)forms, "forms");
            Dictionary<Guid, string> columns = new Dictionary<Guid, string>();
            columns.Add(Guid.NewGuid(), "Date Submitted");
            foreach (var form1 in forms)
            {
                var form = (IForm)form1;
                foreach (var field1 in Enumerable.Where<IField>(form.Field, (Func<IField, bool>)(field => !columns.ContainsKey(field.FieldId))))
                {
                    var field = (Field)field1;
                    columns.Add(field.FieldId, field.FieldName);
                }
            }

            columns.Add(Guid.NewGuid(), "Archive Status");
            return columns;
        }
    }
}
