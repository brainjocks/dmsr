﻿using Sitecore.Configuration;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using WFFM8.To.SQL.Model;
using Field = DeKalbMedical.Custom.WFFM.Field;

namespace DeKalbMedical.Web.Components.Framework.Form.Presentation
{
    public partial class ViewSQLData : Page
    {
        private readonly Custom.WFFM.FormRepository _formRepository = new Custom.WFFM.FormRepository();
        private readonly CsvColumnRepository _csvColumnRepository = new CsvColumnRepository();
        private string _itemName = (string)null;
        private string _itemID = (string)null;
        protected HtmlForm form1;
        protected PlaceHolder WithDateRange;
        protected TextBox txtFrom;
        protected Label warnFrom;
        protected TextBox txtTo;
        protected Label warnTo;
        protected Button btnSearch;
        protected PlaceHolder PHTableResult;
        protected Repeater rptHeaderFormData;
        protected Repeater rptFormData;
        protected PlaceHolder NotLoggedLoggedInPlaceHolder;
        protected PlaceHolder PHNoDataFound;

        private string width = "";
        private string ItemName
        {
            get
            {
                if (this._itemName == null)
                    this._itemName = HttpContext.Current.Server.UrlDecode(this.Request.QueryString["itemName"]) ?? string.Empty;
                return this._itemName;
            }
        }

        private string ItemID
        {
            get
            {
                if (this._itemID == null)
                    this._itemID = HttpContext.Current.Server.UrlDecode(this.Request.QueryString["itemId"]) ?? string.Empty;
                return this._itemID;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.rptFormData.ItemCommand += new RepeaterCommandEventHandler(archiveButton_Command);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.ItemID) || string.IsNullOrEmpty(this.ItemName))
            {
                this.NotLoggedLoggedInPlaceHolder.Visible = true;
            }
            else
            {
                if (Settings.GetBoolSetting("WFFM.SQLServer.SaveToDatabase.ExportOptions", false))
                    this.WithDateRange.Visible = true;

                if (this.txtFrom.Text == String.Empty)
                {
                    this.txtFrom.Text = this._formRepository.OldestFormSubmission(new ID(this.ItemID)).ToString("d");
                }
                if (this.txtTo.Text == String.Empty)
                {
                    this.txtTo.Text = DateTime.Now.ToString("d");
                }
                if (IsPostBack)
                    PopulateFormData();
            }
        }

        private void PopulateFormData()
        {
            DateTime result1;
            if (!DateTime.TryParse(this.txtFrom.Text, out result1))
            {
                this.warnFrom.Text = "Incorrect DateTime format.";
            }
            else
            {
                DateTime result2;
                if (!DateTime.TryParse(this.txtTo.Text, out result2))
                {
                    this.warnTo.Text = "Incorrect DateTime format.";
                }
                else
                {
                    if (result2.Hour == 0 && result2.Minute == 0 && result2.Second == 0)
                        result2 = result2.Add(new TimeSpan(23, 59, 59));
                    IEnumerable<IForm> enumerable = this._formRepository.Get(new ID(this.ItemID), result1, result2);
                    if (Enumerable.Any<IForm>(enumerable))
                    {
                        this.PHNoDataFound.Visible = false;
                        this.PHTableResult.Visible = true;
                        this.BindTableHeader(enumerable);
                        this.BindTableRows(enumerable);
                    }
                    else
                    {
                        this.PHTableResult.Visible = false;
                        this.PHNoDataFound.Visible = true;
                    }
                }
            }
        }

        private void BindTableHeader(IEnumerable<IForm> forms)
        {
            this.rptHeaderFormData.DataSource = (object)this._csvColumnRepository.Get(forms);
            this.rptHeaderFormData.DataBind();
        }

        private void BindTableRows(IEnumerable<IForm> forms)
        {
            this.rptFormData.DataSource = (object)forms;
            this.rptFormData.DataBind();
        }

        protected void rptHeaderFormData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
                return;
            KeyValuePair<Guid, string> keyValuePair = (KeyValuePair<Guid, string>)e.Item.DataItem;
            ((HtmlContainerControl)e.Item.FindControl("divHeader")).InnerText = keyValuePair.Value;
        }

        protected void rptFormData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
                return;
            Custom.WFFM.Form form = (Custom.WFFM.Form)e.Item.DataItem;
            HtmlTableRow htmlTableRow = (HtmlTableRow)e.Item.FindControl("trData");
            HtmlTableCell htmlTimeStampTableCell = new HtmlTableCell();
            htmlTimeStampTableCell.InnerText = form.Timestamp.ToShortDateString();
            htmlTableRow.Controls.Add((Control)htmlTimeStampTableCell);

            foreach (var formfield in form.Field)
            {
                var field = (Field)formfield;

                HtmlTableCell htmlTableCell = new HtmlTableCell();
                //htmlTableCell.Width = htmlTableRow.
                htmlTableCell.InnerText = field.Value;
                htmlTableRow.Controls.Add((Control)htmlTableCell);
            }
            //HtmlTableCell storageTableCell = new HtmlTableCell();
            //storageTableCell.InnerText = form.FormStorageName;
            //htmlTableRow.Controls.Add((Control)storageTableCell);


            Button archiveButton = new Button();
            archiveButton.Attributes.Add("class", "btn-primary lg");
            if (form.FormStorageName == "Not Archived")
            {
                archiveButton.ID = "btnArchive";
                archiveButton.Text = "Archive Now";
                archiveButton.CommandName = "Archive";
                archiveButton.CommandArgument = form.FormId.ToString();
            }
            else
            {
                archiveButton.ID = "btnArchive";
                archiveButton.Text = "Archived";
                archiveButton.Enabled = false;
            }
            HtmlTableCell archiveTableCell = new HtmlTableCell();

            archiveTableCell.Controls.Add((Control)archiveButton);
            htmlTableRow.Controls.Add((Control)archiveTableCell);

        }

        protected void archiveButton_Command(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Archive")
            {
                Button btn = (Button)e.Item.FindControl("btnArchive");
                string formid = btn.CommandArgument;
                this._formRepository.UpdateStorage(formid);
                this.PopulateFormData();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.PopulateFormData();
        }
    }
}