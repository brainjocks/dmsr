
Param (
    [Parameter(Mandatory=$True)]
    [string]$razl
)
$scriptsDirectory = ".\Automation\Razl-Files"
$rootItems = "730C1699-87A4-4CA7-A9CE-294AD7151F13", "15451229-7534-44EF-815D-D93D6170BFCB", "A2AE19F1-96BA-4FF0-B9A2-7E7320C62114"
 
foreach ($rootItem in $rootItems) {
    Write-Host Root node: $rootItem      
        &$razl /script:$scriptsDirectory\Razl.xml > $scriptsDirectory\Razl.xml.log /p:itemId=$rootItem 
} 
$fileContents = Get-Content $scriptsDirectory\Razl.xml.log
$containsWord = $fileContents | Select-String -pattern "Console error" -SimpleMatch | ForEach-Object {
    $sharename = $_.Line
}    
if($sharename)
{
    throw $sharename     
}
